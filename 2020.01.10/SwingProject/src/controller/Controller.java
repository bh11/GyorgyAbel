/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import model.Model;
import view.View;

/**
 *
 * @author george
 */
public class Controller {
    
    private View v;
    private Model m;
    private String actionTime;

    public Controller(View v, Model m) {
        this.v = v;
        this.m = m;

        v.setController(this);
        m.setController(this);
    }
    
    public void handleActionButton(){
        
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-d");
        actionTime = LocalDate.now().format(formatters).toString();
        m.addElementsForList(actionTime);
    }
    
    public List getList(){
        return m.getList();
    }
    
    public void notifyView(){
        v.setText(m.getList().toString());
    }
}
