/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author george
 */
public class View extends JFrame{
    
    private Controller controller;
    
    private final JTextArea result = new JTextArea(20,20);
    private final JButton actionButton = new JButton("Action");
    
    public void buildWindow() {
        add(buildSouth(), BorderLayout.SOUTH);
        add(buildCenter(), BorderLayout.CENTER);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private JPanel buildSouth() {
        
        JPanel jp = new JPanel();
        actionButton.size();
        jp.add(actionButton);
        
        actionButton.addActionListener(p -> controller.handleActionButton());
        
        return jp;
        
    }
    
    private JPanel buildCenter(){
        
        result.setEditable(false);
        JPanel jp = new JPanel();
        jp.add(result);
        
        return jp;
        
    }
    public void setController(Controller c) {
        this.controller = c;
    }

    public void setText(String s) {
        result.setText(s);
    }

    public void start() {
        buildWindow();
    }    
}
