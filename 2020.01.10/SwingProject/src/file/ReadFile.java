/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class ReadFile {
    
    private static final String FILENAME = "output.txt";
    private List<String>list = new ArrayList<>();
    
    public List readFromFile(){
        try (BufferedReader br = new BufferedReader(new java.io.FileReader(FILENAME))){
            
            String currentLine;
            
            while ((currentLine = br.readLine()) != null) {                
                list.add(currentLine);
            }
            return list;
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFile.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFile.class.getName()).log(Level.SEVERE,null, ex);
        }
        return null;
    }

    @Override
    public String toString() {
        return "ReadFile{" + "list" + list + '}';
    }
}
