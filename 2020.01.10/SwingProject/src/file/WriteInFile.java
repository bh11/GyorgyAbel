/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class WriteInFile {
    
    private static final String FILENAME = "output.txt";
    private Integer id = 1;
    
    public void writeToFile(List actionList) throws IOException{
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))){
            
            for (int i = 0; i < actionList.size(); i++) {
                bw.write(id.toString());
                bw.write(actionList.get(0).toString());
                bw.write(": Button pressed");
                bw.newLine();
                id++;
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteInFile.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteInFile.class.getName()).log(Level.SEVERE,null, ex);
        }
    }    
}
