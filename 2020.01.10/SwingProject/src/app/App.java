/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controller.Controller;
import model.Model;
import view.View;

/**
 *
 * @author george
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Model m = new Model();
        View v = new View();
        Controller c = new Controller(v, m);
        
        v.start();
    }
    
}
