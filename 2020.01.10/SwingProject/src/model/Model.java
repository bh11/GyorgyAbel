/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import file.WriteInFile;
import file.ReadFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class Model {
    
    private Controller controller;
    private WriteInFile writeInFile = new WriteInFile();
    private ReadFile fileRead = new ReadFile();
    private List<String> actionList = new ArrayList<>();
    
    public void setController(Controller c) {
        this.controller = c;
    }
    
    public void addElementsForList(String str){
        actionList.add(str);
        ListForSaver();
    }
    
    public void ListForSaver(){
        try {
            writeInFile.writeToFile(actionList);
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List getList(){
        return fileRead.readFromFile();
    }

    public List<String> getActionList() {
        return actionList;
    }

    public void setActionList(List<String> actionList) {
        this.actionList = actionList;
    }

    @Override
    public String toString() {
        return "Model{" + actionList + ": Button pressed" +'}';
    }
}
