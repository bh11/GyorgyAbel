/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import enums.TypeOfProducts;
import interfaces.Weight;

/**
 *
 * @author george
 */
public class Beauty extends AbstractProducts implements Weight{

    public Beauty(int barCode, TypeOfProducts type, String manufacturer, int price) {
        super(barCode, type, manufacturer, price);
    }

    @Override
    public void weight() {
        System.out.println("I have wieght!");
    }
    
}
