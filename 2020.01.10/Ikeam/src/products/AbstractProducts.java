/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import enums.TypeOfProducts;
import java.time.LocalDate;

/**
 *
 * @author george
 */
public class AbstractProducts implements Comparable<AbstractProducts>{
    
    private int barCode;
    private TypeOfProducts type;
    private String manufacturer;
    private int price;
    private String registeredDate = LocalDate.now().toString();

    public AbstractProducts(int barCode, TypeOfProducts type, String manufacturer, int price) {
        this.barCode = barCode;
        this.type = type;
        this.manufacturer = manufacturer;
        this.price = price;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    public TypeOfProducts getType() {
        return type;
    }

    public void setType(TypeOfProducts type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(String registeredDate) {
        this.registeredDate = registeredDate;
    }

    

    @Override
    public int compareTo(AbstractProducts barCode) {
        return this.barCode - barCode.getBarCode();
    }

    @Override
    public String toString() {
        return "AbstractProducts{" + "barCode=" + barCode + ", type=" + type + ", manufacturer=" + manufacturer + ", price=" + price + ", registeredDate=" + registeredDate + '}';
    }
}
