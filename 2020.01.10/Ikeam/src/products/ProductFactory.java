/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import enums.TypeOfProducts;

/**
 *
 * @author george
 */
public class ProductFactory {
    
    public static AbstractProducts create(String [] parameters){
          
        int barCode = Integer.parseInt(parameters[0]);
        TypeOfProducts type = TypeOfProducts.CHEAP.valueOf(parameters[1].toUpperCase());
        String manufacture = parameters[2];
        int price = Integer.parseInt(parameters[3]);
        String countryType = parameters[4];
        
        if ("beauty".equals(countryType.toLowerCase())) {
            return new Beauty(barCode, type, manufacture, price);
        }
        
        if ("entertainment".equals(countryType.toLowerCase())) {
            return new Entertainment(barCode, type, manufacture, price);
        }
        
        if ("kitchen".equals(countryType.toLowerCase())) {
            return new Kitchen(barCode, type, manufacture, price);
        }  
        return null;
    }
}
