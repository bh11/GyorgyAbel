/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import enums.TypeOfProducts;
import exception.TurnOnException;
import interfaces.TurnOnProduct;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class Entertainment extends AbstractProducts implements TurnOnProduct{
    
    private int counter = 3;

    public Entertainment(int barCode, TypeOfProducts type, String manufacturer, int price) {
        super(barCode, type, manufacturer, price);
    }
    
    public void transport() throws TurnOnException{
        if (counter > 5) {
            throw new TurnOnException();
        }
        System.out.println("You can transport it, because it hasn't been frozen so far.");
    }

    @Override
    public int turnOnProduct(){
        if (counter > 5) {
            try {
                throw new TurnOnException();
            } catch (TurnOnException ex) {
                Logger.getLogger(Entertainment.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        counter++;
        return counter;
    }
    
}
