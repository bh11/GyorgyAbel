/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import enums.TypeOfProducts;
import interfaces.Raise;
import interfaces.TurnOnProduct;

/**
 *
 * @author george
 */
public class Kitchen extends AbstractProducts implements Raise, TurnOnProduct{

    public Kitchen(int barCode, TypeOfProducts type, String manufacturer, int price) {
        super(barCode, type, manufacturer, price);
    }

    @Override
    public void canRaiseIt() {
        System.out.println("You raise me.");
    }

    @Override
    public int turnOnProduct() {
        System.out.println("You can turn on me, but I will be crashed at the fifth time.");
        return 0;
    }

    
}
