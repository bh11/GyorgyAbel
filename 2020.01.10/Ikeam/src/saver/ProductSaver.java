/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saver;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class ProductSaver {
    
    private static final String FILENAME = "product.ser";
    
    public void serializationProduct(List list){
        try (FileOutputStream fs = new FileOutputStream(FILENAME) ){
             ObjectOutputStream ou = new ObjectOutputStream(fs);
             ou.writeObject(list);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductSaver.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductSaver.class.getName()).log(Level.SEVERE,null, ex);
        }
    }
}
