/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import store.StoreDatas;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import saver.ProductSaver;

/**
 *
 * @author george
 */
public class ConsoleReader {

    private static final String REPORT = "report";
    private static final String REMOVE = "remove";
    private StoreDatas store = new StoreDatas();
    
    public void read() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) { 
            
            while (true) {
                System.out.println("Kérem adja meg a terméket szóközzel elválasztva: ");
                String line = br.readLine();                    
                
                
//                234234 cheap philips 3111 kitchen
                
                if (REPORT.equalsIgnoreCase(line) || line == null) {
                    store.report();
                    break;
                }
                
                store.splitInputDatas(line);
                
                if (REMOVE.equalsIgnoreCase(line)) {
                    System.out.println("Kérem adja meg a vonakódját az eltávolítandó terméknek:");
                        Scanner sc= new Scanner(System.in);
                        int removeBarCode = sc.nextInt();
                        if (sc.hasNextInt()) {
                            store.removeProduct(removeBarCode);
                        }
                }
                store.splitInputDatas(line);
            }
        } catch (IOException ex) {  
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE,null, ex);  
        }
    }
}
