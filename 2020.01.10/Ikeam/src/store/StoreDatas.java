/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import products.AbstractProducts;
import products.ProductFactory;
import java.util.ArrayList;
import java.util.List;
import report.Report;
import saver.ProductSaver;

/**
 *
 * @author george
 */
public class StoreDatas {
    
    private static final String DELIMITER = " ";
    
    private final List <AbstractProducts> store = new ArrayList<>();
    private Report report = new Report();
    private ProductSaver save = new ProductSaver();
    
    public void splitInputDatas(String line){
        String[] parameters = line.split(DELIMITER);
        AbstractProducts product = ProductFactory.create(parameters);
        store.add(product);
        //save.serializationProduct(store);
    }
    
    public void removeProduct(Integer code){
        
        
    }
    
    public void report(){
        report.report(store);
    }
    
    public void getProductToSave(){
        save.serializationProduct(store);
    }
}
