/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.util.List;
import products.AbstractProducts;

/**
 *
 * @author george
 */
public class Report {
    
    public void report (List<AbstractProducts> store){
        
        System.out.println("\nNumber of save products: " + numberOfSaveProducts(store));
        
    }
    
    private long numberOfSaveProducts (List<AbstractProducts> store){
        long numberOfProducts =        
            store.stream()
                 .count();
        
        return numberOfProducts;
    }
}
