<%-- 
    Document   : login
    Created on : 2020.02.07., 19:24:29
    Author     : threnko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%=session.getAttribute("error_message")%>
        <p>${error_message}</p>
        <form action="/LoginExample/LoginServlet" method="POST">
            <input type="text" name="user_name" value="" />
            <input type="password" name="pwd" value="" />
            <input type="submit" value="Login" />
        </form>
    </body>
</html>

