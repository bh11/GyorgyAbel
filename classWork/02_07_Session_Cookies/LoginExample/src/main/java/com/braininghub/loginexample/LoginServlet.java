package com.braininghub.loginexample;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //request.getSession().setAttribute("belepve", "hollo");
        //request.getSession().setAttribute(name, request);
        //request.getSession().invalidate();
        //response.addCookie(new Cookie("teszt", "elek"));
        //request.getSession().setAttribute("error_message", null);
        if(USERNAME.equals(request.getParameter("user_name")) && PASSWORD.equals(request.getParameter("pwd"))) {
            request.getSession().setAttribute("logged_in", true);
            request.getSession().setAttribute("user_name", request.getParameter("user_name"));
            request.getRequestDispatcher("welcome.jsp").forward(request, response);
        } else {
            request.getSession().setAttribute("error_message", "Wrong username or pwd!!! pls. use admin/admin");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
