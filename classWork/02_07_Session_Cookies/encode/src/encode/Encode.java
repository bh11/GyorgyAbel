/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encode;

/**
 *
 * @author george
 */
public class Encode {
    
    private static String encode(String text) {
     StringBuilder b = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            b.append(c += c + i);
        }
        return b.reverse().toString();
    }
    
    static String decode(String encodedMessage) {
        String reverseEncodedMessage = new StringBuilder(encodedMessage).reverse().toString();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < reverseEncodedMessage.length(); i++) {
            char c = reverseEncodedMessage.charAt(i);
            sb.append((char)((c - i)/2));
        }
        return sb.toString();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String text = decode(encode("cica"));
        System.out.println(text);
    }
    
}
