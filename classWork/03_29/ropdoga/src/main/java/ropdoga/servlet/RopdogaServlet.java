/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ropdoga.servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ropdoga.service.RopdogaService;

/**
 *
 * @author george
 */
@WebServlet(name = "RopdogaServlet", urlPatterns = {"/RopdogaServlet"})
public class RopdogaServlet extends HttpServlet {

    @Inject
    private RopdogaService service;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String birthOfYear = request.getParameter("birthOfYear");
        String result = service.getLastTwoCharacter(birthOfYear);
        
        request.setAttribute("bithOfYearh", result);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
