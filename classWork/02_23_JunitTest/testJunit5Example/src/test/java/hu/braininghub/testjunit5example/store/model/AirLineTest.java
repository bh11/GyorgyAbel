/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.testjunit5example.store.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author george
 */
class AirLineTest {
    
    private Airline createAirLine(Airline.Airlines airline){
        return new Airline(airline, "DUMMY");
    }
    
    @Test
    void testisHungarianAirlineWithWizzAir(){
        //Given
        Airline underTest = createAirLine(Airline.Airlines.WIZZ_AIR);
        
        //When
        boolean isHungary = underTest.isHungarianAirline();
        
        //Than
        assertTrue(isHungary);
    }
    
    @Test
    void testisHungarianAirlineWithBerlinAir(){
        //Given
        Airline underTest = createAirLine(Airline.Airlines.BERLIN_AIR);
        
        //When
        boolean isHungary = underTest.isHungarianAirline();
        
        //Than
        assertFalse(isHungary);
    }
    
    @Test
    void testisHungarianAirlineWithMalevAir(){
        //Given
        Airline underTest = createAirLine(Airline.Airlines.MALEV);
        
        //When
        boolean isHungary = underTest.isHungarianAirline();
        
        //Than
        assertTrue(isHungary);
    }
    
    @Test
    void testisHungarianAirlineWithWienAir(){
        //Given
        Airline underTest = createAirLine(Airline.Airlines.WIEN_AIR);
        
        //When
        boolean isHungary = underTest.isHungarianAirline();
        
        //Than
        assertFalse(isHungary);
    }
    
    @Test
    void testisHungarianAirlineWithBudapestAir(){
        //Given
        Airline underTest = createAirLine(Airline.Airlines.BUDAPEST_AIR);
        
        //When
        boolean isHungary = underTest.isHungarianAirline();
        
        //Than
        assertTrue(isHungary);
    }  
}
