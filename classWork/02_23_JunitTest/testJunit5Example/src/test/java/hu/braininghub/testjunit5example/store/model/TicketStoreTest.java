/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.testjunit5example.store.model;

import hu.braininghub.testjunit5example.store.TicketStore;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author george
 */
@ExtendWith(MockitoExtension.class)
public class TicketStoreTest {
    
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Ticket ticket;
    
    private TicketStore underTest;
    
    @BeforeEach
    void init(){
        underTest = new TicketStore();
    }
    
    @DisplayName("Should get 0 when empty list is used")
    @Test
    void testgetNumberOfStoredHungarianAirlinesWithEmptyList(){
        //Given
        
        //When
        long result = underTest.getNumberOfStoredHungarianAirlines();
        
        //Then
        assertEquals(0L, result);
    }
    
    @Test
    void testgetNumberOfStoredHungarianAirlinesWithNonNun(){
        //Given
        underTest.add(ticket);
        when(ticket.getOwner().isHungarianAirline()).thenReturn(false);
        
        //When
        long result = underTest.getNumberOfStoredHungarianAirlines();
        
        //Then
        assertEquals(0L, result);
    }
    
    @Test
    void testgetNumberOfStoredHungarianAirlinesWith1Hun(){
        //Given
        underTest.add(ticket);
        when(ticket.getOwner().isHungarianAirline()).thenReturn(true);
        
        //When
        long result = underTest.getNumberOfStoredHungarianAirlines();
        
        //Then
        assertEquals(1L, result);
    }
    
    @Test
    void testgetNumberOfStoredHungarianAirlinesWith2Hun(){
        //Given
        underTest.add(ticket);
        underTest.add(ticket);
        when(ticket.getOwner().isHungarianAirline()).thenReturn(true, true);
        
        //When
        long result = underTest.getNumberOfStoredHungarianAirlines();
        
        //Then
        assertEquals(2L, result);
    }
}
