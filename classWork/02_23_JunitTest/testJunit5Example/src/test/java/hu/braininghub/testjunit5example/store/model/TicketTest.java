/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.testjunit5example.store.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author george
 */
@ExtendWith(MockitoExtension.class)
public class TicketTest {
    
    @Mock
    Airline airline;
    
    @InjectMocks
    private Ticket underTest;
    
//    @BeforeEach
//    void given(){
//        uderTest = new Ticket(airline, "A", "B");
//    }
    
    @Test
    void shouldGetLocationReturnBudapest(){
        //Given
        
        when(airline.isHungarianAirline()).thenReturn(true);
        
        //When
        String location = underTest.getLocation();
        
        //Then
        assertEquals("BUDAPEST", location);
    }
    
    @Test
    void shouldGetLocationReturnLondon(){
        //Given
        
        when(airline.isHungarianAirline()).thenReturn(false);
        
        //When
        String location = underTest.getLocation();
        
        //Then
        assertEquals("LONDON", location);
    }
    
//    @Test
//    void testDefaultConstructor(){
//        //Given      
//        
//        //When
//      
//        //Then
//        assertAll("default constructor",
//            () -> assertEquals("BUD", underTest.getOrigin()),
//            () -> assertEquals("FRA", underTest.getDestination()));
//    }
}
