package hu.braininghub.hello.rest;

import hu.braininghub.hello.dto.Hello;
import hu.braininghub.hello.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.sun.deploy.services.ServiceManager.service;

@RestController
@RequestMapping(value = "/hello")
public class HelloController {

    @Autowired
    private HelloService service;

    @RequestMapping(method = RequestMethod.GET)
    public String hello(){
        return "Hello World";
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity helloByName(){
        return ResponseEntity
                .ok(new Hello(service.getName()));
    }
}
