package hu.braininghub.hello;

import hu.braininghub.hello.service.HelloService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloConfig {

    @Bean
    public HelloService helloService(){
        return new HelloService();
    }

}

class A{

    void m(){
        System.out.println("macska");
    }
}

class AProxy extends A{

    @Override
    void m() {
        if (true == false){
            super.m();
        }
    }
}
