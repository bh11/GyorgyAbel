/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author george
 */
public class Humans {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        MyPair<String, Worker> p1 = new MyPair<>("Alma", new Worker("10", "Józsi"));
        MyPair<String, Worker> p2 = new MyPair<>("Alma2", new Worker("13", "Béla"));
        
        
        System.out.println(p1.getClass());
        System.out.println(p2.getClass());
        
        List <Number>  l1 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
      //List           l2 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9)); //ez itt egy Object
        List <Integer> l3 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        List <Object>  l4 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        
        //m5(l3);
    }
    
    public static void m1 (List l){ //Olyan mintha ez lenne (List<?> l)
        //l1, l3, l4
    }
    
    public static void m2(List <Object> l){
        //l4
    }
    public static void m3(List <Number> l){
        //l1
    }
    public static void m4(List <?> l){
        //l1, l3, l4
    }
    public static void m5(List <? extends Number> l){
        //l1, l3, l4
    }
    public static void add(List <? super Number> l, Integer a){ // ide az extends nem jó!!!!
        l.add(a);
    }
    
}
