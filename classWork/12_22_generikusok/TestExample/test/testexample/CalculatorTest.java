/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testexample;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author george
 */
public class CalculatorTest {
    
    @Before
    public void runBeforeAnyMethod(){
        System.out.println("runBeforeAnyMethod");
    }
    
    @Test
    public void testAdd() {
        //Given (beállítjuk a környezetet)
        
        int number1 = 10;
        int number2 = 25;
        
        Calculator calc = new Calculator(number1, number2);
        
        //When (megtörténik az esemény)
        
        int result = calc.add();
        
        //Then (mi lett a hatása az eseménynek)
        Assert.assertEquals(number1 + number2, result);
    }
    
    @Test
    public void testSubstract(){
        //Given (beállítjuk a környezetet)
        
        int number1 = 10;
        int number2 = 25;
        
        Calculator calc = new Calculator(number1, number2);
        
        //When (megtörténik az esemény)
        
        int result = calc.substract();
        
        //Then (mi lett a hatása az eseménynek)
        Assert.assertEquals(number1 - number2, result);
    }
    
    @Test
    public void testMultiply(){
        //Given (beállítjuk a környezetet)
        
        int number1 = 10;
        int number2 = 25;
        
        Calculator calc = new Calculator(number1, number2);
        
        //When (megtörténik az esemény)
        
        int result = calc.multiply();
        
        //Then (mi lett a hatása az eseménynek)
        Assert.assertEquals(number1 * number2, result);
    }
}
