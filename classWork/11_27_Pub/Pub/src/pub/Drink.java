/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pub;

/**
 *
 * @author george
 */
public class Drink{
    
    private int sizeInDeciliter;
    private int availableWaterInDeciliter;
    private int availableWienInDeciliter;
    
    public Drink (int availableWaterInDeciliter, int availableWienInDeciliter){
        this.availableWaterInDeciliter = availableWaterInDeciliter;
        this.availableWienInDeciliter = availableWienInDeciliter;
        this.sizeInDeciliter = this.availableWaterInDeciliter + this.availableWienInDeciliter;
    }
    
    public void alcoholQuantityInPersent(){
        
    }

    public int getSizeInDeciliter() {
        return sizeInDeciliter;
    }

    public void setSizeInDeciliter(int sizeInDeciliter) {
        this.sizeInDeciliter = sizeInDeciliter;
    }

    public int getAvailableWaterInDeciliter() {
        return availableWaterInDeciliter;
    }

    public void setAvailableWaterInDeciliter(int availableWaterInDeciliter) {
        this.availableWaterInDeciliter = availableWaterInDeciliter;
    }

    public int getAvailableWienInDeciliter() {
        return availableWienInDeciliter;
    }

    public void setAvailableWienInDeciliter(int availableWienInDeciliter) {
        this.availableWienInDeciliter = availableWienInDeciliter;
    }

    @Override
    public String toString() {
        return "Drink has{" + "sizeInDeciliter=" + sizeInDeciliter + ", availableWaterinDeciliter=" + availableWaterInDeciliter + ", availableWieninDeciliter=" + availableWienInDeciliter + '}';
    }
    
    
}
