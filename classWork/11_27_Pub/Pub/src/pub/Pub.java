/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pub;

/**
 *
 * @author george
 */
public class Pub {
    
    private int availableWaterInDeciliter;
    private int availableWienInDeciliter;
    
    public Pub(int availableWaterInDeciliter, int availableWienInDeciliter){
        this.availableWaterInDeciliter = availableWaterInDeciliter;
        this.availableWienInDeciliter = availableWienInDeciliter;
    }
    
    public void  serty(){
        
    }

    public int getAvailableWaterInDeciliter() {
        return availableWaterInDeciliter;
    }

    public void setAvailableWaterInDeciliter(int availableWaterinDeciliter) {
        this.availableWaterInDeciliter = availableWaterinDeciliter;
    }

    public int getAvailableWienInDeciliter() {
        return availableWienInDeciliter;
    }

    public void setAvailableWienInDeciliter(int availableWieninDeciliter) {
        this.availableWienInDeciliter = availableWieninDeciliter;
    }

    @Override
    public String toString() {
        return "Pub has{" + "availableWaterinDeciliter=" + availableWaterInDeciliter + ", availableWieninDeciliter=" + availableWienInDeciliter + '}';
    }
}
