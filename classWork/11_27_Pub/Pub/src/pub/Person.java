/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pub;

/**
 *
 * @author george
 */
public class Person{
    
    private int age;
    private int amountOfMoney;
    private int amountOfDrinkAlcohol;

    public Person (int age, int amountOfMoney, int amountOfDrinkAlcohol){
        this.age = age;
        this.amountOfMoney = amountOfMoney;
        this.amountOfDrinkAlcohol = amountOfDrinkAlcohol;
    }
    
    public boolean checkAge(){
        return this.age > 18;
    }
    
    public boolean checkAmountOfDrinkAlcohol(){
        return this.amountOfDrinkAlcohol > 40;
    }
    
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public int getAmountOfDrinkAlcohol() {
        return amountOfDrinkAlcohol;
    }

    public void setAmountOfDrinkAlcohol(int amountOfDrinkAlcohol) {
        this.amountOfDrinkAlcohol = amountOfDrinkAlcohol;
    }

    @Override
    public String toString() {
        return "Person is{" + "age=" + age + ", amountOfMoney=" + amountOfMoney + ", amountOfDrinkAlcohol=" + amountOfDrinkAlcohol + '}';
    }  
}
