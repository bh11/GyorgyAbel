/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package set_list;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class Set_List {
    
    public static boolean isPrime(int n){
        if (n <= 1) return false;
        
        for (int i = 2; i < Math.sqrt(n); i++) {
            if(n % i == 0) return false;
        }
        
        return true;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Alapvetően listákat akkor használjuk, ha feladatban nem tudjuk hány elem kell.
        
        List<Integer> primeNumbers = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            if (isPrime(i)) {
                primeNumbers.add(i);
            }
        }
        System.out.println(primeNumbers);
        
        for(int n : primeNumbers){
            System.out.println(n);
        }
        
        for (int i = 0; i < primeNumbers.size(); i++) {
            System.out.println(primeNumbers.get(i));
        }
    }
    
}
