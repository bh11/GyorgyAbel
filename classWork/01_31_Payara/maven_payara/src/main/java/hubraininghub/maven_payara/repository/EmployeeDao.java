/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.maven_payara.repository;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.sql.DataSource;

/**
 *
 * @author george
 */

@Singleton
public class EmployeeDao {
    
    @Resource(lookup = "resource1")
    private DataSource ds;
    
    @PostConstruct
    public void init(){
        System.out.println("It has been inicialized!");
    }
    
    @PreDestroy
    public void destroy(){
        System.out.println("It has been destroyed!");
    }
    
    public List<String> getNames(){
        List <String> names = new ArrayList<>();
        
        try (Statement stm = ds.getConnection().createStatement()) {
            
            String sql = "select * from employees limit 10";
            
            ResultSet rs = stm.executeQuery(sql);
            
            while(rs.next()){
                names.add(rs.getString("first_name"));
            }
            
        } catch (Exception e) {
        }
        
        return names;
    }
    
}
