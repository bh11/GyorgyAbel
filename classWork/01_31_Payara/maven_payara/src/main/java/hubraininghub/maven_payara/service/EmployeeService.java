/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.maven_payara.service;

import hubraininghub.maven_payara.repository.EmployeeDao;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author george
 */

@Stateless
public class EmployeeService {
    
    @Inject
    private EmployeeDao dao;
    
    public List<String> getNames(){
    
        try {
            
            return dao.getNames();
            
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
    
}
