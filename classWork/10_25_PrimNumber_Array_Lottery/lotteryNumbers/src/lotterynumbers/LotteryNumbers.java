/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lotterynumbers;

/**
 *
 * @author george
 */
public class LotteryNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int[] numbers = new int[5]; //5 elemű tömb amiben tároljuk az elemeket
        int countOfNumber = 0; //hány szám van már meg
        int counter = 0;

        while (countOfNumber < 5) { //megvan-e az 5 szám
            counter++;
            int rndNumber = (int) (Math.random() * 90 + 1); // gnerálok számot 1 - 90-ig

            boolean containInNumber = false; // segédváltozó

            for (int i = 0; i < countOfNumber; i++) { //megnézem tartalmazza-e
                if (numbers[i] == rndNumber) {
                    containInNumber = true;
                }
            }
            if (!containInNumber) { //ha nem volt benne beleteszem a tömbe
                numbers[countOfNumber] = rndNumber;
                countOfNumber++;
            }
        }
        
        System.out.println("counter: " + (counter - 5)); // ez nézi van-e ismétlődés ha 5-nél több, akkor volt ismétlődés, vagy levonok 5-öt
        System.out.println("számok: " );
        
        for (int i = 0; i < numbers.length; i++) { // majd kiíratom
            System.out.println(" " + numbers[i]);
        }

        /*for (int i = 0; i < numbers.length; i++) { //ezzel az a baj hogy ugyanazok az értékek is lehetnek
            numbers[i] = (int) (Math.random() * 90 + 1);
        }
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(" " + numbers[i]);

        }*/

    }

}
