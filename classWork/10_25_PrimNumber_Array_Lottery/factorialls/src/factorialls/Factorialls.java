/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorialls;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class Factorialls {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);
        System.out.println("Adjon meg egy számot: ");
        int number = sc.nextInt();
        int fact = 1;
        for (int i = 1; i < number; i++) {
            fact *= i;
        }
        System.out.println("Faktoriális: " + fact);
    }

}
