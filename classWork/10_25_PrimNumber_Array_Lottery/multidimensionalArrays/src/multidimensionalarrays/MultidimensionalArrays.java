/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multidimensionalarrays;

/**
 *
 * @author george
 */
public class MultidimensionalArrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        // többdimenziós tömbök
        
        int [][] multiArray = new int [10][10];
        
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                multiArray[i][j] = i*j;
            }
        }
        
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.print(" " + multiArray[i][j]);
            }
            System.out.println("");
        }
        
        int [][] ma = {{2,3},{0,1}};
    }
    
}
