/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraynextskill;

/**
 *
 * @author george
 */
public class ArrayNextSkill {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int n = 10;
        int k = n;
        k++;
        System.out.println("n: " + n);
        
        
        int[] numbers = {2, 3, 4}; //ha egyenlővé teszem a két tömböt, akkor a másolaton módosítok, akkor az eredetin is módosítani fog!!!
        int[] karray = numbers;
        karray[1]++;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(karray[i] + "-" +numbers[i]);
        }
        
        karray = new int[numbers.length]; //itt másolom le a tömböt, majd új memória területre kerül, és ha módosítom a másolatot akkor az eredeti érintetlen marad
        for (int i = 0; i < numbers.length; i++) {
            karray[i] = numbers[i];
        }
        
        karray[1]++;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(karray[i] + "-" +numbers[i]);
        }
        
    }
    
}
