/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1to10randomnumbers;

/**
 *
 * @author george
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[] numbers = new int[10]; //10 elemű tömb amiben tároljuk az elemeket
        int countOfNumber = 0; //hány szám van már meg
        int counter = 0;

        while (countOfNumber < 10) { //megvan-e az 10 szám
            counter++;
            int rndNumber = (int) (Math.random() * 10 + 1); // gnerálok számot 1 - 10-ig

            boolean containInNumber = false; // segédváltozó

            for (int i = 0; i < countOfNumber; i++) { //megnézem tartalmazza-e
                if (numbers[i] == rndNumber) {
                    containInNumber = true;
                }
            }
            if (!containInNumber) { //ha nem volt benne beleteszem a tömbe
                numbers[countOfNumber] = rndNumber;
                countOfNumber++;
            }
        }

        System.out.println("counter: " + (counter - 5)); // ez nézi van-e ismétlődés ha 5-nél több, akkor volt ismétlődés, vagy levonok 5-öt
        System.out.println("számok: ");

        for (int i = 0; i < numbers.length; i++) { // majd kiíratom
            System.out.print(" " + numbers[i]);
        }
        System.out.println("");
    }
}
