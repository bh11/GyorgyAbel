/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primenumber;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class PrimeNumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);
        
        System.out.println("Kérem adjon meg egy számot: ");
        
        int number = sc.nextInt();
        
        boolean isPrime = true;
        
        for (int i = 2; i < Math.sqrt(number); i++) {
            if (number % i == 0) {
                isPrime = false;
            }
        }

        if (!isPrime || number == 1) {
            System.out.println("nem prím");
        } else {
            System.out.println("prím");
        }
    }

}
