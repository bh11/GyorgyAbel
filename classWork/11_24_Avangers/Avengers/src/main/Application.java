/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import hero.AbstractHero;
import hero.BornOnEarth;
import hero.IdentityCard;
import hero.NotBornOnEarth;
import stone.AbstractStone;
import stone.Soul;
import stone.Time;

/**
 *
 * @author george
 */
public class Application {
    
    public static void main(String[] args) {
        Soul soul = new Soul();
        Time time = new Time();
        
        AbstractHero[] heroes = new AbstractHero[5];
        
        heroes[0] = new BornOnEarth("IronMan", 10, soul, new IdentityCard());
        heroes[1] = new BornOnEarth("AntMan", 8, soul, new IdentityCard());
        heroes[2] = new BornOnEarth("Wasp", -10, soul, new IdentityCard());
        heroes[3] = new NotBornOnEarth("Gamora", 55, time);
        heroes[4] = new NotBornOnEarth("Thanos", 83, time);
        
        for (int i = 0; i < heroes.length; i++) {
            System.out.println(heroes[i]);
        }
    }
    
}
