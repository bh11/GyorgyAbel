/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.mysql.jdbc;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author george
 */
public class SwingView extends JFrame{
    
    private JTextArea result = new JTextArea(20,20);
    private JScrollPane scroll = new JScrollPane(result);
    private JButton first = new JButton("1");
    private JButton second = new JButton("2");
    private JButton third = new JButton("3");
    private JButton fourth = new JButton("4");
    private JButton fifth = new JButton("5");
    
    HrDao hr = new HrDao();
    
    public void buildWindow() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private JPanel buildNorth() {
        JPanel jp = new JPanel();
        jp.add(first);
        jp.add(second);
        jp.add(third);
        jp.add(fourth);
        jp.add(fifth);
        
        first.addActionListener(p -> result.setText(hr.findEmployeeWithMaxSalary()));
        second.addActionListener(p -> result.setText(hr.mostEmployeeInDepartment()));
        third.addActionListener(p -> result.setText(hr.employeeWithMoreThanAvgSalary()));
        fourth.addActionListener(p -> result.setText(hr.employeeHireDateAfter19900101()));
        fifth.addActionListener(p -> result.setText(hr.jobTitleContainClerkWithOrder()));

        return jp;
    }
    
    private JPanel buildCenter(){
        result.setEditable(false);
        JPanel jp = new JPanel();
        jp.add(scroll);
        
        return jp;
    }
}
