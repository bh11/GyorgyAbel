<%-- 
    Document   : studentDetail
    Created on : 01-Mar-2020, 14:29:35
    Author     : george
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Details</h1>
        <c:set var="student" value='${requestScope["student"]}' />
        
        <table class="table table-hover table-dark">
            <tr>
                <td>
                    Student name:
                </td>
                <td>
                    <c:out value="${student.name}"/>
                </td>
            </tr>
            <tr>
                <td>
                    Neptun id of Student:
                </td>
                <td>
                    <c:out value="${student.neptunId}"/>
                </td>
            </tr>
            <tr>
                <td>
                    Student's birthday:
                </td>
                <td>
                    <c:out value="${student.birthday}"/>
                </td>
            </tr>
            <c:forEach var="s" items="${student.subject}">
                <tr>
                    <td>
                        Name of subject:
                    </td>
                    <td>
                        <c:out value="${s.name}"/>
                    </td>
                </tr> 
                <tr>
                    <td>
                        Description of subject:
                    </td>
                    <td>
                        <c:out value="${s.description}"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
