/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.repository;

import hubraninghub.school.repository.entity.Student;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author george
 */
@Singleton
@LocalBean
public class StudentDao implements CrudeRepository<Student, Integer>{
    
    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Student> FindAll() {
        return em.createQuery("SELECT s FROM Student s") 
                 .getResultList();
        // itt számít hogy a Student kis vagy nagybetű
    }
    
    @Override
    public Optional<Student> findById(Integer id) {
        try {
            
        Student s = (Student) em.createQuery("SELECT s FROM Student s WHERE s.id = :id")
            .setParameter("id", id)
            .getSingleResult();
        
        return Optional.of(s);
            
        } catch (Exception e) {
            
            return Optional.empty();
            
        }
    }

    @Override
    public void deleteById(Integer id) {
        Student s = em.find(Student.class, id);
        
        if (null != s) {
            em.remove(s);
        }
    }

    @Override
    public void save(Student entity) {
        em.persist(entity);
    }

    @Override
    public void update(Student entity) {
        em.merge(entity); //itt számít a cascade
    }

    @Override
    public int count() {
        return em.createQuery("SELECT s FROM Student s") 
                 .getResultList().size();
    }
}
