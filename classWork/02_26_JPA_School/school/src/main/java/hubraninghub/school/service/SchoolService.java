/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.service;

import hubraninghub.school.dto.StudentDto;
import hubraninghub.school.mapper.StudentMapper;
import hubraninghub.school.repository.StudentDao;
import hubraninghub.school.repository.entity.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author george
 */
@Singleton
public class SchoolService {
    
    @Inject
    private StudentDao dao;
    
    @Inject
    private StudentMapper mapper;
    
    public List<StudentDto> getStudents(){
        Iterable<Student> students = dao.FindAll();
        List<StudentDto> dto = new ArrayList<>();
        
        students.forEach( student -> dto.add(mapper.toDto(student)));
          
        return dto;
    }
    
    public StudentDto getStudentById(int id){
        
        Optional<Student> entity = dao.findById(id);
        
        return mapper.toDto(entity.get());
    }

    public void setDao(StudentDao dao) {
        this.dao = dao;
    }

    public void setMapper(StudentMapper mapper) {
        this.mapper = mapper;
    }   
}
