/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.mapper;

import hubraninghub.school.dto.StudentDto;
import hubraninghub.school.repository.entity.Student;
import java.util.ArrayList;
import javax.ejb.LocalBean;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author george
 */
@Singleton
@LocalBean
public class StudentMapper implements Mapper<Student, StudentDto>{
    
    @Inject
    private SubjectMapper subjectMapper;

    @Override
    public Student toEntity(StudentDto dto) {
        Student s = new Student();
        s.setId(dto.getId());
        s.setName(dto.getName());
        s.setBirthday(dto.getBirthday());
        s.setNeptunId(dto.getNeptunId());
        s.setSubject(new ArrayList<>());
        
//        dto.getSubject().forEach((subjectDto) -> {
//            s.getSubject().add(subjectMapper.toEntity(subjectDto));
//        });
        
        s.setSubject(dto.getSubject()
            .stream()
            .map(subjectMapper::toEntity)
            .collect(Collectors.toList())
        );
        
        return s;
    }

    @Override
    public StudentDto toDto(Student entity) {
        StudentDto s = new StudentDto();
        s.setId(entity.getId());
        s.setName(entity.getName());
        s.setBirthday(entity.getBirthday());
        s.setNeptunId(entity.getNeptunId());
        s.setSubject(new ArrayList<>());
        
        entity.getSubject().forEach((subject) -> {
            s.getSubject().add(subjectMapper.toDto(subject));
        });
        
        return s;
    }  
}
