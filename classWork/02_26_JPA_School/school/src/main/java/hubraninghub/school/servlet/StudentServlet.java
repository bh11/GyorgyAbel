/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.servlet;

import hubraninghub.school.dto.StudentDto;
import hubraninghub.school.service.SchoolService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author george
 */
@WebServlet(name = "StudentServlet", urlPatterns = "/StudentServlet")
public class StudentServlet extends HttpServlet{
    
    private static final String NUMBER_OF_REQUEST = "nrReq";
    
    @Inject
    private SchoolService service;
    
    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException{
        
        HttpSession session = req.getSession();
        
        if (session.getAttribute(NUMBER_OF_REQUEST) == null) {
            
            session.setAttribute(NUMBER_OF_REQUEST, 0);
            
        }else{
            session.setAttribute(NUMBER_OF_REQUEST, (int) session.getAttribute(NUMBER_OF_REQUEST) + 1);
        }
        
        super.service(req, resp);
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException{
        
        List<StudentDto> students = service.getStudents();
        req.setAttribute("students", students);
        req.getRequestDispatcher("/WEB-INF/students.jsp").forward(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        
        int id = Integer.parseInt(req.getParameter("id"));
        StudentDto result = service.getStudentById(id);
        
        req.setAttribute("student", result);
        req.getRequestDispatcher("/WEB-INF/studentDetail.jsp").forward(req, resp);
    }
}
