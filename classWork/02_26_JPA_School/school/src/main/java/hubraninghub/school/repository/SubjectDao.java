/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.repository;

import hubraninghub.school.repository.entity.Subject;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author george
 */
@Singleton
@LocalBean
public class SubjectDao implements CrudeRepository<Subject, Integer>{
    
    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Subject> FindAll() {
        return em.createQuery("SELECT e FROM Subjects e")
                 .getResultList();
    }
    
    @Override
    public Optional<Subject> findById(Integer id) {
        try {
            
            Subject s = (Subject) em.createQuery("SELECT e FROM Sbujects e WHERE e.id = :id")
                          .setParameter("id", id)
                          .getSingleResult();
            return Optional.of(s);
            
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        Subject s = em.find(Subject.class, id);
        
        if (null != s) {
            em.remove(s);
        }
    }

    @Override
    public void save(Subject entity) {
        em.persist(entity);
    }

    @Override
    public void update(Subject entity) {
        em.merge(entity);
    }

    @Override
    public int count() {
        return em.createQuery("SELECT e FROM Subjects e")
                 .getResultList().size();
    }
}
