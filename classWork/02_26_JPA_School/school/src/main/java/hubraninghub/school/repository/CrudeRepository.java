/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.repository;

import java.util.Optional;

/**
 *
 * @author george
 */
public interface CrudeRepository<Entity, ID> {
    
    Iterable<Entity> FindAll(); //de itt lehet Collection, List
    void deleteById(ID id); //void vagy boolean
    void save(Entity entity);
    void update(Entity entity); //lehet itt is boolean, ha sikerült true ha nem false
    Optional<Entity> findById(ID id);
    int count(); //vagy long
    
}
