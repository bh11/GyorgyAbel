/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.mapper;

import hubraninghub.school.dto.SubjectDto;
import hubraninghub.school.repository.entity.Subject;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 *
 * @author george
 */
@Singleton
@LocalBean
public class SubjectMapper implements Mapper<Subject, SubjectDto>{
    
    @Override
    public Subject toEntity(SubjectDto dto) {
        Subject subjectEnt = new Subject();
        subjectEnt.setId(dto.getId());
        subjectEnt.setName(dto.getName());
        subjectEnt.setDescription(dto.getDescription());
        
        return subjectEnt;
    }

    @Override
    public SubjectDto toDto(Subject entity) {
        SubjectDto subjectDto = new SubjectDto();
        subjectDto.setId(entity.getId());
        subjectDto.setName(entity.getName());
        subjectDto.setDescription(entity.getDescription());
        
        return subjectDto;
    }   
}
