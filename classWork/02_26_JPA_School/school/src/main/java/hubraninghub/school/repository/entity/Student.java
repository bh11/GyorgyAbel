/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.school.repository.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author george
 */

@Entity
@Table(name = "Student")
public class Student implements Serializable{
    
    @Id
    @Column(name = "student_id")
    private Integer id;
    
    @Column(name = "student_name")
    private String name;
    
    @Column(name = "neptun_id")
    private String neptunId;
    
    @Column(name = "birthday")
    private Date birthday;
    
    @OneToMany (mappedBy = "student", fetch = FetchType.EAGER, cascade = CascadeType.ALL) 
    //itt jelölöm ki a másik osztályban lévő kapcsolatot
    //Eager esetén betöltődik a subject is
    private List<Subject> subject; //ez a kapcsolat a Subject felé

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeptunId() {
        return neptunId;
    }

    public void setNeptunId(String neptunId) {
        this.neptunId = neptunId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Subject> getSubject() {
        return subject;
    }

    public void setSubject(List<Subject> subject) {
        this.subject = subject;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Student{" + "id=" + id + ", name=" + name + ", neptunId=" + neptunId + ", birthday=" + birthday + ", subject=" + subject + '}';
    }    
}
