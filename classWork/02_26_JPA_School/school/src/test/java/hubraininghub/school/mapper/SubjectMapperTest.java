/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.school.mapper;

import hubraninghub.school.dto.SubjectDto;
import hubraninghub.school.mapper.SubjectMapper;
import hubraninghub.school.repository.entity.Subject;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author s26841
 */
@ExtendWith(MockitoExtension.class)
public class SubjectMapperTest {

    private SubjectDto subjectDto;
    private Subject subjectEntity;
    
    @InjectMocks
    private SubjectMapper underTest;

    @BeforeEach
    void init(){
        subjectDto = new SubjectDto();
        subjectDto.setId(1);
        subjectDto.setName("name of subject");
        subjectDto.setDescription("description of subject");
        
        subjectEntity = new Subject();
        subjectEntity.setId(1);
        subjectEntity.setName("name of subject");
        subjectEntity.setDescription("description of subject");
        subjectEntity.setStudent(null);
    }
    
    @Test
    void testGetStudentEntityWithStudentDtoInput(){
        //Given

        //When
            Subject result = underTest.toEntity(subjectDto);
        //Then
            assertEquals(subjectEntity, result);
    }
    
    @Test
    void testGetStudentDtoWithStudentEntityInput(){
        //GIVEN

        //WHEN
            SubjectDto result = underTest.toDto(subjectEntity);
        //THEN
            assertEquals(subjectDto, result);
    }
}
