/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.school.service;

import hubraninghub.school.dto.StudentDto;
import hubraninghub.school.mapper.StudentMapper;
import hubraninghub.school.repository.StudentDao;
import hubraninghub.school.repository.entity.Student;
import hubraninghub.school.service.SchoolService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author george
 */
@ExtendWith(MockitoExtension.class)
public class SchoolServiceTest {
    
    @Mock
    private StudentDao dao;
    
    @Mock
    private StudentMapper mapper;
    
    private SchoolService underTest;
    
    @BeforeEach
    void init(){
        underTest = new SchoolService();
        underTest.setDao(dao);
        underTest.setMapper(mapper);
    }
    
    @Test
    void testGetStudentsWithEmptyResult(){
        //Given
            when(dao.FindAll()).thenReturn(new ArrayList<>());
        //When
            List<StudentDto> students = underTest.getStudents();
        //Then
            Assertions.assertEquals(new ArrayList<>(), students);
    }
    
    @Test
    void testGetStudentsWithOneItem(){
        //Given
        Student s = mock(Student.class);
        StudentDto dto = mock(StudentDto.class);
        
        when(dao.FindAll()).thenReturn(Arrays.asList(s));
        when(mapper.toDto(s)).thenReturn(dto);
        
        //When
        List<StudentDto> students = underTest.getStudents();
        //Then
        Assertions.assertEquals(Arrays.asList(dto), students);
    }
    
    @Test
    void testGetStudentByIdWithIncorrectId(){
        //Given
        Student s = mock(Student.class);
        StudentDto dto = mock(StudentDto.class);
        
        when(dao.findById(-5)).thenReturn(Optional.of(s));
        when(mapper.toDto(s)).thenReturn(dto);
        
        //When
        StudentDto result = underTest.getStudentById(-5);
        
        //Then
        Assertions.assertEquals(dto,result);
    }
    
    @Test
    void testGetStudentByIdWithCorrectId(){
        //Given
        Student s = mock(Student.class);
        StudentDto dto = mock(StudentDto.class);
        
        when(dao.findById(1)).thenReturn(Optional.of(s));
        when(mapper.toDto(s)).thenReturn(dto);
        
        //When
        StudentDto result = underTest.getStudentById(1);
        
        //Then
        Assertions.assertEquals(dto,result);
    }
}
