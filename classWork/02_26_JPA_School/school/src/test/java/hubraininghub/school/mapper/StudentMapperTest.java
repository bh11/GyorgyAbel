/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.school.mapper;

import hubraninghub.school.dto.StudentDto;
import hubraninghub.school.dto.SubjectDto;
import hubraninghub.school.mapper.StudentMapper;
import hubraninghub.school.mapper.SubjectMapper;
import hubraninghub.school.repository.entity.Student;
import hubraninghub.school.repository.entity.Subject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author george
 */
@ExtendWith(MockitoExtension.class)
public class StudentMapperTest {
    
    private Student studentEntity;
    private StudentDto studentDto;
    private Subject subjectEntity;
    private SubjectDto subjectDto;
    private SubjectDto subjectDtoToList;
    private Subject subjectEntityToList;
    Date date = new Date(2020);
    List<Subject> subjectList = new ArrayList<>();
    List<SubjectDto> subjectDtoList = new ArrayList<>();
    
    @Mock
    private SubjectMapper subjectMapper;

    @InjectMocks
    private StudentMapper underTest;

    @BeforeEach
    void init(){
        studentEntity = new Student();
        studentEntity.setId(1);
        studentEntity.setName("Peti");
        studentEntity.setNeptunId("petike");
        studentEntity.setBirthday(date);
        studentEntity.setSubject(subjectList);
        subjectList.add(subjectEntityToList);

        studentDto = new StudentDto();
        studentDto.setId(1);
        studentDto.setName("Peti");
        studentDto.setNeptunId("petike");
        studentDto.setBirthday(date);
        studentDto.setSubject(subjectDtoList);
        subjectDtoList.add(subjectDtoToList);
    }
    
    @Test
    void testGetStudentEntityWithStudentDtoInput(){
        //Given
            when(subjectMapper.toEntity(subjectDto)).thenReturn(subjectEntity);
        //When
            Student result = underTest.toEntity(studentDto);
        //Then
            assertEquals(studentEntity, result);
    }
    
    @Test
    void testGetStudentDtoWithStudentEntityInput(){
        //GIVEN
            when(subjectMapper.toDto(subjectEntity)).thenReturn(subjectDto);
        //WHEN
            StudentDto result = underTest.toDto(studentEntity);
        //THEN
            assertEquals(studentDto, result);
    }
}
