<%-- 
    Document   : filteredEmployees
    Created on : 05-Feb-2020, 19:23:02
    Author     : george
--%>

<%@page import="java.util.List"%>
<%@page import="hubraininghub.maven_payara.repository.dto.Employee"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Filtered Employees</title>
    </head>
    <body>
        <h1>Filtered Employees</h1>
        <table border="3">
            <%
                List<Employee> employees = (List<Employee>)request.getAttribute("employees");
                
                for (Employee emp : employees) {
                    out.print("<tr><td>"
                        + emp.getFirstName() + "</td><td>"
                        + emp.getLastName() + "</td><td>"
                        + emp.getSalary() + "</td></tr>");
                }
            %>
        </table>
    </body>
</html>
