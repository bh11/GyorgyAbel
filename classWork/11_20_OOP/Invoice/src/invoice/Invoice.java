/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice;

/**
 *
 * @author george
 */
public class Invoice {
    private String serialNumber;
    private double amount;
    
    public Invoice (String serialNumber, double amount){
        this.serialNumber = serialNumber;
        this.amount = amount;
    }
    
    public Invoice (String serialNumber){
        this(serialNumber, 1000);
    }
    
    public void setAmount(int amount){
        this.amount = amount;
    }
}
