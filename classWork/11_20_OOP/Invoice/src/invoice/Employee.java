/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invoice;

/**
 *
 * @author george
 */
public class Employee {
    private String name;
    private String id;
    private Account[] accounts = new Account[2];
    
    public Employee(String name, String id, Account[] accounts){
        this.name = name;
        this.id = id;
        this.accounts = accounts;
    }
    
    public Account[] getAccounts(){
        return this.accounts;
    }

    public void listOfInvoices(){
        for (int i = 0; i < this.accounts.length; i++) {
            System.out.println(this.accounts);
        }
    }
    
}
