/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.maven_payara.service;

import hubraininghub.maven_payara.repository.EmployeeDao;
import hubraininghub.maven_payara.repository.entity.Employees;
import hubraininghub.maven_payara.service.dto.EmployeesDto;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author george
 */

@Stateless
public class EmployeeService {

    @Inject
    private EmployeeDao dao;

    public List<String> getNames() {
        
        return dao.getEmployees().stream()
                  .map(Employees::getFirstName)
                  .collect(Collectors.toList());

    }

    public List<EmployeesDto> getEmployees(String str) {
        List<EmployeesDto> result = new ArrayList<>();

        for (Employees e : dao.findByName(str)) {
            EmployeesDto dto = new EmployeesDto();

            try {

                BeanUtils.copyProperties(dto, e);
                result.add(dto);

            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
}
