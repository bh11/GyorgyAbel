<%-- 
    Document   : deserts
    Created on : 24-Jan-2020, 17:28:04
    Author     : george
--%>

<%@page import="java.util.List"%>
<%@page import="hubraninghub.myfirstapi.Desert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  List<Desert> deserts = (List<Desert>)request.getAttribute("employees"); %> 
        <table border="3" align="center">
            <thead>
                <tr>
                    <th>Name and size</th>
                </tr>
            </thead>
            <tbody>
                <% for (int i = 0; i < deserts.size(); i++) { %>
                <tr>
                    <td><%= deserts.get(i).getName()%></td>
                </tr>
                <% } %>
            </tbody>
        </table>
            
        <div style="margin: auto; width: 50%; border: 5px solid lightskyblue; padding: 10px; background-color: whitesmoke">
            <form method="post" action="ListDesertServlet">
                <fieldset>
                <legend>List Deserts</legend>
                    <input type="submit" value="List Deserts"/>
                </fieldset>
            </form>
        </div>
    </body>
</html>
