/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.myfirstapi;

/**
 *
 * @author george
 */
public class DesertFactory {
    
    Desert deserts;
    private static final String DELIMITER = "|";
    
    public void desertFactory(String desert){
        String[] parameters = desert.split(DELIMITER);
        deserts = new Desert(parameters[0], Integer.parseInt(parameters[1]));
    }

    public Desert getDeserts() {
        return deserts;
    }
}
