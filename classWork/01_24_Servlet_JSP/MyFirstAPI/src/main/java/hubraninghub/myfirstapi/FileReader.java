/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.myfirstapi;

import java.io.BufferedReader;

/**
 *
 * @author george
 */
public class FileReader {
    
    private static final String FILENAME = "deserts.txt";
    private DesertFactory desertFactory = new DesertFactory();
    
    public void readFromFile(){
        try (BufferedReader br = new BufferedReader(new java.io.FileReader(FILENAME))){
            
            String currentLine;
            
            while ((currentLine = br.readLine()) != null) {                
                desertFactory.desertFactory(currentLine);
            }            
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
