/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author george
 */
public class JDBC2 {

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "root";
    private static final String pw = "verbatim";

    public static void main(String[] args) {
        String sql = "select * from countries limit 10";

        try(
            Connection conn = DriverManager.getConnection(URL, user, pw);
                Statement stm = conn.createStatement();
                ResultSet rs = stm.executeQuery(sql);
                ) {

            while (rs.next()) {                    
                System.out.println(rs.getString("country_name"));
            }
        } catch (SQLException ex) {

        }
    }
    
}
