-- Valaha legtöbbet kereső dolgozó neve:
-- select first_name, last_name, salary from hr.employees 
-- where salary = (select max(salary) from hr.employees); 

-- Annak a departmennek a neve, ahol a legtöbben dolgoznak:
-- SELECT B.department_name, count(*) nr
-- FROM hr.employees A INNER JOIN hr.departments B
-- ON A.department_id = B.department_id
-- GROUP BY B.department_name
-- ORDER BY nr DESC
-- LIMIT 1;

-- Akik többet keresnek az átlagnál:
-- select first_name, last_name from hr.employees where salary > (select avg(salary) from hr.employees);

-- Kik léptek be 1990.01.01. után:
-- select first_name, last_name, hire_date from hr.employees where hire_date > 19900101;

-- Rendezett, Cleark szót keresve, job title
-- select distinct job_title from hr.jobs where job_title like "%clerk%" order by job_title asc;

select last_name, first_name, max(salary) from employees;