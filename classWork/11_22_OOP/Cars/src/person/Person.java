/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package person;

/**
 *
 * @author george
 */
public class Person {
//        Írj Ember néven egy osztályt, aminek egy String típusú nev attribútuma (private) van. Rendelkezik equals()
//        és hashCode() metódusokkal is. Ez az osztály az ember package-ben legyen és van egy olyan egy
//        paraméteres konstruktora, ami eltárolja az ember nevét.
//        Hozz létre egy másik osztályt a hallgato csomagban Hallgato néven. A Hallgató osztály leszármazottja az
//        Ember osztálynak. A Hallgato osztályról a következőket tudjuk:
//        van egy String típusú privát attribútuma: neptun
//        van egy egyparaméteres konstruktora, ami egy neptun kódot és a Te nevedet tárolja el. Ebben a
//        konstruktorban maximum 1 = operátort használhatsz.
//        van egy paraméter nélküli konstruktora, amiben a saját neptun kódodat tárolod el.
//        Törekedj a super() és a this() használatára!
    
    private final String name;
    
    public Person(String name){
        
        this.name = name;
        
    }
    
    public String getName(){
        return this.name;
    }
    
    @Override
    public String toString(){
        return this.name;
    }
    
}
