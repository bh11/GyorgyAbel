/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

import person.Person;

/**
 *
 * @author george
 */
public class Student extends Person{
    
    private String neptun;
    
    public Student(String neptun){
        super("Ábel György");
        this.neptun = neptun;
    }
    
    public Student(){
        this("an544k");
    }
    
    public void setNeptun(String neptun){
        this.neptun = neptun;
    }
    
    public String getNeptun(){
        return this.neptun;
    }
    
    @Override
    public String toString(){
        return super.toString() + " - " + neptun;
    }
}
