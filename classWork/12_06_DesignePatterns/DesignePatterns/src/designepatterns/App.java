/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designepatterns;

import designepatterns.dp.creation.Car;
import designepatterns.dp.creation.Car2;
import designepatterns.dp.creation.MySingleton;

/**
 *
 * @author george
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Car c = new Car.CarBuilder("trabi", "feher")
                .setAbs(true)
                .setMaxSpeed(200)
                .setSpoiler(false)
                .build();
        
        Car2 c2 = new Car2("trabi", "feher")
                .setAbs(true)
                .setMaxSpeed(221);
        
        MySingleton ms = MySingleton.getInstance();
        MySingleton ms1 = MySingleton.getInstance();
        if (ms == ms1) {
            System.out.println("ugyanaz");
        }
    }
    
}
