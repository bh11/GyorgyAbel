/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designepatterns.factory;

/**
 *
 * @author george
 */
public class AnimalFactory {
    
    public static AbstractAnimal creat(String type){
    
        if(Cat.equals(type){
            return new Cat();
        }
        if(Dog.equals(type){
            return new Dog();
        }
        if(Snake.equals(type){
            return new Cat();
        }
        
        return null;
        
    }
}
