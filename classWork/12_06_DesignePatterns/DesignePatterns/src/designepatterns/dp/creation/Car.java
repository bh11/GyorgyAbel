/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designepatterns.dp.creation;

/**
 *
 * @author george
 */
public class Car {

    private String model;
    private String color;
    private boolean abs;
    private int maxSpeed;
    private boolean spoiler;

    public static class CarBuilder {

        private String model;
        private String color;
        private boolean abs;
        private int maxSpeed;
        private boolean spoiler;

        public CarBuilder(String type, String color) {
            this.model = type;
            this.color = color;
        }

        public CarBuilder setModel(String model) {
            this.model = model;
            return this;
        }

        public CarBuilder setColor(String color) {
            this.color = color;
            return this;
        }

        public CarBuilder setAbs(boolean abs) {
            this.abs = abs;
            return this;
        }

        public CarBuilder setMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public CarBuilder setSpoiler(boolean spoiler) {
            this.spoiler = spoiler;
            return this;
        }
        
        public Car build(){
            return new Car(this);
        }
    }

    public Car(CarBuilder cb) {
        this.model = cb.model;
        this.color = cb.color;
        this.abs = cb.abs;
        this.spoiler = cb.spoiler;
        this.maxSpeed = cb.maxSpeed;
    }
}
