/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bubblesort;

import java.util.Arrays;

/**
 *
 * @author george
 */
public class BubbleSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int [] testArray = {20, 9, 3, 16, -5, 66};
        int j = testArray.length - 1;
        
        for (int i = 0; i < (testArray.length - j); i++) {
            if (testArray[i] > testArray[i + 1]) {
                int tmp = testArray[i];
                testArray[i] = testArray[i + 1];
                testArray[i + 1] = tmp;
                j--;
            } 
        }
        
        for (int i = 0; i < testArray.length; i++) {
            System.out.print(testArray[i] + " ");
        }
        
        Arrays.sort(testArray);
        System.out.println();
        for (int i = 0; i < testArray.length; i++) {
            System.out.print(testArray[i] + " ");
        }
        
        System.out.println();
        int k = Arrays.binarySearch(testArray, 20);
        System.out.println(k);
    } 
}
