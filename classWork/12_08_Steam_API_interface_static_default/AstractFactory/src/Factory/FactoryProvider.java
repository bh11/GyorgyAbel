/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory;

/**
 *
 * @author george
 */
public class FactoryProvider {
    
    public static AbstractFactory getFactory(String factoryType){
        
        switch(factoryType){
            case "AutoFactory" : return new AutoFactory();
            case "ProductFactory" : return new ProductFactory();
        }
        return null;
    }         
}
