/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory;

/**
 *
 * @author george
 */
public class ProductFactory extends AbstractFactory{

    @Override
    public void create() {
        System.out.println("I make products!");
    }
    
}
