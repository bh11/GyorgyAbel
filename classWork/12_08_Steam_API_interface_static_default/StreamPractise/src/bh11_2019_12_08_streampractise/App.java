package bh11_2019_12_08_streampractise;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    static List<Country> countries = new ArrayList<>();
    
    
    public static void main(String[] args) {
        generateData();      
        printAllCountries();
        System.out.println();
        printDistinctCountries();
        System.out.println();
        printCountriesWhereNameContains("Hungary");
        System.out.println();
        printCountOfCountriesWhereNameContains("Hungary");
        System.out.println();
        printCountryByCode("hu");
        System.out.println();
        sortAllOfCountry();
        System.out.println();
        printContinents();
        System.out.println();
        printCountryNames();
        System.out.println();
        printCountryCodes();
        System.out.println();
        printAllCities();
        System.out.println();
        printAllOfCountriesPopulationLessThanX(2000);
        
        System.out.println("\nkontinens neve alapján legyen abc sorrendben csökkenő");
        printSortedCountriesByContinentDesc();
        System.out.println("\nvárosok darabszáma alapján legyen sorrendben");
        printSortedCountriesByCountOfCities();
        System.out.println("\nkap egy név részletét a városnak, írja ki azokat az országokat ahol ez megtalálható, ne legyen casesensitive");
        printCountryWhichContainsPartOfCityName("Szeg");  
        System.out.println("\nírja ki az országokhoz tartozó méreteket");
        printCountrySizes();
        System.out.println("\nflatMap test");
        testFlatMap();
    }
    
    public static void generateData() {
        Country hungary = new Country("hu", "Hungary", "Europe", 93);
        hungary.addCity(new City("Budapest", 2000));
        hungary.addCity(new City("Szeged", 800));
        hungary.addCity(new City("Nyíregyháza", 200));
        hungary.addCity(new City("Tokaj", 30));
        
        Country spain = new Country("sp", "Span", "Europe", 110);
        spain.addCity(new City("Barcelona", 300));
        spain.addCity(new City("Madrid", 250));
        
        Country thailand = new Country("th", "Thailand", "Asia", 200);
        thailand.addCity(new City("Phuket", 50));
        thailand.addCity(new City("Bangkok", 3000));
        
        countries.add(hungary);
        countries.add(hungary);
        countries.add(spain);
        countries.add(thailand);
    }
    //basic
    public static void printAllCountries() {
        countries
            .stream()
            .forEach((p) -> System.out.println(p));
        //countries
        //.stream()
        //.forEach(System.out::println);
    }
    //basic
    public static void printDistinctCountries() {
        countries
            .stream()
            .distinct()
            .forEach((p) -> System.out.println(p));
    }
    //basic
    public static void printCountriesWhereNameContains(String str) {
        countries
            .stream()
            .filter((p) -> p.getName().contains(str))
            .forEach((country)->System.out.println(country));
    }
    //basic
    public static void printCountOfCountriesWhereNameContains(String str) {
        System.out.println(
            countries
            .stream()
            .filter((p) -> p.getName()
            .contains(str))
            .count());
    }    
    //basic
    public static void printCountryByCode(String code) {
        countries
            .stream()
            .filter((p) -> p.getCode().equals(code))
            .forEach( country -> System.out.println(country));
    }
    //basic
    public static void sortAllOfCountry() {
        countries
            .stream()
            .sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
            .forEach((p) -> System.out.println(p));
        }
    //extra
    public static void printAllCities() {
        countries
            .stream()
            .forEach((country -> country.getCities()
            .stream()
            .forEach((city) -> System.out.println(city))));
//        countries
//            .stream()
//            .map((country) -> country.getCities())
//            .collect(Collectors.toList());
    }
    //basic
    public static void printContinents() {
//        countries
//            .forEach((country) -> System.out.println(country.getContinent()));
        countries
            .stream()
            .map((country) -> country.getContinent())
            .distinct()
            .forEach((p) -> System.out.println(p));
        }
    //basic
    public static void printCountryNames() {
        countries
            .stream()
            .distinct()
            .forEach((country) -> System.out.println(country.getName()));
        }
    //basic
    public static void printCountryCodes() {
        countries
            .stream()
            .distinct()
            .forEach((country) -> System.out.println(country.getCode()));
    }    
    //extra
    public static void printSummOffPopulation() {
//        countries.get(0)
//            .getCities()
//            .stream()
//            .mapToInt((city) -> city.getPopulation()).sum();
        countries
            .stream()
            .mapToInt((country) -> country.getCities()
            .stream()
            .mapToInt((city) -> city.getPopulation())
            .sum());
    }
    //extra
    public static void printSummOfEuropePopulation() {
        countries
            .stream()
            .filter((p) -> p.getContinent().equals("Europe"))
            .mapToInt((country) -> country.getCities()
            .stream()
            .mapToInt((city) -> city.getPopulation())
            .sum());
    }
    //extra
    public static void printAllOfCountriesPopulationLessThanX(int x) {
        countries
            .stream()
            .mapToInt((country) -> country.getCities()
            .stream()
            .mapToInt((city) -> city.getPopulation())
            .filter(pop -> pop < x)
            .sum());
    } 
    
    //HF
    //kontinens neve alapján legyen abc sorrendben csökkenő
    public static void printSortedCountriesByContinentDesc() {
        countries
            .stream()
            .sorted((p1, p2) -> p2.getContinent().compareTo(p1.getContinent()))
            .forEach(p -> System.out.println(p + " " + p.getContinent())); 
    }
    
    //városok darabszáma alapján legyen sorrendben
    public static void printSortedCountriesByCountOfCities() {
        countries
            .stream()
            .sorted((p1,p2) -> (int)(p1.getCities().stream().count() - p2.getCities().stream().count()))
            .forEach(System.out::println);
    }

    
    //kap egy név részletét a városnak, írja ki azokat az országokat ahol ez megtalálható, ne legyen casesensitive
    public static void printCountryWhichContainsPartOfCityName(String partOfCityName) {
        countries
            .stream()
            .filter((p) -> p.getCities()
                .toString()
                .toLowerCase()
                .contains(partOfCityName.toLowerCase()))
            .forEach(System.out::println);
    }

    //írja ki az országokhoz tartozó méreteket
    public static void printCountrySizes() {
        countries
            .stream()
            .distinct()
            .peek(System.out::println)
            .mapToInt((country) -> country.getSize())
            .forEach(System.out::println);
    }
    
    public static void testFlatMap(){
        countries
            .stream()
            .filter(p -> "Europe".equals(p.getContinent()))
            .map(p -> p.getCities())
            .flatMap(cities -> cities.stream())
            .forEach(System.out::println);
    }
}
