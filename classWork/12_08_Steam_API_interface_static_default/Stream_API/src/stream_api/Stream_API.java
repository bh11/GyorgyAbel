/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream_api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author george
 */
public class Stream_API {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Stream.of("a1", "a2", "a3", "a4", "a5")
                .filter(p -> {System.out.println("Filterezett: " + p); return true;})
                .forEach(p -> {System.out.println("forEach: " + p);
                });
                
//        List <String> myList = new ArrayList<>();
//        myList.add("valami");
//        myList.add("valami2");
//        myList.add("anti");
//        myList.add("kap");
//        
//        Stream <String> stream = myList.stream().filter(p -> p.startsWith("a"));
//        stream.filter(p -> p.contains("a")); // ez így runTimeExecption
//        stream.filter(p -> p.endsWith("p"));
//        stream.forEach(p -> System.out.println(p));
        
        List <Integer> myList = new ArrayList <>();
        
        for (int i = 0; i < 200; i++) {
            myList.add(i);
        }
        
        long startTime = System.currentTimeMillis();
        
        myList.stream().forEach(p -> {System.out.println("Stream: " + p);});
        long duration = System.currentTimeMillis() - startTime;
        System.out.println("duration time at Stream: " + duration);
        
        System.out.println();
        
        long startTime2 = System.currentTimeMillis();
        myList.parallelStream().forEach(p -> {System.out.println("parallelStream: " + p);});
        long duration2 = System.currentTimeMillis() - startTime2;
        
        System.out.println("Duration time at parallelStream: " + duration2);
        
        
        
    }
    
}
