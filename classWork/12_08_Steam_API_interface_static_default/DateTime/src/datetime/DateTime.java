/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author george
 */
public class DateTime {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("*************************** Date ************************************");
        LocalDate localDate = LocalDate.now();
        
        System.out.println(localDate);
        
        localDate = LocalDate.of(1848, 3, 15);
        System.out.println(localDate);
        DateTimeFormatter DateTimaFormatter;
        
        localDate = LocalDate.parse("1988.11.20", DateTimeFormatter.ofPattern("yyyy.MM.dd"));
        System.out.println(localDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd")));
        
        System.out.println(localDate.plusDays(10000));
        System.out.println(localDate.plusWeeks(16));
        
        System.out.println(localDate.isLeapYear()); // szökőév
        
        localDate.minus(10, ChronoUnit.MONTHS);
        
        System.out.println(localDate); // 1988.11.20- mert immutable a date
        
        System.out.println("************************ Time ***************************************");
        
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime.format(DateTimeFormatter.ofPattern("HH:mm")));
        
        localTime = LocalTime.of(23, 20);
        System.out.println(localTime);
        
        localTime = LocalTime.parse("23:20");
        System.out.println(localTime);
        
        System.out.println("************************ Date Time ***************************************");
        
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
        LocalDateTime endOfLesson = LocalDateTime.of(2019, 12, 8, 18, 0);
        System.out.println(endOfLesson);
        System.out.println(localDateTime.getMonth());
        
        System.out.println("************************ Time Zones ***************************************");
        
//        for (String Zone : ZoneId.getAvailableZoneIds()) {
//            System.out.println(Zone);
//        }
        
        System.out.println();
        
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime zdt = ZonedDateTime.now(ZoneId.of("America/Puerto_Rico"));
        System.out.println(zdt);

        
        
    }
    
}
