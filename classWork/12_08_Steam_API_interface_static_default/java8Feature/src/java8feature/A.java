/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8feature;

/**
 *
 * @author george
 */
public interface A {
    
    void method1();
    
    static void staticMethod(){
        System.out.println("static method in A");
    }
}
