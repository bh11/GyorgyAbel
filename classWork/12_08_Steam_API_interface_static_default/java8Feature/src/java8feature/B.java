/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8feature;

/**
 *
 * @author george
 */
public interface B extends A{
    
    @Override
    default void method1(){
        System.out.println("do something in B");
    }
    
//    @Override ezt itt nem használhatom mert static az A-ban ez . amethod és akkor elfedi
    static void staticMethod(){ //őt nem lehet felülírni
        System.out.println("so something in static B");
    }
}
