/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package endswith7;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class EndsWith7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner scanner = new Scanner(System.in);

        //Egyik szám beolvasása
        boolean isValidInput = false;
        int number1 = -1;

        do {
            System.out.println("Adjon meg egy egész számot!");
            if (scanner.hasNextInt()) {
                number1 = scanner.nextInt();
                isValidInput = true;

            } else {
                scanner.next();
            }
        } while (!isValidInput);

        //Másik szám beolvasása
        isValidInput = false;
        int number2 = -1;

        do {
            System.out.println("Adjon meg egy egész számot!");
            if (scanner.hasNextInt()) {
                number2 = scanner.nextInt();
                isValidInput = true;

            } else {
                scanner.next();
            }

        } while (!isValidInput);

        //min - max
        int from;
        int to;

        from = Math.min(number1, number2);
        to = Math.max(number1, number2);

        System.out.println("----------- 7-re végződő számok ------------------");
        int i = from;
        while (i <= to) {
            if (i % 10 == 7) {
                System.out.println(i);
            }
            i++;
        }
    }
}
