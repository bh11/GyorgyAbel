/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package october20ascii;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class October20ASCII {

    /**
     * @param args the command line arguments
     */
    /////////////////////////////////////////////////////////////////////
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char value = scanner.next().charAt(0);
        int ascii = (int) value;
        if(ascii <= 122 && ascii >= 97){
            System.out.println("Kisbetű");
        }else if(ascii <= 90 && ascii >= 65){
            System.out.println("Nagybetű");
        }else if(ascii <= 57 && ascii >= 48){
            System.out.println("Szám");
        }else{
            System.out.println("Csak kisbetűt, nagybetűt 0-9 közötti számot fogadok el!");
        }
        
    /////////////////////////////////////////////////////////////////////
        
        int number = scanner.nextInt();
        if(number < 0){
            System.out.println("Hibás bemenet!");
        }
        else if(number <= 200){
            System.out.println("Alföld");
        }else if(number <= 500){
            System.out.println("Dombság");
        }else if(number <= 1500){
            System.out.println("Középhegység");
        }else{
            System.out.println("Hegység");
        }
        
        
    /////////////////////////////////////////////////////////////////////
    
        System.out.println("Számítások elvégzéséhez adja meg az első egész számot: ");
        int firstNumber = scanner.nextInt();
        System.out.println("Számítások elvégzéséhez adja meg a második egész számot: ");
        int secondNumber = scanner.nextInt();
        System.out.println("Számítások elvégzéséhez adja meg a harmadik egész számot: ");
        int thirdNumber = scanner.nextInt();
        
        /*
        if((firstNumber + secondNumber) > thirdNumber && 
           (firstNumber + thirdNumber) > secondNumber && 
           (thirdNumber + secondNumber) > firstNumber 
            || firstNumber == secondNumber || firstNumber == thirdNumber
            || secondNumber == thirdNumber){
            System.out.println("Lehet egyenlő szárú háromszög!");
        }else{
            System.out.println("Nem lehet egyenlő szárú háromszög!");
        }*/
        
        boolean abGtc = firstNumber + secondNumber > thirdNumber;
        boolean acGtb = firstNumber + thirdNumber > secondNumber;
        boolean bcGta = secondNumber + thirdNumber > firstNumber;
        boolean isCorrectTriangle = firstNumber == secondNumber || firstNumber == thirdNumber || secondNumber == thirdNumber;
        
        System.out.println(abGtc && acGtb && bcGta && isCorrectTriangle);
        
        /////////////////////////////////////////////////////////////////////
        
        
        if (scanner.hasNextInt()){
            int osztalyzat = scanner.nextInt();
            
            switch (osztalyzat){
                case 5:
                case 4: System.out.println("Megfelelt"); break;
                case 3:
                case 2:
                case 1: System.out.println("Nem felelt meg");
            }
        }
        
    }   
}
