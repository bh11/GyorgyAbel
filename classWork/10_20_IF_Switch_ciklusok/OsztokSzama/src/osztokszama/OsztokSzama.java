/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package osztokszama;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class OsztokSzama {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        boolean isRequirednNumber = false;
        int number = -1;
        
        do{
            System.out.println("Adjon meg egy pozitív egész számot: ");
            if(scanner.hasNextInt()){
                number = scanner.nextInt();
                if(number > 0){
                    isRequirednNumber = true;
                }
            }else{
                    scanner.next();
                }
        }while(!isRequirednNumber);
        
        
        //pozitív osztók számának kiírása
        int i = 1;       
        while (i <= number){
            if(number%i == 0){   
                System.out.println(i);
            }
            i++;
        }
        
        //prím számok
        int count0fDevider = 0;
        int j = 1;       
        while (j <= number){
            if(number%j == 0){   
                System.out.println(j);
            }
            j++;
        }
        if(count0fDevider == 2){
            System.out.println("Prím");
        }
    }
    
}
