/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package october20feladatok;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class October20Feladatok {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean asking = true;
        int number = -1;

        do {
            System.out.println("Kérem adjon meg 1 és 5 közötti számot: ");
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                if (number >= 1 && number <= 5) {
                    asking = false;
                }
            } else {
                scanner.next();
            }

        } while (asking);

        if (number == 1) {
            System.out.println("I");
        } else if (number == 2) {
            System.out.println("II");
        } else if (number == 3) {
            System.out.println("III");
        } else if (number == 4) {
            System.out.println("IX");
        } else if (number == 5) {
            System.out.println("X");
        }

        switch (number) {
            case 1:
                System.out.println("I");
                break;
            case 2:
                System.out.println("II");
                break;
            case 3:
                System.out.println("III");
                break;
            case 4:
                System.out.println("IX");
                break;
            case 5:
                System.out.println("X");
                break;
            default:
                System.out.println("Invalid number");
        }
    }
}
