/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package szorzotabla;

/**
 *
 * @author george
 */
public class Szorzotabla {

    /**
     * @param args the command line arguments
     */
    
    
    
    public static void main(String[] args) {
        
        //while használatával
        int number = 10;
        int i = 1;
        while(i <= number){
            System.out.printf("10 x %d = %d\n", i, i * 10);
            i++;
        }
        
        //for használatával
        for(i = 1; i <= 10; i++){
            for(int j = 1; j <= 10; j++){
                System.out.printf("");
            }
        }
    }
    
    
    
    
}
