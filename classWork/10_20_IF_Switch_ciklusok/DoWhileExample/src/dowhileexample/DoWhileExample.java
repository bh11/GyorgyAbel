/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dowhileexample;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class DoWhileExample {

    /**
     * @param args the command line arguments
     */
    // Példa 1
    public static void main(String[] args) {
        /*
        do{
            System.out.println("Legalább egyszer lefut!");
        }while(false);
        
    // Példa 2
        Scanner scanner = new Scanner(System.in);
        int stop_number = 0;
        int number = 0;
        
        do{
            number = scanner.nextInt();
            if(number != stop_number){
            System.out.println("Out: " + number);
            }
            
        }while(number != stop_number);
    
        */
        
    }
    
}
