/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whileexample;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class WhileExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /*byte b = 0;
        
        while (true){
            System.out.println(b++);
        }
        int i = 1;
        
        while (i <= 10){
            System.out.println(i + ". Gyuri");
            i++;
        }*/
        
        Scanner scanner = new Scanner (System.in);
        
        int i = 0;
        int sum = 0; //a 0 semleges az összeadásban
        
            while(i < 5){
                int number = scanner.nextInt();
                sum = sum + number;
                i++;
            }
            System.out.println("Eredmény: " + sum);
    }
    
}
