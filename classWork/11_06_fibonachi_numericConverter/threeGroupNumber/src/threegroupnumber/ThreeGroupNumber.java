/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threegroupnumber;

/**
 *
 * @author george
 */
public class ThreeGroupNumber {

    static void writeNumberInFormat(int n) {
        //16077216 -> 16 077 216

        int[] arr = new int[100];
        int index = 0;
        if (n == 0) {
            index = 1;
        }
        while (n > 0) {
            //System.out.println(n % 1000);
            arr[index] = n % 1000;
            index++;

            n = n / 1000;
        }

        for (int i = index - 1; i >= 0; i--) {
            String str = arr[i] + " ";
            if (arr[i] <= 99 && i != index - 1) {
                str = "0" + str;
            }
            if (arr[i] <= 9 && i != index - 1) {
                str = "0" + str;
            }

            System.out.print(str);
        }
        System.out.println("");
    }

    static String formatInteger(long i) {
        String str = "" + i;

        if (i < 99) {
            str = "0" + str;
        }
        if (i <= 9) {
            str = "0" + str;
        }

        return str;
    }

    static void recursiveWriteNumberInFormat(long num) {
        if (num >= 1000) {
            recursiveWriteNumberInFormat(num / 1000);
        }
        String str = num % 1000 + " ";
        if (num > 1000) {
            str = formatInteger(num % 1000) + " ";
        }
        System.out.print(str);
    }

    static String harmasCsop(String szam) {
        String result = "";
        String space = " ";
        if (szam.length() == 0) {
            return result;
        }
        result += (szam.length() % 3 == 1 ? szam.substring(0, 1)
                + space + harmasCsop(szam.substring(1)) : szam.substring(0, 1) + harmasCsop(szam.substring(1)));
        return result;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        writeNumberInFormat(16077216);
        recursiveWriteNumberInFormat(16077216);
        System.out.println();
        System.out.println(harmasCsop(""+16077216));
    }

}
