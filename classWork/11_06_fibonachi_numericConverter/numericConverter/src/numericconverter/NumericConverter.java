/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numericconverter;

/**
 *
 * @author george
 */
public class NumericConverter {

    static String numericConverter(int number) {
        String str = "";

        while (number > 0) {
            str += number % 2;
            number = number / 2;
        }
        for (int i = 0; i < 10; i++) {

        }
        return new StringBuilder(str).reverse().toString();
    }

    static String convertToBin(int number) {
        /*
           9 | 1
           4 | 0
           2 | 0
           1 | 1
           0
         */
        String strTMP = "";
        while (number > 0) {
            strTMP = strTMP + (number % 2);
            number = number / 2;
        }
        String str = "";
        for (int i = strTMP.length() - 1; i >= 0; i--) {
            str = str + strTMP.charAt(i);
        }
        return str;
    }

    public static void decToNewBase(int num, int base) {
        if (num > 0) {
            decToNewBase(num / base, base);
            System.out.print(num % base);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int number = 50;

        System.out.println(numericConverter(number));
        System.out.println(convertToBin(number));
        //System.out.println(decToNewBase(50, 2));

    }

}
