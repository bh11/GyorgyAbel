/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sumeachnumbers;

/**
 *
 * @author george
 */
public class SumEachNumbers {

    //adott szám számjegyeinek összege
    static int sumEachNumbers(int num){
        int res = 0;
        
        while (num > 0){
            res += num % 10;
            num /= 10;
        }
        return res;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(sumEachNumbers(555));
    }
    
}
