/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursive1.pkg100;

/**
 *
 * @author george
 */
public class Recursive1100 {

    static void m(int a){
        if(a == 0){
            return;
        }
        
        System.out.println(a);
        
        m(--a);
    }
    
    static void n(int a){
        if(a == 101){
            return;
        }
        
        System.out.println(a);
        
        n(++a);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        m(100);
        n(0);
    }
    
}
