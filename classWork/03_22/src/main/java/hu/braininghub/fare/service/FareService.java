package hu.braininghub.fare.service;

import hu.braininghub.fare.dto.FareDto;
import hu.braininghub.fare.repository.FareRepository;
import hu.braininghub.fare.repository.entity.Fare;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FareService {

    private FareRepository fareRepository;

    @Autowired
    public void setFareRepository(FareRepository fareRepository) {
        this.fareRepository = fareRepository;
    }

    public List<FareDto> getAllFare(){
        List<Fare> fares = fareRepository.findAll();
        List<FareDto> result = new ArrayList<>();

        fares.forEach(fare -> {
            FareDto dto = new FareDto();
            BeanUtils.copyProperties(fare, dto);

            result.add(dto);
        });

        return result;
    }

    public List<FareDto>filter(String origin, String destination){
        List<Fare> fares = fareRepository.findByOriginAndDestination(origin, destination);
        List<FareDto> fareDto = new ArrayList<>();

        fares.forEach(fare -> {
            FareDto dto = new FareDto();
            BeanUtils.copyProperties(fare, dto);

            fareDto.add(dto);
        });

        return fareDto;
    }
}
