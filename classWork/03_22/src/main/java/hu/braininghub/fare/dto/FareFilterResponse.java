package hu.braininghub.fare.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class FareFilterResponse implements Serializable {

    @JsonProperty("_fares")
    private List<FareDto> fareDto;

    public List<FareDto> getFareDto(List<FareDto> result) {
        return fareDto;
    }

    public void setFareDto(List<FareDto> fareDto) {
        this.fareDto = fareDto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FareFilterResponse that = (FareFilterResponse) o;
        return Objects.equals(fareDto, that.fareDto);
    }

    @Override
    public String toString() {
        return "FareFilterResponse{" +
                "fareDto=" + fareDto +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(fareDto);
    }
}
