/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadexample;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class StringStore {
    
    private static final int MAX_SIZE = 10;
    
    private List<String> strings = new ArrayList<>();
    
    public synchronized void add(String s) throws InterruptedException{
        while (strings.size() == MAX_SIZE) {
            wait();
        }
        
        strings.add(s);
        
        notifyAll();
    }
    
    public synchronized String remove() throws InterruptedException{
        while (strings.size() == 0) {
            wait();
        }
        
        String s = strings.get(strings.size() - 1);
        strings.remove(s);
        
        notifyAll();

        return s;
    }
}
