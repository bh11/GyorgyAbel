/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dimensionarray;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class Main {

    static final Scanner SCANNER = new Scanner(System.in);
    
    static final char GOOD_BOMB = 'D';
    static final char BAD_BOMB = '*';
    
    static final char PLAYER = '8';
    
    static final int MIN_SIZE = 10;
    static final int MAX_SIZE = 15;
    
    static final int MIN_NUMBER_OF_BOMBS = 8;
    static final int MAX_NUMBER_OF_BOMBS = 20;
    
    static final int EMPTY = 0;
    
    static int lifePoints = 5;
    
    static int playerX = 0;
    static int playerY = 0;

    static int getNumber(int from, int to) {
        do {
            System.out.printf("Give me a number between %d and %d. ", from, to);
            if (SCANNER.hasNextInt()) {
                int number = SCANNER.nextInt();
                if (number >= from && number <= to) {
                    return number;
                }
            } else {
                swallowString();
            }
        } while (true);
    }

    static void swallowString() {
        SCANNER.next();
    }

    static void printField(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == EMPTY) {
                    System.out.print("\\");
                } else {
                    System.out.print(field[i][j]);
                }
            }
            System.out.println();
        }
    }

    static void play(int row, int col) {
        char[][] field = new char[row][col];
        putPlayer(field);
        printField(field);
        putBombs(field);
        step(field);
    }

    static void putPlayer(char[][] field) {
        field[playerX][playerY] = PLAYER;
    }

    static void putBombs(char[][] field) {
        int numberOfBombs = getNumber(MIN_NUMBER_OF_BOMBS, MAX_NUMBER_OF_BOMBS);
        int numberOfGoodBombs = calculateNumberOfGoodBombs(numberOfBombs);
        int numberOfBadBombs = numberOfBombs - numberOfGoodBombs;
        setBombs(field, numberOfGoodBombs, GOOD_BOMB);
        setBombs(field, numberOfBadBombs, BAD_BOMB);
        printField(field);
    }

    static int calculateNumberOfGoodBombs(int numberOfBombs) {
        return numberOfBombs / 3;
    }

    /**
     * This method returns a random number between from and to excluding to
     *
     * @param from
     * @param to
     * @return
     */
    static int generateRandomNumber(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static void setBombs(char[][] field, int count, char character) {
        for (int i = 0; i < count; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[i].length);
            if (field[x][y] == EMPTY) {
                field[x][y] = character;
            } else {
                i--;
            }
        }
    }

    static void step(char[][] field) {
        while (lifePoints > 0 && !isPlayerInTheLastRow(field)) {
            char c = SCANNER.next().charAt(0);
            switch (c) {
                case 'r':
                    stepRight(field);
                    break;
                case 'd':
                    stepDown(field);
                    break;
            }
            printField(field);
        }
    }

    static void stepRight(char[][] field) {
        if (!isPlayerInTheLastColumn(field)) {
            handleBombs(field, playerX, playerY + 1);
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerY++;
                field[playerX][playerY] = PLAYER;
            }
        }
    }

    static void stepDown(char[][] field) {
        if (!isPlayerInTheLastRow(field)) {
            handleBombs(field, playerX + 1, playerY);

            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerX++;
                field[playerX][playerY] = PLAYER;
            }
        }
    }

    static void handleBombs(char[][] field, int x, int y) {
        if (field[x][y] == GOOD_BOMB) {
            lifePoints--;
        }
        if (field[x][y] == BAD_BOMB) {
            lifePoints = 0;
        }
    }

    static boolean isPlayerInTheLastColumn(char[][] field) {
        return playerY == field[0].length - 1;
    }

    static boolean isPlayerInTheLastRow(char[][] field) {
        return playerX == field.length - 1;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int row = getNumber(MIN_SIZE, MAX_SIZE);
        int col = getNumber(MIN_SIZE, MAX_SIZE);
        play(row, col);
    }
}
