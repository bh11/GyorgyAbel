/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recusiveexample;

/**
 *
 * @author george
 */
public class RecusiveExample {
    
    static void m(int a){
        
        if(a == 0){
            return;
        }
        
        System.out.println(a);
        
        m(++a);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        m(10);
    }
    
}
