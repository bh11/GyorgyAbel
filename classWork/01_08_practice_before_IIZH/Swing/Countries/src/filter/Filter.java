/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author george
 */
public class Filter {

    public List<String> filterCountries(List<String> countries, String str){
        List<String> filteredCountries =
                countries.stream()
                         .filter(p -> p.toString().toLowerCase().contains(str.toLowerCase()))
                         .collect(Collectors.toList());                  
        return filteredCountries;                         
    }
}
