/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author george
 */
public class View extends JFrame{
    
    private Controller controller;
    private JTextField inputText = new JTextField(30);
    private JTextField result = new JTextField(50);
    private JButton searchButton = new JButton("Keresés");
    private JButton listCountries = new JButton("Listázás");
    
    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    public void buildWindow() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private JPanel buildNorth() {
        JPanel jp = new JPanel();
        jp.add(inputText);
        jp.add(searchButton);
        jp.add(listCountries);
        
        searchButton.addActionListener(p -> controller.handleInputText(inputText.getText()));
        listCountries.addActionListener(p -> controller.handleInputText(""));

        return jp;
    }
    
    private JPanel buildCenter(){
        result.setEditable(false);
        JPanel jp = new JPanel();
        jp.add(result);
        
        return jp;
    }
    
    public void setText(String str){
        result.setText(str);
    }
}
