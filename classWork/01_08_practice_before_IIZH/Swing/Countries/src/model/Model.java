/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import java.util.List;
import java.util.Objects;
import fileReader.FileReader;
import java.util.ArrayList;

/**
 *
 * @author george
 */
public class Model {
    
    private Controller controller;
    private FileReader reader = new FileReader();
    private List<String>countries = new ArrayList<>(reader.readFromFile());

    public List<String> getCountries() {
        return countries;  
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }
    
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.countries);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Model other = (Model) obj;
        if (!Objects.equals(this.countries, other.countries)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model{" + "countries=" + countries + '}';
    }
}
