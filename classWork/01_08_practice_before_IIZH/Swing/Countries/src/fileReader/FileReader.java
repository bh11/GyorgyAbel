/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileReader;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author george
 */
public class FileReader {
 
    private static final String FILENAME = "countries.txt";
    private List<String>countries = new ArrayList<>();
    
    public List readFromFile(){
        try (BufferedReader br = new BufferedReader(new java.io.FileReader(FILENAME))){
            
            String currentLine;
            
            while ((currentLine = br.readLine()) != null) {                
                countries.add(currentLine);
            }
            return countries;
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return null;
    }
}
