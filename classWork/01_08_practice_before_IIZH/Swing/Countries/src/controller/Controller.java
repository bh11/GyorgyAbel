/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import filter.Filter;
import java.util.List;
import model.Model;
import view.View;

/**
 *
 * @author george
 */
public class Controller {
    
    private View v;
    private Model m;
    
    Filter filteredList = new Filter();
    
    public Controller(View v, Model m) {
        this.v = v;
        this.m = m;

        v.setController(this);
        m.setController(this);
    }
    
    public void handleInputText(String str){
        
        if (0 == str.length()) {
            notifyView(m.getCountries());
        }
        
        if (0 < str.length()) {
            notifyView(filteredList.filterCountries(m.getCountries(), str));
        }
    }
    
    public void notifyView(List countries){
        v.setText(countries.toString());
    }
}
