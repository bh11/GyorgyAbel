/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controller.Controller;
import model.Model;
import view.View;

/**
 *
 * @author george
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        Készíts egy Java Swing alkalmazást, 
//        amely országok között képes keresni. 
//        Az országokat egy fájlból olvasd be az induláskor. 
//        A UI-on legyen egy beviteli mező és egy keresés gomb és egy lista az országokkal,
//        a keresés gombra kattintva a lista tartalma legyen megszűrve.
//        Szövegrészekre is szűrjünk (agyar-ra keresés esetén a Magyarország legyen találat). 
//        Példaként legalább 20 országgal töltsd fel a fájlt.

        Model m = new Model();
        View v = new View();
        Controller c = new Controller(v, m);
        
        v.buildWindow();
    }
    
}
