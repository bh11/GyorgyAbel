/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practice;

/**
 *
 * @author george
 */
public class Practice {
    
//  A a = (int n) -> {return n*n;};
//  B b = (String str, int n) -> {return str.length() + n;};
//  C c = () -> {System.out.println("my name is Bond");};
    
    interface A{
        int something(int n);
    }
    
    interface B{
        int something(String str, int n);
    }
    
    interface C{
        void something();
    }
    
    interface D { String method (int age, String... names);
    
//  public interface D {
//  String method(int age, String... names);
//  }
    
    D d = (int age, String... names) -> {
        return "something";
    };

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here   
    } 
  }
}
