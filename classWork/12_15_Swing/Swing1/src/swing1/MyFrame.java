/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing1;

import java.awt.BorderLayout;
import javafx.scene.control.ComboBox;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author george
 */
public class MyFrame extends JFrame{
    
    private final JButton jb = new JButton("Click!"); // azért itt van mert több metódus is használhatja
    private final JTextField from = new JTextField(26); //50 karakter hosszúságú lesz a field
    private final JTextField to = new JTextField(26); //50 karakter hosszúságú lesz a field
    private final JTextField set = new JTextField(1);
    private final JComboBox <Character> jc = new JComboBox<>(new Character[]{'A', 'B', 'C', 'D'});

    public MyFrame() {
        buildWindow();
    }
        
    private void buildWindow(){
        jb.addActionListener(l -> new Controller().copyText(from, to, (char)jc.getSelectedItem()));

        this.add(buildNorth(), BorderLayout.NORTH);
        this.add(buildCenter(), BorderLayout.CENTER);
        this.add(buildSouth(), BorderLayout.SOUTH);
        
        pack(); //újrarendezi az ablakot és a megfelelő mérteben megjeleníti az elemeket
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private JPanel buildNorth(){
        JLabel fromLabel = new JLabel("From ");
        JPanel north = new JPanel();
        
        north.add(fromLabel);
        north.add(from);
        north.add(jc);
        
        return north;
    }
    
    private JPanel buildSouth(){
        JLabel toLabel = new JLabel("To ");
        JPanel south = new JPanel();
        
        south.add(toLabel);
        south.add(this.to);
        
        return south;
    }
    
    private JPanel buildCenter(){
        JPanel center = new JPanel();
        center.add(jb);
        
        return center;
    }
    
//    private Character [] printABC(){
//        Character [] text = new Character[26];
//        for (char i = 'a'; i < 'z'; i++) {
//                    int j = 0;
//                    text [j] = i; 
//                    j++;
//        }
//        return text;
//    }
}
