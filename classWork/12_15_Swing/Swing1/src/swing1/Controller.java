/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing1;

import javax.swing.JTextField;

/**
 *
 * @author george
 */
public class Controller {
    
    public void copyText(JTextField from, JTextField to, char inputCharacter){
        int offset = inputCharacter - 'A';
        StringBuilder br = new StringBuilder();
        for (char character : from.getText().toCharArray()) {
            char next = (char)(character + offset);
            
            if (next > 'Z') {
                br.append((char)('A' + (next - 'Z') - 1));
            } else {
                br.append(next);
            }
        }
        to.setText(br.toString());
    }
    
//    public void shiftTextByCharacter(String text, char inputCharacter){
//        
//      int offset = inputCharacter - 'A';
//      
//      StringBuilder br = new StringBuilder();
//      
//      for (char character : from.getText().toCharArray()) {
//          char next = (char)(character + offset);
//
//          if (next > 'Z') {
//              br.append((char)('A' + (next - 'Z') - 1));
//          } else {
//              br.append(next);
//          }
//      }
//      to.setText(br.toString());
//    }
    
      
}

