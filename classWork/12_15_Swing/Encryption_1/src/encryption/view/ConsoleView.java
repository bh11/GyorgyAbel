/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.view;

import encryption.controller.Controller;
import java.util.Scanner;

/**
 *
 * @author czirjak_zoltan
 */
public class ConsoleView implements View {

    private static final String STOP = "exit";

    private Controller controller;

    private void startConsoleReader() {
        try ( Scanner scanner = new Scanner(System.in)) {
            String inputText;

            do {
                System.out.println("Input text: ");
                inputText = scanner.nextLine();

                if (STOP.equalsIgnoreCase(inputText)) {
                    break;
                }

                System.out.println("Offset cahracter: ");
                char c = scanner.nextLine().charAt(0);

                controller.shiftTextByCharacter(inputText, c);

            } while (true);
        }
    }

    @Override
    public void setController(Controller c) {
        controller = c;
    }

    @Override
    public void setText(String s) {
        System.out.println("Result: " + s);
    }

    @Override
    public void start() {
        startConsoleReader();
    }

}
