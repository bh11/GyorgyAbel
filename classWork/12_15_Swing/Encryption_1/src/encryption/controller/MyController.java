/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.controller;

import encryption.model.Model;
import encryption.view.View;

/**
 *
 * @author czirjak_zoltan
 */
public class MyController implements Controller {
    
    private final View view;
    private final Model model;

    public MyController(View view, Model model) {
        this.view = view;
        this.model = model;
        
        //view.setController(this);
        //model.setController(this);
    }
    
    public void shiftTextByCharacter(String text, char inputCharacter) {
        int offset = inputCharacter - 'A';
        StringBuilder br = new StringBuilder();

        for (char character : text.toCharArray()) {
            char next = (char)(character + offset);
            
            if (next > 'Z') {
                br.append((char)('A' + (next - 'Z') - 1));
            } else {
                br.append(next);
            }
        }

        model.setText(br.toString());
    }
    
    public void notifyView() {
        view.setText(model.getText());
    }
}
