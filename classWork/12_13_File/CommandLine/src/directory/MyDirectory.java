/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package directory;

import java.io.File;
import java.io.IOException;

import static parser.Commands.*;

/**
 *
 * @author george
 */
public class MyDirectory {
    
    private File file = new File(System.getProperty("user.dir"));
   
    public void pwd() {
        try {
            System.out.println(file.getCanonicalPath());
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void ls() {
        File[] files = file.listFiles();
        
        for (File f : files) {
            System.out.print(f.getName());
            System.out.println(f.isFile() ? " F " + f.length() :  " D");
        }
    
    }
    
    public void cd(String command) {
        if (PARENT_DIRECTORY.equals(command)) {
            cdParent();
        } else {
            cdDirectory(command);
        }
    }
    
    public void mv(String from, String to) {
      File oldFileName = null;
      File newFileName = null;
      boolean bool = false;
      
      try {  
         oldFileName = new File(from);
         newFileName = new File(to);

         bool = oldFileName.renameTo(newFileName);

         System.out.println("Átnevezés állapota: " + bool);
         
      } catch(Exception e) {
          System.out.println("Nem sikerült az átnevezés!");
      }
    }
    
    private void cdParent() {
        file = file.getParentFile();
    }
    
    private void cdDirectory(String directory) {
        File to = new File(file, directory);
        
        if (to.exists() && to.isDirectory()) {
            file = to;
        }
    }
}
