/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializationeample;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author george
 */
public class App {
    
    static Set <Employee> emp = new LinkedHashSet<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
                
        generateData();
        
        printText("\nÖsszes dolgozó kilistázva");
        printAllEmployee();
        printText("\nA választott dolgozó adatai");
        printOneEmployeeByName("emp4");
        printText("\nÚj dolgozó neve és adatai");
        addEmployee("emp11", 52, 23423, "taska utca", "IT");
        printText("\nDepartment szerint");
        printEmployeesByDepartment("IT");
        updateEmployeeSalary("emp2", 213153);
        removeEmployeeByName("emp5");
        
        printText("\nEmployee before serialization");
        
        for (Employee employee : emp) {
            System.out.println(employee);
        }
        
        serialize();
        
        printText("\nEmployee after serialization");
        
        for (Employee employee : deserialize()) {
            System.out.println(employee);
        }
        
    }
        public static void generateData(){
            
            Employee emp1 = new Employee("emp1", 27, 1000000, new Department("kis utca", "IT"));
            Employee emp2 = new Employee("emp2", 32, 651515, new Department("nagy utca", "Finance"));
            Employee emp3 = new Employee("emp3", 50, 543443, new Department("szel utca", "HR"));
            Employee emp4 = new Employee("emp4", 40, 3513100, new Department("eso utca", "Telecom"));
            Employee emp5 = new Employee("emp5", 51, 436400, new Department("klapka utca", "Automotive"));
            Employee emp6 = new Employee("emp6", 22, 343560, new Department("patak utca", "Telecom"));
            Employee emp7 = new Employee("emp7", 62, 374340, new Department("lapat utca", "HR"));
            Employee emp8 = new Employee("emp8", 42, 98778, new Department("gepmadar utca", "Finance"));
            Employee emp9 = new Employee("emp9", 19, 3967546, new Department("kerek utca", "Automotive"));
            Employee emp10 = new Employee("emp10", 20, 36896, new Department("kor utca", "IT"));

            emp.add(emp1);
            emp.add(emp2);
            emp.add(emp3);
            emp.add(emp4);
            emp.add(emp5);
            emp.add(emp6);
            emp.add(emp7);
            emp.add(emp8);
            emp.add(emp9);
            emp.add(emp10);
            
        }
        
        public static void printText(String string){
            System.out.println(string);
        }
        
        public static void serialize(){
            EmployeeRepository.serializeEmployee(emp);
        }
    
        public static LinkedHashSet<Employee> deserialize(){
            LinkedHashSet<Employee> result = EmployeeRepository.deserializeEmployee();
            return result;
        }
        
        public static void addEmployee(String name, int age, int salary, String address, String department){
            Employee newEmployee = new Employee(name, age, salary, new Department(address, department));
            emp.add(newEmployee);
            System.out.println(newEmployee);
        }
        
        public static void printAllEmployee(){
            emp.stream()
               .forEach(p -> System.out.println(p));
        }
        
        public static void printOneEmployeeByName(String name){
            emp.stream()
               .filter(p -> p.getName() != null)
               .filter(p -> p.getName().equals(name))
               .forEach(System.out::println);
        }
        
        public static void printEmployeesByDepartment(String departmentName){
            emp.stream()
               .filter(p -> p.getDepartment().getName().equals(departmentName))
               .forEach(p -> System.out.printf(departmentName+ " " + p.getName() + "\n"));
        }
        
        public static void updateEmployeeSalary(String name, int salary){ 
            emp.stream()
               .filter(p -> p.getName().equals(name))
               .findAny()
               .ifPresent(p -> {
                   p.setSalary(salary);
               });           
        }
        
        public static void removeEmployeeByName(String name){
            emp.removeIf(p -> p.getName().equals(name));
        }           
}
