/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializationeample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class EmployeeRepository{
    private static final String FILENAME = "employee.ser";    
    
    public static void serializeEmployee(Set emp){
        try (FileOutputStream fs = new FileOutputStream(FILENAME)){
            ObjectOutputStream ou = new ObjectOutputStream(fs);
            ou.writeObject(emp);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        }
    }
    
    public static LinkedHashSet deserializeEmployee(){
        LinkedHashSet emp = null;
        try (FileInputStream fs = new FileInputStream(FILENAME)){
            ObjectInputStream oi = new ObjectInputStream(fs);
            emp = (LinkedHashSet) oi.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        }
        return emp;
    }
}
