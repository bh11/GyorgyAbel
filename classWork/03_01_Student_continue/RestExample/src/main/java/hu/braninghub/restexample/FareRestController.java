/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braninghub.restexample;

import hu.braininghub.restexample.dto.Fare;
import hu.braininghub.restexample.dto.FareRequest;
import java.util.Arrays;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author george
 */
@Path("fare_service")
@Singleton
public class FareRestController {
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getFare(){
        Fare fare = new Fare("BUD", "FRA", "LH", 1500);
        return Response.ok(Arrays.asList(fare)).build();
    }  
    
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Consumes(value = MediaType.APPLICATION_JSON)
    public Response filter(FareRequest req){
        Fare fare = new Fare(req.getOrigin(), req.getDestination(), "LH", 1500);
        return Response.ok(Arrays.asList(fare)).build();
    }
}
