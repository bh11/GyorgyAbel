/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package author;

/**
 *
 * @author george
 */
public class Author {
    
    private String [] authors;
    private String name;
    private String dateOfBirth;
    public static final String DATE_OF_BIRTH = "1971.01.01";
    private String books;

    public Author(String name, String books){
        this.name = name;
        this.books = books;
        this.dateOfBirth = DATE_OF_BIRTH;
    }
    
    public Author(String name, String dateOfBirth, String books){
        this(name, books);
        this.dateOfBirth = dateOfBirth;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }
    
    
}
