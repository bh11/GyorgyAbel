/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package author;

/**
 *
 * @author george
 */
public class Books {
    
    private String dateOfrelease;
    private String title;
    private String publisher;
    
    public Books(String dateOfRelease, String title, String publisher){
        this.dateOfrelease = dateOfRelease;
        this.title = title;
        this.publisher = publisher;
    }

    public String getDateOfrelease() {
        return dateOfrelease;
    }

    public void setDateOfrelease(String dateOfrelease) {
        this.dateOfrelease = dateOfrelease;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    
    
}
