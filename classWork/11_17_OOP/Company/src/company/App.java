/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company;

/**
 *
 * @author george
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Company company = new Company();
        
        Employee emp1 = new Employee ("Bela", "Kiss", 200000);
        Employee emp2 = new Employee ("Adam", "Nagy", 300000);
        Employee emp3 = new Employee ("Eszter", "Móricz", 500000);
        
        company.addEmployee(emp1);
        company.addEmployee(emp2);
        company.addEmployee(emp3);
        
        company.printEmployeesWithSalaries();
        company.increaseSalaries();
        company.printEmployeesWithSalaries();
        
               
//        Employee.raiseIndicator = 1.2;
//        Employee employee = new Employee("Bela", "Balázs", "Kiss", 300000.0);
//        company.addEmployee(employee);
//        System.out.println(employee.name.getFullName());
//        System.out.println(employee.salary + "Ft");
//        
//        
//        employee.increaseSalary();
//        System.out.println(employee.salary + "Ft");
//        
//        
//        Employee employee2 = new Employee("Adam", "Nagy", 200000.0);
//        company.addEmployee(employee2);
//        System.out.println(employee2.name.getFullName());
//        System.out.println(employee2.salary + "Ft");
//        
//        employee2.increaseSalary();
//        System.out.println(employee2.salary + "Ft");
//        
//        Name name = new Name("Eszter" , "Moricz");
//        Employee employee3 = new Employee (name, 500000.0);
//        System.out.println(employee3.getFullname());
//        
//        Employee employee4 = new Employee(new Name("Lajos", "Lajvér"), 1500000.0);
//        System.out.println(employee4.getFullname());
//        
//        Employee emloyee5 = new Employee();
//        System.out.println(emloyee5.getFullname());
//        
//        for (int i = 0; i < emloyees.length; i++) {
//            emloyees[i].increaseSalary();
//            System.out.println(emloyees[i].name.getFullName()+ " increased salary: " + emloyees[i].salary + " Ft");
//        }
//        
//        for(Employee emp : emloyees){
//            System.out.println(emp.name.getFullName());
//        }
    }
    
}
