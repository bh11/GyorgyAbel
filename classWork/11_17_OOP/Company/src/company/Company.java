/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company;

/**
 *
 * @author george
 */
public class Company {
    
    private Employee[] employees;
    private int numberOfEmployes;
    
    public static double raiseIndicator = 1.1;
    
    Company() {
        employees = new Employee [10];
    }
    
    public void addEmployee (Employee emp){
        // increaseMaximumNumberOfEmployees
        if (employees.length == numberOfEmployes) {
            increaseMaximumNumberOfEmployees();
        }
        employees[numberOfEmployes] = emp;
        numberOfEmployes++;
    }
    
    private void increaseMaximumNumberOfEmployees(){
        Employee[] employeesTmp = new Employee[numberOfEmployes*2];
        for (int i = 0; i < numberOfEmployes; i++) {
            employeesTmp[i] = employees[i];
        }
        employees = employeesTmp;
    }
    
    public void increaseSalaries(){
        for (int i = 0; i < numberOfEmployes; i++) {
            employees[i].increaseSalary(raiseIndicator);
        }
    }
    
    public void printEmployeesWithSalaries(){
        for (int i = 0; i < numberOfEmployes; i++) {
            System.out.println(employees[i].getName().getFullName() + "'s salary: " + employees[i].getSalary());
        }
    }
}
