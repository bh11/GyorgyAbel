/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company;

/**
 *
 * @author george
 */
public class Employee {
    
    protected Name name;
    protected double salary;
    
    
    public Employee (String firsName, String lastName, double salary){
        name = new Name(firsName, lastName);
        this.setSalary(salary);
    }

    public Employee(String firstName, String middleName, String lastName, double salary) {
        this(firstName, lastName, salary);
        this.name.setMiddleName(middleName);
    }
    
    public Employee(Name name,double salary){
        this.name = name;
        
    }
    
    public String getFullname(){
        return this.name.getFullName();
    }

    public void increaseSalary(double raiseIndicator){
        salary = salary * raiseIndicator;
    }
    
    public Name getName(){
        return name;
    }
    
    public void setName(Name name){
        this.name = name;
    }
    
    public double getSalary(){
        return salary;
    }
    
    public void setSalary(double salary){
        if (salary < 0 || salary > 10000000) {
            this.salary = 0;
            System.out.println("Wrong salary, salary is set to 0");
        }else{
            this.salary = salary;
        }
    }

    
    
}
