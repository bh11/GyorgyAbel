/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braninghub.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author george
 */
public class EmployeeService {
    
    private static List<Employee> employees = new ArrayList<>();
    
    public List<Employee> getEmployees(){
        return employees;
    }
    
    public void addEmployee(Employee employee){
        employees.add(employee);
    }
    
    public void deleteEmployeeByName(String name){
        int index = getIndexByName(name);
        
        if (index != -1) {
            employees.remove(index);
        }
    }
    
    private int getIndexByName(String name){
        for (int i = 0; i < employees.size(); i++) {
            if (name.equals(employees.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }
    
    public List<Employee> listEmployeesByDepartment(String departmentName){ 
        return employees.stream()
                 .filter(p -> p.getDepartment().equalsIgnoreCase(departmentName))
                 .collect(Collectors.toList());
    }
}
