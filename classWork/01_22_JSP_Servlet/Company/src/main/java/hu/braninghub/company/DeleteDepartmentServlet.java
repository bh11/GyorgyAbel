/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braninghub.company;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author s26841
 */
public class DeleteDepartmentServlet extends HttpServlet {
    
    DepartmentService departmentService = new DepartmentService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        departmentService.deleteDepartmentByName(request.getParameter("name"));
        request.setAttribute("departments", this.departmentService.getDepartments());
        request.getRequestDispatcher("departments.jsp").forward(request, response);
    }


}
