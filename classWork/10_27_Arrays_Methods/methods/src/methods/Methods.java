/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methods;

/**
 *
 * @author george
 */
public class Methods {

    static boolean sendMail(String to, String text) {
        if (to.contains("@")) {
            System.out.println("Sent...");
            return true;
        }

        System.out.println("Something went wrong...");
        return false;
    }
    
    static void printName(){
        System.out.println("Ábel György");
        return; //felesleges, mert az utolsó parancs a return érték.
    }
    
    static boolean isEven(int number){
        if(number % 2 == 0){
            return true;
        }
        return false;
        
        //return number % 2 == 0;
    }
    static String concat (int number1, int number2){
        //return number1 + "" +number2;
        //return "" + number1 + number2;
        //return String.valueOf(number1) + String.valueOf(number2);
        return String.format("%d%d", number1,number2);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        sendMail("gyuri152@gmail.com", "fdsfdsf....");
        boolean success = sendMail("gyuri152gmail.com", "fdsfdsf....");
        System.out.println(success);
        printName();
        System.out.println(isEven(12));
        
        String s = concat(10, 25);
        System.out.println(s);
        System.out.println(concat(10, 25));
    }

}
