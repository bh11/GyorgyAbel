/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m7highway;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class M7Highway {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /*
        Az M7-es autópályán traffipaxot szerelnek fel, amely a Balaton felé igyekvők gyorshajtásait rögzíti. A
        mérés több nap adatait is tartalmazza; egy autó adatait „óra perc sebesség” formában. Pl. 12 45 198 azt
        jelenti, 12:45-kor 198 km/h-val száguldott valaki. Az adatsor végét 0 0 0 zárja.
        Készítsen a C programod egy táblázatot arról, hogy melyik órában mennyivel ment a leggyorsabb autó!
        Ha egy adott órában nem volt gyorshajtás, az maradjon ki! A kimenet ilyen legyen:
        14:00-14:59 -> 145 km/h
        16:00-16:59 -> 167 km/h (Tipp: a sebességek mind pozitív számok.)
        */
        int hour = -1, min = hour, speed = hour;
        
        int[] velocities = new int[24];
        Scanner scanner = new Scanner(System.in);
        
        //velocities[30] = 10; --> java.lang.ArrayIndexOutOfBoundsException
        
        do {
            System.out.println("Adja meg az időt és a sebességet szóközzel elválasztva:");
            hour = scanner.nextInt();
            min = scanner.nextInt();
            speed = scanner.nextInt();
            
            if (hour >= 0 && hour < 24 && speed > 130 && velocities[hour] < speed) {
                velocities[hour] = speed;
            }
        
        } while (hour != 0 || min != 0 || speed != 0);
        
        for (int i = 0; i < velocities.length; i++) {
            if (velocities[i] != 0) {
                System.out.printf("%d:00 - %d:59 --> %d km/h\n", i, i, velocities[i]);
            }
        }
        
    }
    
}
