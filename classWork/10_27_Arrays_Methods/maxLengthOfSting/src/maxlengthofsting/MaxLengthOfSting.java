/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maxlengthofsting;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class MaxLengthOfSting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Felhasználó szavakat ad meg és az "exit" szó megadásáig.
        //Adjuk meg a leghosszabb szót és azt, hogy hanyadik bevitel volt.
        
        Scanner sc = new Scanner(System.in);
        String word = "";
        int number = -1;
        String longestWord = "";
        
        String currentWord = "";
        int cnt = 0;
        do{
            currentWord = sc.next();
            
            if(!"exit".equals(currentWord) && currentWord.length() > longestWord.length()){
                longestWord = currentWord;
                number = cnt;
            }
            
            cnt++;
            
        } while( !"exit".equals(currentWord));
        
        System.out.println(longestWord);
        System.out.println(number);
    }
    
}
