/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tenpositivenumber;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class TenPositiveNumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Kérjünk be a felhasználótól 10db pozitív számot. 
        //Add meg, hány 3-mal nem osztható van közöttük.
        //Add meg a számok átlagát

        Scanner sc = new Scanner(System.in);

        int[] numbers = new int[10];
        
        
        for (int i = 0; i < 10; i++) {
            if(sc.hasNextInt()){
                int number = sc.nextInt();
                if(number > 0){
                    numbers[i] = number;
                }else{
                    i--;
                }
            }else{
                sc.next();
                i--;
            }
        }
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers [i] + " ");
        }
        System.out.println();
        int numberOfNotDevidavleBy3 = 0;
        for (int i = 0; i < numbers.length; i++) {
            if(i % 3 != 0){
                numberOfNotDevidavleBy3++;
            }
        }
        System.out.printf("3-mal nem osztható számok: %d db\n", numberOfNotDevidavleBy3);
        
        int sum = 0;
        int counter = numbers.length;
        
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        
        System.out.println("A számok átlaga: " + (double)sum / numbers.length);
        
    }

}
