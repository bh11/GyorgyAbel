/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stone;

/**
 *
 * @author george
 */
public enum StoneType {
    
    TIME("GREEN", 11),
    SOUL("YELLOW", 10);
        
    private final String color;
    private final int power;

    StoneType(String color, int power){ //default private a láthatósága!!!
        this.color = color;
        this.power = power;
    }

    public String getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }
}
