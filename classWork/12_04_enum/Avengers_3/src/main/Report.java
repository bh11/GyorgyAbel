/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import stone.StoneType;
import store.Fleet;
import store.Ship;


/**
 *
 * @author george
 */
public class Report {
    
    private Fleet fleet;

    public Report(Fleet fleet) {
        this.fleet = fleet;
    }
    
//    private void printAllHeores(Fleet fleet) {
//        fleet.getShips().forEach((p) -> {
//            p.getHeroes().forEach((p) -> {
//                System.out.println(p.toString());
//            });
//        });
//    }

    private void bornOnEarthInShip (Fleet fleet) {
        System.out.println(
                fleet.getShips()
                        .stream()
                        .flatMap(p -> p.getHeroes().stream())
                        .filter(p -> p instanceof BornOnEarth)
                        .count());
    }

    private void maxPower(Fleet fleet) {
        System.out.println(
                fleet.getShips()
                        .stream()
                        .flatMap(p -> p.getHeroes().stream())
                        .max((p1, p2) -> p1.getPower() - p2.getPower())
                        .get()
                        .getName()
        );
    }

    private void IdentityCardList(Fleet fleet) {
        fleet.getShips()
                .stream()
                .flatMap(p -> p.getHeroes().stream())
                .filter(p -> p instanceof BornOnEarth)
                .forEach(p -> System.out.println(((BornOnEarth)p).getIdentityCard().getNumber()));
    }
    
    //Hajónként melyik kőből mennyi van
    
    private Map<StoneType, Integer> countOfStone(Ship ship){
        return ship.getHeroes().stream()
                   .map(AbstractHero::getStone) // itt megvannak a kövek 
                   .collect(Collectors.toMap(
                           k -> k,  // ez köteleő
                           v -> 1,  // idáig azt mondja meg, hogy egy hajón hány kő van, ez is kötelező
                           (v1, v2) -> v1 + v2, // itt adod meg mi legyen kulcsütközéskor, ha nem adod meg akkor exceptionnal el fog szállni
                           HashMap::new // ez nem kötelező megadni
                        )
                   );
    }
    
    private Map<Ship, Map<StoneType, Integer>> countOfStoneByShip(){
        return fleet.getShips().stream()
                    .collect(Collectors.toMap(
                            s -> s, // itt megtartjuk a hajót mint kulcsot (Ship tipus, hogy lesz Shipből kulcs)
                            v -> countOfStone(v), // ez itt az értéke (Ship tipus, hogy lesz Shipből érték)
                            // this::CountOfStone // ez a metódus referenciája ennek: v -> countOfStone(v)
                            (x, y) -> x // ha ütközik a kulcs tartsa meg a régit
                        )
                    );
    }
    
    private void printCountOfStoneByShips(){
        countOfStoneByShip().entrySet().stream()
                .forEach(i->{System.out.println("Ship: "+i);
                i.getValue()
                        .forEach((j, k)-> System.out.println("Stone: "+j+" Count: "+k));
                });
    }
    
    private boolean containsOnlyBornOnEarth(Ship ship){
        return ship.getHeroes().stream().allMatch(p -> p instanceof BornOnEarth);
    }
    
    private Map <Boolean, List<Ship>> partitionByContainsOnlyBornOnEarth(){
        return fleet.getShips().stream()
                    .collect(Collectors.partitioningBy(
                            this::containsOnlyBornOnEarth // két halmazba rendeztük a hajókat
                    ));
        
                    // partitioningBy két halmazra bontja predicate alapján
    }
    
    private long numberOfShipsContainingOnlyBornOnEarth(){
        return partitionByContainsOnlyBornOnEarth().get(Boolean.TRUE).size();
    }
    
}
