/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class Saver {
    
    private static final String FILENAME = "avengers.ser";
    
    public static void SerializationAvengers (List fleet){
        try (FileOutputStream fo = new FileOutputStream(FILENAME)){
            ObjectOutputStream ou = new ObjectOutputStream(fo);
            ou.writeObject(fleet);
        } catch (FileNotFoundException ex) {
            System.out.println("Fájl nem található!" + ex);
        } catch (IOException ex) {
            System.out.println("Váratlan hiba lépett fel!" + ex);
        }
    }
    
    public static List DeserializableAvengers (){
        ArrayList heroes = null;
        
        try (FileInputStream is = new FileInputStream(FILENAME)){
            ObjectInputStream oi = new ObjectInputStream(is);
            heroes = (ArrayList) oi.readObject();
        } catch (FileNotFoundException ex) {
            System.out.println("Fájl nem található!" + ex);
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Váratlan hiba lépett fel!" + ex);
        }
        
        return heroes;
    }   
}
