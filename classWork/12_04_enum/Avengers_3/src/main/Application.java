/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import read.ConsoleReader;



/**
 *
 * @author george
 */
public class Application {
    
    public static void main(String[] args) {
        
//2. Írj programot (objektum orientáltan), ami képes tárolni a Bosszúálló hősöket. 
//Minden hős rendelkezik egy névvel és egy szuper erővel (2 és 10 közötti szám), 
//valamint képesek egy-egy végtelen kőnek őrzését ellátni. Ugyanazt a követ több hős is őrizheti. 
//(Végtelen kövek: tér, erő, valóság, elme, idő, lélek). 
//Minden végtelen kőnek van színe (tetszőleges) és ereje (tetszőleges). 
//A hősök 2 speciális csoportba sorolhatók. 
//Vannak, akik a Földön születtek és vannak, akik nem. 
//Minden földi születésűnek van útlevele (6jegyű generált szám) és tud repülni. 
//A többiek ( nem földiek) úsznak és lángot  szórnak, de igazolvány nélkül. 
//A bosszúállókat sorba is tudjuk rendezni ha kell: az őrzött kő ereje és a hősök neve alapján.
//Thanos ellen harci hajókba teszik a hősöket. Egy hajóba 4 hős fér. 
//A felhasználó konzolon adja meg a hősöket az “exit” szó begépeléséig alábbi formátumban:
////név;erő;kő;földi-e
//Captain America;3.5;Soul Stone;0
//Hulk, 9, Soul Stone;1
//…
//Ha a hősök bevitele megtörtént, adjuk meg az alábbi adatokat olvasható formában a képernyőn:
//Hajónként melyik kőből mennyi van?
//Hány földi születésű hős indul a harcba?
//Ki rendelkezik a legnagyobb erővel?
//Listázzuk ki a felhasznált útlevelek sorszámát!
//Hány harci hajó indul útnak?
//A hősöket sorrendben írjuk ki egy szöveges fájlba! 
//(15 pont)

        ConsoleReader reader = new ConsoleReader();
        reader.read();    
    }
    
}
