/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import main.Saver;

/**
 *
 * @author george
 */
public class Fleet implements Serializable{
    
    final private List<Ship> ships = new ArrayList<>();
    
    public void add(AbstractHero hero){
        findShip().add(hero);
    }
    
    private Ship findLastShip(){
        if (ships.isEmpty()) {
            ships.add(new Ship());
        }
        return ships.get(ships.size() -1);
    }
    
    private Ship findShip(){
        Ship ship = findLastShip();
                
        if (ship.isEmptySeat()) {
            return ship;
        }else{
            ships.add(new Ship());
            return ships.get(ships.size() -1);
        }
    }

    public List<Ship> getShips() {
        return ships;
    }
    
    public void serialize(){
        Saver.SerializationAvengers(ships);
    }
    
    public void deserialize(){
        Saver.DeserializableAvengers();
    }
    
     @Override
    public String toString() {
        return "Fleet{" + "ships=" + ships + '}';
    }
}
