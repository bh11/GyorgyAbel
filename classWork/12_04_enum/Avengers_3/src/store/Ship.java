/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author george
 */
public class Ship implements Serializable{
    private static final int MAX_SIZE = 4;
    private static final int INDEX_IN_NAME = 1;
    
    private final List<AbstractHero> heroes = new ArrayList<>();
    
    public void add(AbstractHero hero){
        if (isEmptySeat()) {
            heroes.add(hero);
        }
        orderBySecondCharacterOfName();
    }
    
    public boolean isEmptySeat(){
        return heroes.size() < MAX_SIZE;
    }

    public List<AbstractHero> getHeroes() {
        return heroes;
    }
    
    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }
    
    private void orderBySecondCharacterOfName() {
        Comparator<AbstractHero> cmp = new Comparator<AbstractHero>() {
            public int compare(AbstractHero h1, AbstractHero h2) {
                return h1.getName().charAt(INDEX_IN_NAME) - 
                       h2.getName().charAt(INDEX_IN_NAME);
            }
        };
        
        Collections.sort(heroes, cmp);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.heroes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ship other = (Ship) obj;
        if (!Objects.equals(this.heroes, other.heroes)) {
            return false;
        }
        return true;
    }
    
    
}
