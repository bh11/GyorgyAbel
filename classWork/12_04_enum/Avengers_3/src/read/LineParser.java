/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import hero.AbstractHero;
import hero.HeroFactory;
import store.Fleet;


/**
 *
 * @author george
 */
public class LineParser {
    
    private static final String DELIMITER = ";";
    private static final int MIN_CHARACTERS_OF_NAME = 2;
    
    private final Fleet store = new Fleet();
    
    public void process(String line){
        String[] parameters = line.split(DELIMITER);
        AbstractHero hero = HeroFactory.create(parameters);
        store.add(hero);
    }
    
    public void print(){
        System.out.println(store.toString());
    }
    
//    public void checkNameRestricition(String name) throws InvalidNameAvengersException{
//        if (name.length() < MIN_CHARACTERS_OF_NAME) {
//            throw new InvalidNameAvengersException("Invalid name: " + name);
//        }
//    }
}
