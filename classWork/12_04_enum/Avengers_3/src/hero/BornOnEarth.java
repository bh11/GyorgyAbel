/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import ability.Flying;
import stone.StoneType;

/**
 *
 * @author george
 */
public class BornOnEarth extends AbstractHero implements Flying{
    
    private final IdentityCard identityCard;

    public BornOnEarth(String name, int power, StoneType stone, IdentityCard identityCard) {
        
        super(name, power, stone);
        this.identityCard = identityCard;
        
    }

    public IdentityCard getIdentityCard() {
        return identityCard;
    }

    @Override
    public void fly() {
        System.out.println("I can fly.");
    }

    @Override
    public String toString() {
        return super.toString() + "BornOnEarth{" + "identityCard=" + identityCard + '}';
    }
}
