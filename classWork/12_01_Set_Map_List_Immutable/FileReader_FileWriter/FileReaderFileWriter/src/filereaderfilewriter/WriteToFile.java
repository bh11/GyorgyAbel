/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filereaderfilewriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author s26841
 */
public class WriteToFile {
    
    public void write(String string, String ... test){
        try (FileWriter fw = new FileWriter("test.txt");
             PrintWriter pw = new PrintWriter(fw)){
            
            pw.println(string);
            for (int i = 0; i < test.length; i++) {
                pw.println(test[i]);
            }
            
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
