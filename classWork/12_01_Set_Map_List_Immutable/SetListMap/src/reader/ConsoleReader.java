/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import java.util.Scanner;
import store.StringStore;

/**
 *
 * @author george
 */
public class ConsoleReader {
    
    private static final String STOP = "exit";
    
    public void read(StringStore store){
        Scanner scanner = new Scanner (System.in);
        String word = null;
        
        do{
            word = scanner.next();
            store.add(word);
        }while(!STOP.equals(word));
        
        scanner.close();
    }
}
