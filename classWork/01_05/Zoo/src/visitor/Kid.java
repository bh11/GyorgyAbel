/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;
import animal.HappinessFactor;

/**
 *
 * @author george
 */
public class Kid implements Visitor{

    @Override
    public void act(AbstractAnimal animal) {
        animal.setVisitor(animal.getVisitor() + 2);
        animal.setHunger(animal.getHunger() - 1);
        animal.setHappiness(HappinessFactor.nextLevel(animal.getHappiness()));
    }

    @Override
    public void feed(AbstractAnimal animal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
