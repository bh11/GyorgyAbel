/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import zoo.shop.Food;

/**
 *
 * @author george
 */
public abstract class AbstractMammal extends AbstractAnimal{
    
    public AbstractMammal(String name, int requiredPlace) {
        super(name, requiredPlace);
    }
    
    @Override
    public void eat (Food food){
        setHunger(getHunger() + 2);
    }   
}
