/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import java.time.LocalDateTime;
import java.util.Objects;
import visitor.Visitor;
import zoo.shop.Food;

/**
 *
 * @author george
 */
public abstract class AbstractAnimal {
    
    private final String name;
    private final int requiredPlace;
    
    private int visitor;
    private LocalDateTime entryCage;
    private double hunger;
    private HappinessFactor happiness = HappinessFactor.SAD;

    public AbstractAnimal(String name, int requiredPlace) {
        this.name = name;
        this.requiredPlace = requiredPlace;
    }

    public int getVisitor() {
        return visitor;
    }

    public void setVisitor(int visitor) {
        this.visitor = visitor;
    }

    public LocalDateTime getEntryCage() {
        return entryCage;
    }

    public void setEntryCage(LocalDateTime entryCage) {
        this.entryCage = entryCage;
    }

    public double getHunger() {
        return hunger;
    }

    public void setHunger(double hunger) {
        this.hunger = hunger;
    }

    public HappinessFactor getHappiness() {
        return happiness;
    }

    public void setHappiness(HappinessFactor happiness) {
        this.happiness = happiness;
    }

    public String getName() {
        return name;
    }

    public int getRequiredPlace() {
        return requiredPlace;
    }
    
    public void visit (Visitor visitor){
        visitor.act(this);
    }
    
    public void eat (Visitor visitor){
        visitor.feed(this);
    }
    
    public abstract void eat(Food food);
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + this.requiredPlace;
        hash = 67 * hash + this.visitor;
        hash = 67 * hash + Objects.hashCode(this.entryCage);
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.hunger) ^ (Double.doubleToLongBits(this.hunger) >>> 32));
        hash = 67 * hash + Objects.hashCode(this.happiness);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractAnimal other = (AbstractAnimal) obj;
        if (this.requiredPlace != other.requiredPlace) {
            return false;
        }
        if (this.visitor != other.visitor) {
            return false;
        }
        if (Double.doubleToLongBits(this.hunger) != Double.doubleToLongBits(other.hunger)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.entryCage, other.entryCage)) {
            return false;
        }
        if (this.happiness != other.happiness) {
            return false;
        }
        return true;
    }
    
    
}
