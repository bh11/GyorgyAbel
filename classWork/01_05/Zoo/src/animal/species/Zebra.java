/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal.species;

import animal.AbstractMammal;
import animal.feature.Terrestrial;

/**
 *
 * @author george
 */
public class Zebra extends AbstractMammal implements Terrestrial{
    
    public Zebra(String name, int requiredPlace) {
        super(name, requiredPlace);
    }
    
}
