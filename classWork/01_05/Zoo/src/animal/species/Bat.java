/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal.species;

import animal.AbstractMammal;
import animal.feature.Flying;
import animal.feature.Terrestrial;

/**
 *
 * @author george
 */
public class Bat extends AbstractMammal implements Terrestrial, Flying{

    public Bat(String name, int requiredPlace) {
        super(name, requiredPlace);
    }

    @Override
    public void Fly() {
        System.out.println("I can Fly! Yeah" + toString());
    }  
}
