/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import filter.handler.Filter;
import filter.handler.SizeLimited2MbFilter;
import filter.handler.SmallLetterContainedFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author george
 */
public class FilterAggregator {
    
    private final List<Filter> filters = new ArrayList<>();
    
    public FilterAggregator(){
        filters.add(new SizeLimited2MbFilter()); // itt regisztrálom be a filtereket
        filters.add(new SmallLetterContainedFilter());
    }
    
    public List<File> filter(List<File> files){ // itt adjuk vissza a megszürt fájlokat
        return files.stream()
                    .filter(this::allFilterMatched) // f -> allFilterMatched(f)
                    .collect(Collectors.toList());
    }
    
    private boolean allFilterMatched(File f){
        for (Filter filter : collectRelevantFilters(f)) {
            if (!filter.filterFile(f)) {
                return false;
            }
        }
        return true;
    }
    
    private List<Filter> collectRelevantFilters(File file){
        return filters.stream()
                      .filter(p -> p.test(file))
                      .collect(Collectors.toList());
    }
}
