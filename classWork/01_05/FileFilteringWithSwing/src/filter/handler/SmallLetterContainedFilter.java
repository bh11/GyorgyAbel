/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handler;

import java.io.File;

/**
 *
 * @author george
 */
public class SmallLetterContainedFilter implements Filter{

    @Override
    public boolean filterFile(File file) {
        String name = file.getName();
        
        for (int i = 0; i < name.length(); i++) {
            if (isSmallerLetter(name.charAt(i))) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isSmallerLetter(char c){
        return c >= 'a' && c <= 'z';
    }

    @Override
    public boolean test(File file) {
        return true;
    }
}
