/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handler;

import java.io.File;

/**
 *
 * @author george
 */
public class AgeLimitedFilter implements Filter{
    private static final String USER_HOME = System.getProperty("user.home");

    @Override
    public boolean filterFile(File file) {
        return file.lastModified() / 1000D / 3600D / 24D < 2D;
    }

    @Override
    public boolean test(File file) {
        return file.getPath().contains(USER_HOME);
    }
    
}
