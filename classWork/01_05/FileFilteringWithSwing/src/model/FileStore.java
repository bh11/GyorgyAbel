/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ModelController;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class FileStore implements Model{
    
    private ModelController controller;
    private List<File> files = new ArrayList<>();

    @Override
    public void setController(ModelController controller) {
        this.controller = controller;
    }

    @Override
    public List<File> getFiles() {
        return files;
    }

    @Override
    public void setFiles(List<File> files) {
        this.files = files;
        
        controller.notifyView();
    }
    
}
