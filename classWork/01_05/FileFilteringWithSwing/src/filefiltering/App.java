/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filefiltering;

import controller.Controller;
import controller.FileController;
import model.FileStore;
import model.Model;
import view.SwingView;
import view.View;

/**
 *
 * @author george
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        View v = new SwingView();
        Model m = new FileStore();
        
        Controller c = new FileController(v, m);
        v.enableView();
    }
    
}
