/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambda;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author s26841
 */
public class Lambda {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        List <String> list = new ArrayList<>();
        
        list.add("harcsapaprikas");
        list.add("hah");
        list.add("csokimiki");
        list.add("akacmez");
        list.add("belepokartya");
        list.add("viz");
        list.add("router");
        
        System.out.println("Rövidtől a hosszúig:");        
        list.sort((t1,t2) -> t1.length()-t2.length());
        list.forEach(n -> System.out.println(n));
        
        System.out.println("\nHosszútól a rövidig:");
        list.sort((t1,t2) -> t2.length()-t1.length());
        list.forEach(n -> System.out.println(n));
        
        System.out.println("\nABC sorrend, csak első betű:");        
        list.sort((t1,t2) -> t1.charAt(0)-t2.charAt(0));
        list.forEach(n -> System.out.println(n));
        
        System.out.println("\n\" e \" betűsök előre és minden más utána: ");
        list.sort((t1,t2) -> (t2.contains("e")? 1 : 0) - (t1.contains("e")? 1 : 0));
        list.forEach(n -> System.out.println(n));
    }
}
