/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgenum;

/**
 *
 * @author s26841
 */
public enum Weekday {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
    
    public boolean isWorkday(){
        return this != SATURDAY && this != SUNDAY;
    }
    
    public boolean isWeekend(){
        return !isWorkday();
    }
    
    public static void printDays(Weekday day){
        if (day.isWorkday()) {
            System.out.println(day + " munkanap!");
        }else{
            System.out.println(day + " pihenőnap!");
        }
    }
    
    public static void greaterOrLess(Weekday day){
        Weekday saturday = Weekday.SATURDAY;
        int result = day.compareTo(saturday);
        if (result < 0) {
            System.out.println(day + " elötte van a " + saturday);
        }else if (result == 0){
            System.out.println(day + " ugyanazon a napon van mint a " + saturday);
        }else{
            System.out.println(day + " utána van a " + saturday);
        }
    }
}
