/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strings;

/**
 *
 * @author s26841
 */
public class Strings {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, "
        + "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
        + "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        + " Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
        + " Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        StringBuilder sb = new StringBuilder(str);
        
        int sentencesCounter = 0;
        int itCounter = 0;
        int spaceCounter = 1;
        String eachSecondWords = "";
        
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == '.' || str.charAt(i) == '!' || str.charAt(i) == '?'){
                sentencesCounter++;
            }
        }
        
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == 'i' && str.charAt(i + 1) == 't'){
                itCounter++;
            }
        }

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' '){
                if(spaceCounter % 2 != 0){
                    eachSecondWords += " " + (str.substring((i + 1), str.indexOf(" ", (i + 1))));
                }
                spaceCounter++;
            }
        }
        
        System.out.println("Szöveg amit vizsgálunk:\n" + str);
        System.out.println("Mondatok száma a szövegben: " + sentencesCounter);
        System.out.printf("\" it \" szó %d alkalommal fordúl elő a szövegben \n", itCounter);
        System.out.println("Első és az utolsó \" it \" közötti rész: \n" + str.substring(str.indexOf("it") + 2, str.lastIndexOf("it")));
        System.out.println("Minden második szó: \n" + eachSecondWords);
        System.out.println("Kicsi \" a \" cserélve nagy \" A \" karakterre: \n" + str.replace("a", "A"));
        System.out.println("Szöveg visszafelé: \n" + sb.reverse());
    }
}
