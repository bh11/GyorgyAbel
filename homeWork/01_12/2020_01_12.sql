-- create database bh11;
-- use bh11;
-- create table students ( id int auto_increment primary key,
-- 						nev varchar(40),
--                         born_of_year int, 
-- 						born_of_city varchar(50),
--                         class_number int
--                         );
-- alter table students MODIFY id int not null auto_increment;
-- select * from students;
-- insert into students (nev, born_of_year, born_of_city, class_number)
-- 			values ("Laza Géza", 2002, "Kecskemét", 10),
-- 					("Szabó Géza", 2001, "Szeged", 9),
--                     ("Kiss Leopold", 1998, "Pécs", 8);
-- SET SQL_SAFE_UPDATES = 0;
-- update students set nev="Nagy Leopold" where nev="Kiss Leopold";
-- select distinct nev from students;
-- select * from students;
-- select * from students where born_of_year between 1998 and 2000; 
-- select * from students where born_of_year between 1998 and 2000 limit 2;

-- create table class (id int primary key auto_increment, name varchar (50));
-- select * from students,class;

-- create table customer (customer_id int auto_increment primary key,
-- 						customer_name varchar (40),
--                         contact_name varchar (40), 
--                         address varchar(50), 
--                         city varchar (100),
--                         postal_code varchar (50),
--                         country varchar (50));

-- insert into customer (customer_name, contact_name, address, city, postal_code, country)
-- 					values ("Teszt János", "Teszt Jánosné", "Kémény utca", "Berlin", 12233, "Németország"),
-- 						   ("Tészta Béla", "Tészta Béláné", "Cserép utca", "Mexico", 5345, "Maxico"),
--                            ("Laza Géza", "Laza Gézáné", "Lapos utca", "Mexico", 5346, "Mexico"),
--                            ("Szemes János", "Szemesné Katika", "Poló utca", "London", "WA1 1DP", "UK"),
--                            ("Lábas Olga", "Lábas Róbert", "Keszeg utca", "Szaszi", "2t468", "Svédország");

-- feladatok
-- selects the "CustomerName" and "City" columns from the "Customers" table:
-- select customer_name, city from customer;

-- selects all the columns from the "Customers" table:
-- select * from customer;

-- selects only the DISTINCT values from the "Country" column in the "Customers" table:
-- select distinct country from customer;

-- selects the number of different (distinct) customer countries:
-- select count(distinct customer_id) from customer;

-- selects all the customers from the country "Mexico", in the "Customers" table:
-- select * from customer where country="Mexico";

-- selects all fields from "Customers" where country is "Germany" AND city is "Berlin":
-- select * from customer where country="Németország" and city="Berlin";

-- selects all fields from "Customers" where city is "Berlin" OR "München":
-- select * from customer where city in ("Berlin", "München");

-- selects all fields from "Customers" where country is "Germany" OR "Spain":
-- select * from customer where country in ("Németország","Spanyolország");

-- selects all fields from "Customers" where country is NOT "Germany":
-- select * from customer where country<>"Németország"; 

-- selects all fields from "Customers" where country is "Germany" AND city must be "Berlin" OR "München"
-- select * from customer where country="Németország" and city in ("Berlin","München");

-- selects all fields from "Customers" where country is NOT "Germany" and NOT "USA":
-- select * from customer where country not in("Németország","USA");

-- selects all customers from the "Customers" table, sorted by the "Country" column:
-- select * from customer order by country ASC;

-- selects all customers from the "Customers" table, sorted DESCENDING by the "Country" column:
-- select * from customer order by country DESC;

-- selects all customers from the "Customers" table, sorted by the "Country" and the "CustomerName" column. 
-- This means that it orders by Country, but if some rows have the same Country, it orders them by CustomerName:
-- select * from customer order by country, customer_name;

-- selects all customers from the "Customers" table, 
-- sorted ascending by the "Country" and descending by the "CustomerName" column:
-- select * from customer order by country ASC, customer_name DESC;

-- lists all customers with a NULL value in the "Address" field:
-- select * from customer where address is null;

-- lists all customers with a value in the "Address" field:
-- select * from customer where address is not null;

-- updates the first customer (CustomerID = 1) with a new contact person and a new city.
-- update customer set contact_name="new_contact_person" where customer_id=1;
-- select * from customer where customer_id = 1;

-- update the contactname to "Juan" for all records where country is "Mexico":
-- update customer set contact_name = "Juan" where country = "Mexico";
-- select * from customer where country = "Mexico";

-- deletes the customer "Alfreds Futterkiste" from the "Customers" table:
-- delete from customer where customer_name = "Laza Géza";
-- select * from customer;

-- selects the first three records from the "Customers" table:
-- select * from customer order by customer_id ASC limit 3;

-- selects all customers with a CustomerName starting with "a":
-- select * from customer where customer_name LIKE "a%";

-- selects all customers with a CustomerName ending with "a":
-- select * from customer where customer_name LIKE "%a";

-- selects all customers with a CustomerName that have "or" in any position:
-- select * from customer where customer_name LIKE "%or%";

-- selects all customers with a CustomerName that have "r" in the second position:
-- select * from customer where customer_name LIKE "_r%";

-- selects all customers with a CustomerName that starts with "a" and are at least 3 characters in length:
-- select * from customer where customer_name LIKE "a%" and length(customer_name) < 3;

-- selects all customers with a ContactName that starts with "a" and ends with "o":
-- select * from customer where customer_name LIKE "a%" and "%o";

-- selects all customers with a CustomerName that does NOT start with "a":
-- select * from customer where customer_name not like "a%";

-- selects all customers that are located in "Germany", "France" or "UK":
-- select * from customer where country in ("Németország", "Franciaország", "UK");

-- selects all customers that are NOT located in "Germany", "France" or "UK":
-- select * from customer where country not in ("Németország", "Franciaország", "UK");

-- selects all customers that are from the same countries as the suppliers:

-- start transaction; 
-- insert into customer (customer_name, contact_name, address, city, postal_code, country)
-- 								values("test", "test", "test", "test", "test", "test");

-- select * from customer;

-- rollback;

-- select * from customer;

-- create table product(product_id int auto_increment primary key, product_name varchar(150),
-- 					 supplier_id int, category_id int, unit varchar(150), price int);

-- insert into product(product_name, supplier_id, category_id, unit, price)
-- 			values("Chais", 1, 1, "10 boxes X 20 bags", 18),
-- 				  ("Chang", 1, 1, "24 - 12 oz bottles", 19),
--                   ("Anissed Sysrup", 1, 2, "12 - 550 ml bottles", 10),
--                   ("Chef Anton's Cajun Seasoning", 2, 2, "48 - 6 oz jars", 22),
--                   ("Chef Anton's Gumbo Mix", 2, 2, "36 boxes", 21.35);
                     
-- finds the price of the cheapest product:
-- select * from product where price = (select min(price) from product);

-- finds the price of the most expensive product:
-- select * from product where price = (select max(price) from product);

-- finds the number of products:
-- select count(product_id)AS numberOfProduct from product;

-- finds the average price of all products
-- select avg(price) from product;

-- selects all products with a price BETWEEN 10 and 20:
-- select * from product where price between 10 and 20;

-- display the products outside the range of the previous example, use NOT BETWEEN:
-- select * from product where price not between 10 and 20;

-- selects all products with a price BETWEEN 10 and 20. In addition; do not show products with a CategoryID of 1,2, or 3:
-- select  * from product where price between 10 and 20 and category_id not between 1 and 3;

-- create table orderDetailTable(order_detail_id int auto_increment primary key, order_id int, product_id int, quantity int);
-- create table orders(order_id int primary key not null, customer_id int, employee_id int, order_date date, shipper_id int);

-- insert into orderDetailTable(order_id, product_id, quantity)
-- 					  values(10248, 11, 12),
-- 							(10248, 42, 10),
--                             (10248, 72, 5),
--                             (10249, 14, 9),
--                             (10249, 51, 40);
-- insert into orders(order_id, customer_id, employee_id, order_date, shipper_id)
-- 			values(10248, 90, 5, 19960704, 3),
-- 					(10249, 84, 6, 19960705, 1),
--                     (10250, 34, 4, 19960708, 2),
--                     (10251, 84, 3, 19960709, 1),
--                     (10252, 76, 4, 19960710, 2);

-- alter table orders rename column cuttomer_id to customer_id;

-- sum of the "Quantity" fields in the "OrderDetails" table:
-- select sum(quantity) from orderDetailTable;

-- selects all orders with an OrderDate BETWEEN '01-July-1996' and '31-July-1996':
-- select * from orders where order_date between 19960701 and 19960731; 

-- lists the number of customers in each country:
-- select count(customer_id),country from customer group by country;

-- lists the number of customers in each country, sorted high to low:
-- select count(customer_id), country from customer group by country order by customer_name desc;

-- lists the number of customers in each country. Only include countries with more than 5 customers:
-- select count(customer_id)>5, country from customer group by country;

-- lists the number of customers in each country, sorted high to low (Only include countries with more than 5 customers):
-- select count(customer_id)>5, country from customer group by country order by customer_name desc;

-- create table employee(employee_id int auto_increment primary key,
--  					last_name varchar(40), first_name varchar(40), birth_date date, photo varchar(150));

-- insert into employee (last_name, first_name, birth_date, photo)
-- 		values("Davollo", "Nancy", 19681208, "EmpID1.pic"),
-- 			   ("Fuller", "Andrew", 19520219, "EmpID2.pic"),
--                ("Leverling", "Janet", 19630830, "EmpID3.pic");

-- selects all orders with customer information:
-- select customer.customer_name, orders.order_id from customer inner join orders on orders.customer_id = customer.customer_id;

-- select all customers, and any orders they might have
-- select customer.customer_name, orders.order_id from customer left join orders on orders.customer_id = customer.customer_id;

-- return all employees, and any orders they might have placed
-- select order_id, last_name, first_name from orders right join employee on orders.employee_id = employee.employee_id;

-- selects all customers, and all orders
-- select customer.customer_name, orders.order_id from customer full outer join orders on customer.customer_id = orders.customer_id;

-- matches customers that are from the same city
-- select a.costumer_name, b.customer_name, a.city from customer where a.customer_id <> b.customer_id and a.city = b.city; 

-- lists the number of orders sent by each shipper:

-- lists the employees that have registered more than 10 orders:

-- lists if the employees "Davolio" or "Fuller" have registered more than 25 orders:

-- Select all information in one result table

-- Lekérdezi a Nagy vezetéknevű diák kódját és vezetéknevét:
-- select vnév, dkód from diákok where vnév = "Nagy";

-- Lekérdezi a nullával kezdődő órakódu regisztrációkat:
-- select okód from regisztrácó where ókod like "0%";

-- Lekérdezi a diákok számát:
-- select count(dkód) from diákok;

-- Lekérdezi a tandíj átlagát:
-- select avg(tandíj) from tandijak;

-- Lekérdezi azon órák listája, amelyekre már jelentkezett valaki. 
-- select órák.okód, regisztráció.dkód from órák inner join regisztráció on regisztráció.okód = órák.okód;

-- Szeretném megtudni, hogy melyik órára hányan diák regisztrált. 
-- select órák.okód, count(regisztrácó.dkód) from órák left join regisztrácó on regisztrácó.okód=órák.okód;

-- Keresem azokat az órákat, ahol 3-nál többen regisztráltak:
-- select órák.okód, count(regisztrácó.dkód) from órák join regisztrácó on regisztrácó.okód=órák.okód
-- group by órák.okód where regisztrácó.okód > 3;

-- Készítsünk névsorszerint rendezett listát a „nem János” diákokról! 
-- select vnév, knév from diákok where knév <> "János" order by knév, vnév asc;

-- Az alábbi paranccsal Katalin és Kinga keresztnev diákjainkat listázhatjuk ki.
-- select knév, vnév from diákok where knév in ("Kinga", Ktalain);






 

						
                       