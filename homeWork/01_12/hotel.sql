-- create table hotel(hotel_id int auto_increment, hotel_name varchar(150), hotel_address varchar(200), primary key (hotel_id));

-- create table room(room_id int auto_increment, hotel_id int, type varchar(10),price double, 
-- 					primary key(room_id), foreign key (hotel_id) references hotel(hotel_id));
                  
-- create table booking(booking_id int auto_increment, hotel_id int, guest_id int, date_from date, date_to date, room_id int,
-- 					 primary key (booking_id), 
--                      foreign key (hotel_id) references hotel(hotel_id),
--                      foreign key (guest_id) references guest(guest_id),
--                      foreign key (room_id) references room(room_id));
                     
-- create table guest(guest_id int auto_increment, guest_name varchar(150), guest_address varchar(300), primary key(guest_id));

-- INSERT INTO hotel (hotel_name, hotel_address)
-- 		VALUES ("Grosvenor Hotel", "London");

-- INSERT INTO room (hotel_id, type, price)
-- 		VALUES (1, "S", 72.00);

-- INSERT INTO hotel (hotel_name, hotel_address)
-- 		VALUES ("Grosvenor Hotel", "London");

-- CREATE TABLE archive (
--     id INT PRIMARY KEY AUTO_INCREMENT,
--     hotel_id VARCHAR(15),
--     guest_id VARCHAR(15) ,
--     date_from DATE,
--     date_to DATE,
--     room_id VARCHAR(15),
--     FOREIGN KEY (hotel_id) REFERENCES hotel (id),
--     FOREIGN KEY (guest_id) REFERENCES guest (id),
--     FOREIGN KEY (room_id)  REFERENCES room (id)
-- );
-- insert into archive (id,hotel_id,guest_id,date_from,date_to,room_id) 
-- select * 
-- from booking 
-- where date_from < 20000101;
-- DELETE FROM booking 
-- WHERE date_from < 20000101;

-- List full details of all hotels.
-- select * from hotel;

-- List full details of all hotels in London.
-- select * from hotel where address = 'London';

-- List the names and addresses of all guests in London, alphabetically ordered by name.
-- select guest_name from guest where address = "London" order by guest_name asc;

-- List all double or family rooms with a price below £40.00 per night, in ascending order of price.
-- select * from room where type = 'double' or type = 'family' and price < 40 order by price asc;

-- List the bookings for which no date_to has been specified.
-- select * from booking where date_to is null;

-- How many hotels are there?
-- select count(hotel_id) from hotel;

-- What is the average price of a room? 
-- select avg(price) from room;

-- What is the total revenue per night from all double rooms? 
-- select count(price) from room inner join booking on booking.room_id = room.room_id
-- where room.type = 'double' group by room_id;

-- How many hotels are there? 
-- select count(hotel_id) from hotel;

-- What is the average price of a room? 
-- select avg(rice) from room;

-- What is the total revenue per night from all double rooms? 
-- select sum(price) from room where type = "double";

-- How many different guests have made bookings for August?
-- select count(distinct guest_id) from booking where date_from > 20000801 and date_to < 20000831;

-- List the number of rooms in each hotel. 
-- select hotel.hotel_name, count(distinct room.room_id) from hotel inner join room on hotel.hotel_id = room.hotel_id group by hotel.hotel_name;








