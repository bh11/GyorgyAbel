/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lowercasetouppercase;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class LowercaseToUppercase {

    /**
     * @param args the command line arguments
     */
    
    static Scanner SC = new Scanner (System.in, "cp1250");
    
    static char inputChar(){
        do{
            System.out.println("Please enter a lowercase letter: ");
            char character = SC.next().charAt(0);
            if(Character.toString(character).matches("[a-zíöüóőúéáű]")){
                return character;
            }else{
                System.out.println("You haven't entered the letter or lowercase letter!");
            }
        }while(true);
    }
    
    static char lowercaseToUppercase(char character){
        return Character.toUpperCase(character);
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(lowercaseToUppercase(inputChar()));
    }
    
}
