/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reversearray;

/**
 *
 * @author s26841
 */

public class ReverseArray {
    
    static int[] NUMBERS = new int [30];

    static int[] randomNumbers (int a, int b){        
        System.out.print("Elements of array: ");
        for (int i = 0; i < NUMBERS.length; i++) {
            int randomNumbers = (int) (Math.random ()*(b-a)+a);
            NUMBERS[i] = randomNumbers;
            System.out.print(NUMBERS[i] + " ");
        }
        System.out.println();
        return NUMBERS;
    }
    
    static void reverseArray(int[] numbers) {
        System.out.print("Every second reverse element of array: ");
        for (int i = NUMBERS.length - 1; i >= 0; i -= 2) {
            System.out.print(NUMBERS[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // TODO code application logic here
        
        int a = -200;
        int b = 200;
        
        randomNumbers(a, b);
        reverseArray(NUMBERS);
    }
}
