/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombgame;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class BombGame {
    static final Scanner SC = new Scanner(System.in);
    
    static final char DEADLY_BOMB = 'D';
    static final char LIGHT_BOMB = '*';
    static final char GIFT_LIFE = 'G';
    
    static final char PLAYER = '8';
    static int PLAYERX_POSITION = 0;
    static int PLAYERY_POSITION = 0;
    
    static final int MIN_SIZE = 10;
    static final int MAX_SIZE = 15;
    
    static final int MIN_BOMB_NUMBER = 8;
    static final int MAX_BOMB_NUMBER = 20;
    
    static final char EMPTY = '\0';
    
    static int lifepoints = 5;
    
    static int counter = 0;
    
    static int getNumber(int from, int to){
        do{
            System.out.printf("Give me a number between %d and %d ", from ,to);
            if(SC.hasNextInt()){
                int number = SC.nextInt();
                    if(number >= from && number <= to){
                        return number;
                    }
            }else{
                nextLine();
            }
        }while(true);
    }
    
    static void nextLine(){
        SC.next();
    }

    static void printField (char[][] field){
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if(field[i][j] == EMPTY){
                    System.out.print("\\");
                }else{
                    System.out.print(field[i][j]);
                }
            }
            System.out.println();
        }
    }
    
    static void play(int row, int col){
        char [][] field =  new char [row][col];
        putPLayer(field);
        printField(field);
        putBombs(field);
        step(field);
    }
    
    static void putPLayer(char [][] field){
        field[PLAYERX_POSITION][PLAYERY_POSITION] = PLAYER;
    }

    static void putBombs(char [][] field){
        int numberOfBombs = getNumber(MIN_BOMB_NUMBER, MAX_BOMB_NUMBER);
        int deadlyBomb = deadlyBombsNumber(numberOfBombs);
        int lightBomb = lightBombsNumber(numberOfBombs);
        int giftLife = giftLife(numberOfBombs);
        setBombs(field, deadlyBomb, DEADLY_BOMB);
        setBombs(field, lightBomb, LIGHT_BOMB);
        setBombs(field, giftLife, GIFT_LIFE);
        printField(field);
    }
    
    static int deadlyBombsNumber(int numberOfBombs){
        return numberOfBombs / 2;
    }
    
    static int lightBombsNumber(int numberOfBombs){
        return numberOfBombs / 2;
    }
    
    static int giftLife(int numberOfBombs){
        return numberOfBombs = 3;
    }
    
    static int generateRandomNumber (int from, int to){
        return (int) (Math.random()*(to - from) + from);
    }
    
    static void setBombs (char [][] field, int count, char character){
        for (int i = 0; i < count; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[i].length);
            if(field[x][y] == EMPTY){
                field[x][y] = character;
            }else{
                i--;
            }
        }
    }
    
    static void handleBombs(char [][] field, int x, int y){
        switch (field[x][y]) {
            case DEADLY_BOMB:
                lifepoints = 0;
                break;
            case LIGHT_BOMB:
                lifepoints--;
                break;
            case GIFT_LIFE:
                lifepoints++;
                break;
            default:
                break;
        }
    }
    
    static void step (char [][] field){
        while (lifepoints > 0 && !isPlayerInTheLastRow(field)) {
            System.out.println();
            char c = SC.next().charAt(0);
            switch (c) {
                case 'r':
                    rightStep(field);
                    break;
                case 'd':
                    downStep(field);
                    break;
                case 'l':
                    leftStep(field);
                    break;
                case 'u':
                    upStep(field);
                    break;
            }
            printField(field);
            System.out.printf("Life Remain: %d", lifepoints);
            System.out.println();
            System.out.printf("You have stepped: %d \n", counter);
            if(lifepoints == 0){
                System.out.println("You are dead! :X");
            }
        }
    }
    
    static int counter(int number){
        return counter++;
    }
   
    static void upStep(char [][] field){
        if(!isPlayerInTheFirstRow(field)){
            handleBombs(field, PLAYERX_POSITION - 1, PLAYERY_POSITION);
            if(lifepoints > 0){
                field[PLAYERX_POSITION][PLAYERY_POSITION] = EMPTY;
                PLAYERX_POSITION--;
                counter(counter);
                field[PLAYERX_POSITION][PLAYERY_POSITION] = PLAYER;
            }
        }
    }
    
    static void downStep(char [][] field){
        if(!isPlayerInTheLastRow(field)){
            handleBombs(field, PLAYERX_POSITION + 1, PLAYERY_POSITION);
            if(lifepoints > 0){
                field[PLAYERX_POSITION][PLAYERY_POSITION] = EMPTY;
                PLAYERX_POSITION++;
                counter(counter);
                field[PLAYERX_POSITION][PLAYERY_POSITION] = PLAYER;
            }
        }
    }
    
    static void leftStep(char [][] field){
        if(!isPlayerInTheFirstColumn(field)){
            handleBombs(field, PLAYERX_POSITION, PLAYERY_POSITION - 1);
            if(lifepoints > 0){
                field[PLAYERX_POSITION][PLAYERY_POSITION] = EMPTY;
                PLAYERY_POSITION--;
                counter(counter);
                field[PLAYERX_POSITION][PLAYERY_POSITION] = PLAYER;
             }
        }
    }
    
    static void rightStep(char [][] field){
        if(!isPlayerInTheLastColumn(field)){
            handleBombs(field, PLAYERX_POSITION, PLAYERY_POSITION + 1);
            if(lifepoints > 0){
                field[PLAYERX_POSITION][PLAYERY_POSITION] = EMPTY;
                PLAYERY_POSITION++;
                counter(counter);
                field[PLAYERX_POSITION][PLAYERY_POSITION] = PLAYER;
             }
        }
    }
    
    static boolean isPlayerInTheLastColumn(char [][] field){
        return PLAYERY_POSITION == field[0].length -1;
    }
    
    static boolean isPlayerInTheFirstRow(char [][] field){
        return PLAYERX_POSITION <= field[0][0];
    }
    
    static boolean isPlayerInTheFirstColumn(char [][] field){
        return PLAYERY_POSITION <= field[0][0];
    }
    
    static boolean isPlayerInTheLastRow(char [][] field){
        return PLAYERX_POSITION == field.length -1;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int row = getNumber(MIN_SIZE, MAX_SIZE);
        int col = getNumber(MIN_SIZE, MAX_SIZE);
        
        play(row, col);
    }
    
}
