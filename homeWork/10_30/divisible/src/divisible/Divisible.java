/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package divisible;

/**
 *
 * @author s26841
 */
public class Divisible {

    /**
     * @param args the command line arguments
     */
    
    static int[] NUMBERS = new int [300];
    static int COUNTER = 0;

    static int[] randomNumbers (int a, int b){        
        System.out.print("Elements of array: ");
        for (int i = 0; i < NUMBERS.length; i++) {
            int randomNumbers = (int) (Math.random ()*(b-a)+a);
            NUMBERS[i] = randomNumbers;
            System.out.print(NUMBERS[i] + " ");
        }
        System.out.println();
        return NUMBERS;
    }
    
    static int divisible(int counter){
        System.out.print("Numbers are been divisible with three and not divisible with five: ");
        for (int i = 0; i < NUMBERS.length; i++) {
            if(NUMBERS[i] % 3 == 0 && NUMBERS[i] % 5 != 0){
                System.out.print(NUMBERS[i] + " ");
                COUNTER++;
            }
        }
        
        return counter;
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        int a = -200;
        int b = 200;
        
        randomNumbers(a, b);
        divisible(COUNTER);
        System.out.println();
        System.out.printf("%d numbers",COUNTER);
        System.out.println();
    }
}
