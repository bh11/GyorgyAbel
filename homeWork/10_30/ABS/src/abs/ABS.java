/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abs;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class ABS {

    static Scanner SC = new Scanner(System.in);
    
    static int number1 = 0;
    static int number2 = 0;
    
    static int getNumber(int number){
        do{
            System.out.println("Give me a number: ");
            if(SC.hasNextInt()){
                number = SC.nextInt();
                return number;
            }else{
                SC.next();
            }
        }while(true);
    }
    
    static int absNumber(int a, int b){
        a = Math.abs(a);
        b = Math.abs(b);
        
        if(a > b){
            return a;
        }else{
            return b;
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int a = getNumber(number1);
        int b = getNumber(number2);
        System.out.println("Higher absolute value: " + absNumber(a, b));
    }
    
}
