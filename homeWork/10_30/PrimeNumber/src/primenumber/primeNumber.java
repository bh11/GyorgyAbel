/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primenumber;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class primeNumber {

    static final Scanner SC = new Scanner(System.in);
    static int NUMBER = 0;
    static boolean prime = true;

    static int getNumber(int number) {
        do {
            System.out.println("You give me an integer positive number: ");
            if (SC.hasNextInt()) {
                number = SC.nextInt();
                    if(number > 0){
                        return number;
                    }
            } else {
                SC.next();
            }
        } while (true);
    }

    static boolean checkNumberIsprime(int number) {
        for (int i = 2; i < Math.sqrt(number) + 1; i++) {
            if (number % i == 0) {
                return false;                
            }
        }
        return true;
    }

    static int countPrime(int number) {
        int temp = 0;
        int count = 0;
        while (count < number) {
            if (checkNumberIsprime(temp)) {
                count++;
            }
            temp++;
        }
        return temp - 1;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int number = getNumber(NUMBER);
        checkNumberIsprime(number);
        System.out.printf("%d prime number: ", countPrime(number));
    }

}
