/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rectangle;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Rectangle {

    static Scanner SC = new Scanner (System.in);
    
    static int inputCheck(int number){
        do{
            System.out.println("Please enter a number: ");
            if(SC.hasNextInt()){
                number = SC.nextInt();
                return number;
            }else{
                System.out.println("You haven't entered number!");
                SC.next();
            }
        }while(true);
    }
    
    static void rectangleArea(int a, int b){
        System.out.println("Rectangle area is: " + (a*b));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int number1 = inputCheck(0);
        int number2 = inputCheck(0);

        rectangleArea(number1, number2);
        
    }
    
}
