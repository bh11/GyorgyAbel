/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trianglestar;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class TriangleStar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);

        System.out.println("Kérlek adjon meg egy pozitív egész számot: ");
            if(sc.hasNextInt()){
                int number = sc.nextInt();
                    if(number > 0){
                        for (int i = number; i > 0; i--) {
                            for (int j = i; j >= 1; j--) {
                                System.out.print("*");
                            }
                            System.out.println("");
                        }
                }else{
                        System.out.println("Nem pozitív egész számot adott meg!");
                }
            }else{
                System.out.println("Nem számot adott meg!");
            }
    }

}
