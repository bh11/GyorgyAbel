/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondnumbersystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author george
 */
public class SecondNumberSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        ArrayList<Integer> array = new ArrayList<Integer>();
        int rest = 0;
        int j = 0;
        System.out.println("Kérem adjon meg egy egész pozitív számot: ");
        if (sc.hasNextInt()) {
            int number = sc.nextInt();
            for (int i = number; i > 0; i = i / 2) {
                rest = i % 2;
                array.add(rest);  
            }
            Collections.reverse(array);
            System.out.println(array);
        } else {
            System.out.println("Nem számot adtál meg!");
        }
    }
    
}
