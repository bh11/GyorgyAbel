/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package christmastree;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class ChristmasTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner sc = new Scanner (System.in);
        int number = sc.nextInt();
        
        number = (number % 2 == 0)?number = number - 1 : number;
        
        for (int i = 1; i <= number; i++) {
                for (int j = 1; j <= number - i; j++) {
                    System.out.print(" ");
                }
                for (int j = 1; j <= 2 * i - 1; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
    }

}
