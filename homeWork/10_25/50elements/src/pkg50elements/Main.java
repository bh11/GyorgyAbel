/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg50elements;

/**
 *
 * @author george
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int[] array = new int[50];
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 200 + (-100));
        }
        System.out.println("A tömb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
            sum = sum + array[i];
        }
        System.out.println("");
        int maxValue = array[0];
        int maxIndex = 0;
        int minValue = array[0];
        int minIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];
                maxIndex = i;
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
                minIndex = i;
            }
        }
        System.out.println("A tömb elmeinek összege: " + sum);
        System.out.println("A tömb értékeinek átlaga: " + ((float)sum / array.length));
        System.out.println("A max elem: " + maxValue + " ez a tömb " + maxIndex + " -ik eleme.");
        System.out.println("A min elem: " + minValue + " ez a tömb " + minIndex + " -ik eleme.");
    }
    
}
