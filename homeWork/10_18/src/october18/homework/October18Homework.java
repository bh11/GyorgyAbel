/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package october18.homework;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class October18Homework {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ///////////////////////////////////////////////////////////////////////
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Számítások elvégzéséhez adja meg az első egész számot : ");
        int firstNumber = scanner.nextInt();
        System.out.println("Számítások elvégzéséhez adja meg a második egész számot : ");
        int secondNumber = scanner.nextInt();
        System.out.println("Számítások elvégzéséhez adja meg a harmadik egész számot : ");
        int thirdNumber = scanner.nextInt();
        float sum = firstNumber + secondNumber + thirdNumber;
        float average = sum / 3;
        System.out.println("A számok átlaga: " + average);
        
        ///////////////////////////////////////////////////////////////////////
        
        if(firstNumber >= secondNumber && firstNumber >= thirdNumber){
            System.out.println("A legnagyobb szám a: " + firstNumber);
        }else if(secondNumber >= firstNumber && secondNumber >= thirdNumber){
            System.out.println("A legnagyobb szám a: " + secondNumber);
        }else{
            System.out.println("A legnagyobb szám a: " + thirdNumber);
        }
        
        ///////////////////////////////////////////////////////////////////////
        
        if((firstNumber + secondNumber) > thirdNumber && (firstNumber + thirdNumber) > secondNumber && (thirdNumber + secondNumber) > firstNumber){
            System.out.println("Lehet háromszög!");
        }else{
            System.out.println("Nem lehet háromszög!");
        }
        
        ///////////////////////////////////////////////////////////////////////
        
        System.out.println("Adj meg további egész számot: ");
        int fourthNumber = scanner.nextInt();
        System.out.println("Adj meg még egy egész számot: ");
        int fifthNumber = scanner.nextInt();
        System.out.println("Adj meg egy matematikai műveletet ('+''-''*''/''%'): ");
        String operator = scanner.next();
        switch (operator) {
            case "+":
                System.out.println("Összeadva: " + (fourthNumber + fifthNumber));
                break;
            case "*":
                System.out.println("Szorzatuk: " + (fourthNumber * fifthNumber));
                break;
            case "-":
                System.out.println("Kivonva: " + (fourthNumber - fifthNumber));
                break;
            case "/":
                if(fifthNumber == 0){
                    System.out.println("0-val nem osztunk!!!");
                }else{
                    if((((float)fourthNumber / (float)fifthNumber) % 2) == 0){
                        System.out.println("Elosztva: " + (fourthNumber / fifthNumber));
                    }else{
                        System.out.println("Elosztva: " + ((float)fourthNumber / fifthNumber));
                    }
                }   break;
            case "%":
                if(fifthNumber == 0){
                    System.out.println("0-val nem képzünk maradékot!!!");
                }else{
                    System.out.println("maradék: " + (fourthNumber % fifthNumber));
                }   break;
            default:;
                break;
        }
    }
}
    
    

