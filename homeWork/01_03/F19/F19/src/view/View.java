/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Operator;

/**
 *
 * @author george
 */
public class View extends JFrame {

    private Controller controller;

    private JTextField jt = new JTextField(30);

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void showWindow() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    public void setText(String expr) {
        jt.setText(expr);
    }

    private JPanel buildNorth() {
        jt.setEditable(false);
        JPanel jp = new JPanel();
        jp.add(jt);

        return jp;
    }

    private JPanel buildCenter() {
        JPanel jp = new JPanel();
        jp.setLayout(new GridLayout(4, 3));

        int value = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                JButton jb = new JButton(String.valueOf(value++));
                jp.add(jb);

                jb.addActionListener(l -> {
                    JButton button = (JButton) l.getSource();
                    int nr = Integer.parseInt(button.getText());

                    controller.setNumber(nr);
                });
            }
        }

        addOperators(jp);

        return jp;
    }

    private void addOperators(JPanel jp) {
        JButton plus = new JButton("+");
        JButton subtraction = new JButton("-");
        JButton multiply = new JButton("*");
        JButton divison = new JButton("/");
        JButton equal = new JButton("=");
        JButton memory = new JButton("M+");

        plus.addActionListener(l -> controller.handleOperator(Operator.ADD));
        subtraction.addActionListener(l -> controller.handleOperator(Operator.SUBTRACTION));
        multiply.addActionListener(l -> controller.handleOperator(Operator.MULTIPLY));
        divison.addActionListener(l -> controller.handleOperator(Operator.DIVISON));
        equal.addActionListener(l -> controller.handleOperator(Operator.EQ));
        memory.addActionListener(l -> controller.saveModel());
        

        jp.add(plus);
        jp.add(subtraction);
        jp.add(multiply);
        jp.add(divison);
        jp.add(equal);
        jp.add(memory);
    }

}
