/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author george
 */
public enum Operator {
    ADD("+") {
        public int calc(int a, int b) {
            return a + b;
        }
    },
    SUBTRACTION("-"){
        public int calc(int a, int b){
            return a - b;
        }
    },
    MULTIPLY("*") {
        public int calc(int a, int b) {
            return a * b;
        }
    },
    DIVISON("/") {
        public int calc(int a, int b) {
            if (a != 0 && b != 0) {
                return a / b;
            }
            return 0;
        }
    },
    EQ("=") {
        public int calc(int a, int b) {
            return 0;
        }
    };

    private final String character;

    private Operator(String character) {
        this.character = character;
    }

    public String getCharacter() {
        return character;
    }

    public abstract int calc(int a, int b);

}
