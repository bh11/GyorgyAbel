/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saver;

import java.io.BufferedWriter;
import java.io.FileWriter;
import model.Operator;

/**
 *
 * @author george
 */
public class SaveModel {
    
    private static final String FILENAME = "output.txt";
    
    public void writeToFile(Integer num1, Integer num2, Operator operator){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))){
            
            if (num2 > 0 && operator.getCharacter() != null) {
                bw.write(num1.toString() + operator.getCharacter() + num2.toString());
            }else if(num1 > 0){
                bw.write(num1.toString());
            }
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
