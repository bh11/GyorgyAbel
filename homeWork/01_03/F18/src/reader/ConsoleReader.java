/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import saver.SaveWine;

/**
 *
 * @author george
 */
public class ConsoleReader {

    private int counter = 0;
    private static final String STOP = "SAVE";
    private StoreAlcohol store = new StoreAlcohol();
    
    public void read() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (counter <= 100 ) {                
                String line = br.readLine();
                if (STOP.equalsIgnoreCase(line) || line == null) {
                    store.report();
                    SaveWine.serializationWien(store.getWineListToSave());
                    break;
                }
                store.splitInputDatas(line);
                counter++;
            }  
        } catch (IOException ex) {  
            System.out.println(ex);  
        }
    }
}
