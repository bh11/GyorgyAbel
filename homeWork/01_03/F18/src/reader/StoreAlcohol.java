/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import alcohol.AbstractAlcohol;
import alcohol.AlcoholFactory;
import java.util.ArrayList;
import java.util.List;
import report.Report;

/**
 *
 * @author george
 */
public class StoreAlcohol {
    
    private static final String DELIMITER = " ";
    
    private final List <AbstractAlcohol> store = new ArrayList<>();
    private Report report = new Report();
    
    public void splitInputDatas(String line){
        String[] parameters = line.split(DELIMITER);
        AbstractAlcohol alcohol = AlcoholFactory.create(parameters);
        store.add(alcohol);
    }
    
    public void report(){
        report.report(store);
    }
    
    public List getWineListToSave(){
        return report.wineListToSave(store);
    }
}
