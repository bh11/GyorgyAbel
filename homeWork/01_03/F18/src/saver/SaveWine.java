/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class SaveWine {
    
    private static final String FILENAME = "wine.ser";
    
    public static void serializationWien(List wine){
        try (FileOutputStream fs = new FileOutputStream(FILENAME) ){
             ObjectOutputStream ou = new ObjectOutputStream(fs);
             ou.writeObject(wine);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SaveWine.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SaveWine.class.getName()).log(Level.SEVERE,null, ex);
        }
    }
    
    public static List deserializationWien(){
        List wine = null;
        try (FileInputStream fs = new FileInputStream(FILENAME)){
            ObjectInputStream oi = new ObjectInputStream(fs);
            wine = (List) oi.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SaveWine.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(SaveWine.class.getName()).log(Level.SEVERE,null, ex);
        }
        return wine;
    }
}
