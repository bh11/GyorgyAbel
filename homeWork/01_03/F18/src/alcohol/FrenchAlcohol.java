/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcohol;

import exception.FrozenException;
import manufacturer.Manufacturer;

/**
 *
 * @author george
 */
public class FrenchAlcohol extends AbstractAlcohol{
    
    private boolean wasFrozen;

    public FrenchAlcohol(AlcoholType type, Manufacturer manufacturer, int price, int alcoholPercent, String countryType) {
        super(type, manufacturer, price, alcoholPercent, countryType);
    }
    
    public boolean freeze(){
        return this.wasFrozen = true;
    }

    public boolean isWasFrozen() {
        return wasFrozen;
    }

    public void setWasFrozen(boolean wasFrozen) {
        this.wasFrozen = wasFrozen;
    }
    
    public void transport() throws FrozenException{
        if (freeze()) {
            throw new FrozenException();
        }
        System.out.println("You can transport it, because it hasn't been frozen so far.");
    }

    @Override
    public String toString() {
        return super.toString() + "FrenchAlcohol{" + "wasFrozen=" + wasFrozen + '}';
    }
}
