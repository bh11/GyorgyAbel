/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcohol;

import manufacturer.Manufacturer;

/**
 *
 * @author george
 */
public class HungarianAlcohol extends AbstractAlcohol{

    public HungarianAlcohol(AlcoholType type, Manufacturer manufacturer, int price, int alcoholPercent, String countryType) {
        super(type, manufacturer, price, alcoholPercent, countryType);
    }

    public void drinkAlcohol(){
        System.out.println("Drinking");
    }
    
    public void extenutae(){
        setAlcoholPercent((int)(getAlcoholPercent() / 2));
    }

    @Override
    public String toString() {
        return super.toString() + "HungarianAlcohol{" + '}';
    }
}
