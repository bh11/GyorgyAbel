/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcohol;

import java.time.LocalDateTime;
import labelGenerator.LabelGenerator;
import manufacturer.Manufacturer;

/**
 *
 * @author george
 */
public class GermanAlcohol extends AbstractAlcohol{
    
    private String label;
    private String createDateOfLabel;
    
    LabelGenerator randomLabel = new LabelGenerator();

    public GermanAlcohol(AlcoholType type, Manufacturer manufacturer, int price, int alcoholPercent, String countryType) {
        super(type, manufacturer, price, alcoholPercent, countryType);
        this.label = randomLabel.labelGenerator();
        this.createDateOfLabel = LocalDateTime.now().toString();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCreateDateOfLabel() {
        return createDateOfLabel;
    }

    @Override
    public String toString() {
        return super.toString() + "GermanAlcohol{" + "label=" + label + ", createDateOfLabel=" + createDateOfLabel + '}';
    }
}
