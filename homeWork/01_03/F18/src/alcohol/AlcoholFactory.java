/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcohol;

import manufacturer.Manufacturer;

/**
 *
 * @author george
 */
public class AlcoholFactory {
    
    public static AbstractAlcohol create(String [] parameters){
          
        AlcoholType type = AlcoholType.valueOf(parameters[0].toUpperCase());
        Manufacturer manufacture = new Manufacturer(parameters[1], "address");
        int price = Integer.parseInt(parameters[2]);
        int alcoholPersent = Integer.parseInt(parameters[3]);
        String countryType = parameters[4];
        
        if ("hungarian".equals(countryType.toLowerCase())) {
            return new HungarianAlcohol(type, manufacture, price, alcoholPersent, countryType);
        }
        
        if ("german".equals(countryType.toLowerCase())) {
            return new GermanAlcohol(type, manufacture, price, alcoholPersent, countryType);
        }
        
        if ("french".equals(countryType.toLowerCase())) {
            return new FrenchAlcohol(type, manufacture, price, alcoholPersent, countryType);
        }  
        return null;
    }
}
