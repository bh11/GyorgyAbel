/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcohol;

import idgenerator.IdGenerator;
import java.io.Serializable;
import manufacturer.Manufacturer;

/**
 *
 * @author george
 */
public class AbstractAlcohol implements Comparable<AbstractAlcohol>, Serializable{
    
    private AlcoholType type;
    private Manufacturer manufacturer;
    private int id;
    private int alcoholPercent;
    private int price;
    private String countryType;
    
    IdGenerator generator = new IdGenerator();

    public AbstractAlcohol() {
    }
    
    

    public AbstractAlcohol(AlcoholType type, Manufacturer manufacturer,int price, int alcoholPercent, String countryType) {
        this.type = type;
        this.manufacturer = manufacturer;
        this.id = generator.idGenerator();
        this.alcoholPercent = alcoholPercent;
        this.price = price;
        this.countryType = countryType;
    }

    public AlcoholType getType() {
        return type;
    }

    public void setType(AlcoholType type) {
        this.type = type;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAlcoholPercent() {
        return alcoholPercent;
    }

    public void setAlcoholPercent(int alcoholPercent) {
        this.alcoholPercent = alcoholPercent;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCountryType() {
        return countryType;
    }

    public void setCountryType(String countryType) {
        this.countryType = countryType;
    }

    @Override
    public String toString() {
        return "AbstractAlcohol{" + "type=" + type + ", manufacturer=" + manufacturer + ", id=" + id + ", alcoholPercent=" + alcoholPercent + ", price=" + price + ", countryType=" + countryType + '}';
    }
   
    @Override
    public int compareTo(AbstractAlcohol id) {
        return this.id - id.getId();
    }
}
