/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import alcohol.AbstractAlcohol;
import alcohol.AlcoholType;
import alcohol.FrenchAlcohol;
import alcohol.GermanAlcohol;
import alcohol.HungarianAlcohol;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author george
 */
public class Report {
    
    public void report (List<AbstractAlcohol> store){
        
        System.out.println("\nNumber of save products: " + numberOfSaveWine(store));
        System.out.println("\nProduct number by SpecialGroup: ");
            System.out.println("Hungarian products is: " + productsBySpecialGroups(store)[0]);
            System.out.println("German products is: " + productsBySpecialGroups(store)[1]);
            System.out.println("French products is: " + productsBySpecialGroups(store)[2]);
        System.out.println("\nWien number is in the store: " + numberOfWine(store));
        System.out.println("\nManufacturer by the store: " + manufaturersByStore(store));
        
    }
    
    public long numberOfSaveWine (List<AbstractAlcohol> store){
        long numberOfWien =        
            store.stream()
                 .filter(p -> AlcoholType.WINE.equals(p.getType()))
                 .count();
        
        return numberOfWien;
    }
    
    public long [] productsBySpecialGroups(List<AbstractAlcohol> store){
        long [] specialGroup = new long[3];
        
        specialGroup[0] = 
            store.stream()
                 .filter(p -> p instanceof HungarianAlcohol)
                 .count();
        specialGroup[1] = 
            store.stream()
                 .filter(p -> p instanceof GermanAlcohol)
                 .count();
        specialGroup[2] = 
            store.stream()
                 .filter(p -> p instanceof FrenchAlcohol)
                 .count();
        
        return specialGroup;
    }
    
    public long numberOfWine (List<AbstractAlcohol> store){
        long numberOfWien = 
            store.stream()
                 .filter(p -> AlcoholType.WINE.equals(p.getType()))
                 .count();
        return numberOfWien;
    }
    
    public List manufaturersByStore(List<AbstractAlcohol> store){
        List manufaturersByStore =
            store.stream()
                 .map(p -> p.getManufacturer().getName())
                 .distinct()
                 .collect(Collectors.toList());
        return manufaturersByStore;
    }
    
    public List wineListToSave(List<AbstractAlcohol> store){
        List wineList = 
            store.stream()
                 .filter(p -> AlcoholType.WINE.equals(p.getType()))
                 .collect(Collectors.toList());
        return wineList;
    }
}
