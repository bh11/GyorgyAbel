/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f1;

/**
 *
 * @author george
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Bag <String> bag = new Bag();

        bag.addElements("firstString");
        bag.addElements("secondString");
        bag.addElements("thirdString");

        System.out.println(bag.toString());

        bag.removeElement("firstString");

        System.out.println(bag.toString());
    }

}
