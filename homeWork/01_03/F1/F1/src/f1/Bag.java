/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f1;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class Bag<T>{
    private List<T> list= new ArrayList<>();

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
    
    public boolean addElements(T element){
        return list.add(element);
    }
    
    public boolean removeElement(T element){
        return list.remove(element);
    }

    @Override
    public String toString() {
        return "" + list;
    }
}
