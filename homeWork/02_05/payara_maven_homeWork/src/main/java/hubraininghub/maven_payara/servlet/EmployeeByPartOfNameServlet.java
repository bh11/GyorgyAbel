/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.maven_payara.servlet;

import hubraininghub.maven_payara.service.EmployeeService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author s26841
 */
@WebServlet(name = "EmployeeByPartOfNameServlet", urlPatterns = {"/EmployeeByPartOfNameServlet"})
public class EmployeeByPartOfNameServlet extends HttpServlet {

    @Inject
    private EmployeeService service;
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String partOfName = request.getParameter("partOfName");  
        List<String> filteredNames = service.getNamesByPartOfName(partOfName);

        request.setAttribute("names", filteredNames);
        request.getRequestDispatcher("/WEB-INF/empnames.jsp").forward(request, response);
    }
}
