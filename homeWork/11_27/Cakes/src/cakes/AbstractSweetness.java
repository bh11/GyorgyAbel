/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cakes;

/**
 *
 * @author s26841
 */
public abstract class AbstractSweetness {
    private String fantasyName;
    private int calorieNumber;
    
    public abstract String suggestedServing();
    
    public AbstractSweetness(String fantasyName, int chalorieNumber){
        this.fantasyName = fantasyName;
        this.calorieNumber = chalorieNumber;
    }
    
    public String getFantasyName() {
        return this.fantasyName;
    }
    
    public int getCalorieNumber() {
        return this.calorieNumber;
    }
    
    public void setFantasyName(String fantasyName) {
        this.fantasyName = fantasyName;
    }

    public void setCalorieNumber(int calorieNumber) {
        this.calorieNumber = calorieNumber;
    }
    
    @Override
    public String toString() {
        return "Név: " + fantasyName + ", kalória száma: " + calorieNumber;
    }
}
