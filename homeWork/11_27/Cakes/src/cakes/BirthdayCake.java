/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cakes;

/**
 *
 * @author s26841
 */
public class BirthdayCake extends Cake{
    
    private String desire;
    private String sameOfComponent;
    
    public BirthdayCake(String fantasyName, int calorieNumber, String componentOfCake, String desire) {
        super(fantasyName, calorieNumber, componentOfCake);
        this.desire = desire;
    } 
    
    private String checkSameComponents(String sameOfComponent){
        String [] components = super.getComponentOfCake().split(" ");
        for (int i = 0; i < components.length; i++) {
            for (int j = i + 1; j < components.length; j++) {
                if (components[i].equals(components[j])) {
                    System.out.println("Két összetevő ugyanaz!");
                    return components[i];
                }
            }
        }
        return this.sameOfComponent;
    }
    
    
   @Override
    public String suggestedServing() {
        return " Javasolt tálalás tányéron szeletelve.";
    }

    @Override
    public String toString() {
        return "Szülinapi torta neve: " +
                super.getFantasyName() + " (kalória száma: " +
                super.getCalorieNumber() + ") (öszzetevői: " +
                super.getComponentOfCake() + ") (kívánság: " +
                desire + " )" + "( tálalási javaslat: " +
                suggestedServing() + " )" +
                checkSameComponents(sameOfComponent);
    }
}
