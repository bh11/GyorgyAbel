/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cakes;

/**
 *
 * @author s26841
 */
public class Cake extends AbstractSweetness{

    private String componentOfCake;
    
    public static final int MAX_COMPONENT_OF_CAKE = 5; 
    private int numberOfComponent;

    public Cake (String fantasyName, int calorieNumber, String componentOfCake){
        super(fantasyName, calorieNumber);
        this.componentOfCake = componentOfCake;
    }
    
    private int checkMaxComponentNumber(int numberOfComponent){
        String [] components = this.componentOfCake.split(" ");
        if (components.length <= MAX_COMPONENT_OF_CAKE) {
            return numberOfComponent = components.length;
        }else{
            System.out.println("Maximum 5 összetevőt tartalmazhat!");
            return numberOfComponent = 0;
        }
    }

    @Override
    public String suggestedServing() {
        return " Tálalása tálcán.";
    }

    @Override
    public String toString() {
        return "Torta neve: " +
                super.getFantasyName() + " (kalória száma: " +
                super.getCalorieNumber() + ") (összetevői: " +
                componentOfCake + ") (összetevők száma: " +
                checkMaxComponentNumber(numberOfComponent) + ") (tálalási javaslat: " +
                suggestedServing() + " )";
    }
    
    public String getComponentOfCake() {
        return this.componentOfCake;
    }

    public void setComponentOfCake(String componentOfCake) {
        this.componentOfCake = componentOfCake;
    }
}
