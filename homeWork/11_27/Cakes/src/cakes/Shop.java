/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cakes;
/**
 *
 * @author s26841
 */
public class Shop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        AbstractSweetness[] sweets = new AbstractSweetness [5];
               
        sweets [0] = new Cake("Csupa", 600, "cukor só csoki vanilia túró");
        sweets [1] = new Cake("Csoda", 950, "cukor só csoki vanilia túró szeder");
        sweets [2] = new BirthdayCake("Csudás", 604, "cukor só csoki vanilia szeder", "valami csoda");
        sweets [3] = new BirthdayCake("Csodás", 602, "cukor csoki vanilia szeder szeder", "másik csoda");
        sweets [4] = new BirthdayCake("Csupa Csudás", 560, "cukor só csoki vanilia túró szeder", "harmadik csoda");
        
        int sum = 0;
        for (AbstractSweetness sweet : sweets){
            System.out.println(sweet.toString());
            sum += sweet.getCalorieNumber();
        }
        
        System.out.println("Össz kalória szám: " + sum + "\n");
        
        
        sum = 0;
        
        for (int i = 0; i < sweets.length; i++) {
            System.out.println(sweets[i]);
        }
    }  
}
