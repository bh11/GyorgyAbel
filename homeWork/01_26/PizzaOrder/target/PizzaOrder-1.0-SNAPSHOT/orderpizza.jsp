<%-- 
    Document   : orderpizza
    Created on : 25-Jan-2020, 15:02:31
    Author     : george
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div style="margin: auto; width: 50%; border: 5px solid lightskyblue; padding: 10px; background-color: whitesmoke">
            <form method="post" action="PizzaOrder" >
                <fieldset>
                <legend>Order Pizza</legend>
                    <p>Name of pizza </p>
                    <select name="pizza_name" size=”1”>
                        <option value="first" selected>Sajtos pizza</option>
                        <option value="second">Szalámis pizza</option>
                        <option value="third">Sonkás pizza</option>
                        <option value="fourth">Tonhalas pizza</option>
                    </select><br>
                    <p>Size of pizza </p><input type="number" name="pizza_size" /><br>
                    <p>Name: </p><input type="text" name="user_name" /><br>
                    <p>Ship Address: </p><input type="text" name="user_address"/><br><br>
                    <input type="submit" value="Order Pizza"/>
                </fieldset>
            </form>
        </div>
    </body>
</html>
