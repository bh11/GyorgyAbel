/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.pizzaorder;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author george
 */
public class PizzaOrderServlet extends HttpServlet {

    private PizzaDao pizzaDao = new PizzaDao();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            //doPost(request, response);
    request.getRequestDispatcher("orderpizza.jsp").forward(request, response);
            
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
                
        
            String userName = request.getParameter("user_name");
            String shipAddress = request.getParameter("user_address");
            String pizzaName = request.getParameter("pizza_name");
            Integer pizzaSize = Integer.parseInt(request.getParameter("pizza_size"));

            request.setAttribute("orderpizza", pizzaDao.saveOrder(userName, shipAddress, pizzaName, pizzaSize));
            
            request.getRequestDispatcher("orderpizza.jsp").forward(request, response);
            
    }
}
