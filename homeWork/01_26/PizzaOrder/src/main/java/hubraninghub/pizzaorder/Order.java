/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.pizzaorder;

/**
 *
 * @author george
 */
public class Order {
    
    private String userName;
    private String shipAddress;
    private String pizzaName;
    private int pizzaSize;

    public Order(String userName, String shipAddress, String pizzaName, int pizzaSize) {
        this.userName = userName;
        this.shipAddress = shipAddress;
        this.pizzaName = pizzaName;
        this.pizzaSize = pizzaSize;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public int getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(int pizzaSize) {
        this.pizzaSize = pizzaSize;
    }
}
