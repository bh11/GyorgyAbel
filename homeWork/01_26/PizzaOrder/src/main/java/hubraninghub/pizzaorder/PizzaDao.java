/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraninghub.pizzaorder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class PizzaDao {
    
    private static final String URL = "jdbc:mysql://localhost:3306/pizza?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PW = "verbatim";

    
    public String saveOrder(String userName, String shipAddress, String pizzaName, Integer pizzaSize){
        
        String sql = "insert into orders(user_name, user_address, pizza_name, pizza_size)"
                                + "values (?, ?, ?, ?)";
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PizzaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, shipAddress);
            preparedStatement.setString(3, pizzaName);
            preparedStatement.setInt(4, pizzaSize);
            
            if (preparedStatement.executeUpdate() > 0) {
                System.out.println(preparedStatement.executeUpdate());
                return "successful!";
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(PizzaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "unsuccessful!";
    }
}
