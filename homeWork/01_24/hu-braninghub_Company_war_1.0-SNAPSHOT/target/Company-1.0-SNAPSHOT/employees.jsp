<%-- 
    Document   : employees
    Created on : 22-Jan-2020, 17:24:49
    Author     : george
--%>

<%@page import="hu.braninghub.company.Department"%>
<%@page import="hu.braninghub.company.Employee"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div style="margin: auto; min-height: 80px; width: 50%; border: 5px solid lightskyblue; padding: 10px; background-color: whitesmoke">
            <%  List<Employee> employees = (List<Employee>)request.getAttribute("employees"); %> 
            <table border="3" align="center">
                <thead>
                    <tr>
                        <th>Employees</th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Salary</th>
                        <th>Department</th>
                    </tr>
                </thead>
                <tbody>
                    <% for (int i = 0; i < employees.size(); i++) { %>
                    <tr>
                        <td><%= employees.get(i).getName()%></td>
                        <td><%= employees.get(i).getAddress()%></td>
                        <td><%= employees.get(i).getSalary()%></td>
                        <td><%= employees.get(i).getDepartment()%></td>
                    </tr>
                    <% } %>
                </tbody>
                </table>
        </div>
        <div style="margin: auto; width: 50%; border: 5px solid lightskyblue; padding: 10px; background-color: whitesmoke">
                <form method="post" action="AddEmployeeServlet" >
                    <fieldset>
                    <legend>Add Employee</legend>
                        <p>Name: </p><input type="text" name="name" /><br>
                        <p>Address: </p><input type="text" name="address"/><br>
                        <p>Salary: </p><input type="number" name="salary"/><br>
                        <p>Department: </p><input type="text" name="department"/><br><br>
                        <input type="submit" value="Add employee"/>
                    </fieldset>
                </form>
                <form method="post" action="DeleteEmployee">
                    <fieldset>
                    <legend>Delete Employee</legend>
                        <p>Name: </p><input type="text" name="name" /><br><br>
                        <input type="submit" value="Delete employee"/>
                    </fieldset>
                </form>
                <form method="post" action="ListEmployeesByDepartment">
                    <fieldset>
                    <legend>List Employees By Department</legend>
                        <p>Department name: </p><input type="text" name="name" /><br><br>
                        <input type="submit" value="Employees By Department"/>
                    </fieldset>
                </form>
            </div>
    </body>
</html>
