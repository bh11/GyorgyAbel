<%-- 
    Document   : departments
    Created on : Jan 24, 2020, 9:25:21 AM
    Author     : s26841
--%>

<%@page import="hu.braninghub.company.Department"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div style="margin: auto; min-height: 80px; width: 50%; border: 5px solid lightskyblue; padding: 10px; background-color: whitesmoke">
            <%  List<Department> departments = (List<Department>)request.getAttribute("departments"); %>
            <table border="3" align="center">
                <thead>
                    <tr>
                        <th>Departments</th>
                    </tr>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    <% for (int i = 0; i < departments.size(); i++) { %>
                    <tr>
                        <td><%= departments.get(i).getName()%></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
            <form method="post" action="AddDepartment">
                <fieldset>
                    <legend>Add Department</legend>
                    <p>Name: </p><input type="text" name="name" /><br><br>
                    <input type="submit" value="Add department"/>
                </fieldset>
            </form>
            <form method="post" action="DeleteDepartment">
                <fieldset>
                    <legend>Delete Department</legend>
                    <p>Name: </p><input type="text" name="name" /><br><br>
                    <input type="submit" value="Delete department"/>
                </fieldset>
            </form>
        </div>
    </body>
</html>
