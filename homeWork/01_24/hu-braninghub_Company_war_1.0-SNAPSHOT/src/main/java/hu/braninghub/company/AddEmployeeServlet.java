package hu.braninghub.company;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author george
 */
@WebServlet(name = "AddEmployeeServlet", urlPatterns = {"/AddEmployeeServlet"})

public class AddEmployeeServlet extends HttpServlet {
    
    EmployeeService employeeService = new EmployeeService();
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
            this.employeeService.addEmployee(
                new Employee(
                        request.getParameter("name"),
                        request.getParameter("address"),
                        Integer.parseInt(request.getParameter("salary")),
                        request.getParameter("department")
                )
            );
            request.setAttribute("employees", this.employeeService.getEmployees());
            request.getRequestDispatcher("employees.jsp").forward(request, response);
    }
}
