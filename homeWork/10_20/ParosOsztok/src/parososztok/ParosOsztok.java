/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parososztok;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class ParosOsztok {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Kérj be egy számot, írd ki a páros osztóit!
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Kérlek adj meg egy pozitív egész számot, hogy meg tudjam nézni a páros osztóit: ");
        if (scanner.hasNextInt()) {
            int number = scanner.nextInt();
                if(number > 0){
                    System.out.println(number + " páros ósztói a következők: ");
                    for (int i = 1; i <= number; i++) {
                        if ((number % i) == 0 && (i % 2) == 0) {
                            System.out.println(i);
                        }
                    }
                }else{
                    System.out.println("Kérem csak pozitív egész számot adjon meg!");
                }
        } else {
            System.out.println("Nem számot vagy nem egész számot adtál meg!");
        }
    }
}
