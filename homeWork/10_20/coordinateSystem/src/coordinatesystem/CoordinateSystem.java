/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coordinatesystem;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class CoordinateSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        /*
        Olvass be 2 számot. Határzod meg, hogy a 2 szám által meghatározott koordináta melyik síknegyedben van.
        X≥0 és Y≥0 → Síknegyed=1
        X≥0 és Y<0 → Síknegyed=4
        X<0 és Y≥0 → Síknegyed=2
        X<0 és Y<0 → Síknegyed=3
         */
        Scanner scanner = new Scanner(System.in);

        System.out.println("Kérlek adj meg egy egész számot: ");
        if (scanner.hasNextInt()) {
            int a = scanner.nextInt();
            System.out.println("Kérlek adj meg még egy számot: ");
            int b = scanner.nextInt();
            if (a >= 0 && b >= 0) {
                System.out.println("Első síknegyedben van!");
            } else if (a < 0 && b >= 0) {
                System.out.println("Második síknegyedben van!");
            } else if (a < 0 && b < 0) {
                System.out.println("Harmadik síknegyedben van!");
            } else if (a >= 0 && b < 0) {
                System.out.println("Negyedik síknegyedben van!");
            }
        } else {
            System.out.println("Nem számot vagy egész számot adtál meg!");
        }
    }

}
