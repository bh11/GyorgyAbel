/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package endswith11;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Endswith11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Kérj be 2 számot a felhasználótól. A kisebbtől a nagyobbig írd ki a 11-re végződő számokat!
        
        Scanner scanner = new Scanner (System.in);
        boolean isValidInput = false;
        int number1 = 0;
        // Első szám beolvasása
        do{
            System.out.println("Kérem adja meg az első számot: ");
            if(scanner.hasNextInt()){
                number1 = scanner.nextInt();
                isValidInput = true;
            }else{
                System.out.println("Nem számot adott meg!");
                scanner.next();
            }
        }while(!isValidInput);
        //Második szám beolvasása
        isValidInput = false;
        int number2 = 0;
        do{
            System.out.println("Kérem adja meg a második számot: ");
            if(scanner.hasNextInt()){
                number2 = scanner.nextInt();
                isValidInput = true;
            }else{
                System.out.println("Nem számot adott meg!");
                scanner.next();
            }
        }while(!isValidInput);
        
        int from = Math.min(number1, number2);
        int to = Math.max(number1, number2);
        
        int i = from;
        
        while (i <= to){
            if((i % 100) == 11){
                System.out.println(i);                
            }
            i++;
        }
    }   
}
