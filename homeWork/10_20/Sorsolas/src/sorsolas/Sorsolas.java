/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorsolas;

/**
 *
 * @author s26841
 */
public class Sorsolas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Sorsolj ki egy egész számot az [1000;10000] intervallumból. Írd ki az első és utolsó számjegyét!
        
        int number = (int) (Math.random() * 9000 + 1000);
        int firstDigit = number / 1000;
        int lastDigit = number % 10;
        
        System.out.println("Random Number is: " + number);
        System.out.println("First Digit of random number: " + firstDigit);
        System.out.println("Last Digit of random number: " + lastDigit);
        }
    }

