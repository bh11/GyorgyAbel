/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commondivisor;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class CommonDivisor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //A felhasználótól kérj be 2 egész számot 10 és 500 között. Határozd meg a két pozitív egész szám legnagyobb közös osztóját!
        Scanner scanner = new Scanner(System.in);
        int firstNumber = 0, secondNumber = 0;
        do {
            System.out.println("Kérem adjon meg két pozitív egész számot: ");
            if (scanner.hasNextInt()) {
                firstNumber = scanner.nextInt();
            } else {
                System.out.println("Nem pozitív egész számot adott meg!");
                scanner.next();
            }
            if (scanner.hasNextInt()) {
                secondNumber = scanner.nextInt();
            }
        } while (firstNumber <= 0 || secondNumber <= 0);
        int maradek = firstNumber % secondNumber;
        while (maradek != 0) {
            secondNumber = maradek;
            maradek = firstNumber % secondNumber;
        }
        System.out.println("A legnagyobb közös osztójuk: " + secondNumber);
    }

}
