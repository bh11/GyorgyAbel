/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lessthan10;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class LessThan10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Írj programot, mely addig olvas be számokat a billentyűzetről, ameddig azok kisebbek, mint tíz. Írd ki ezek után a beolvasott számok összegét!
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        boolean isValidInput = false;

        do {
            System.out.println("Kérem adjon meg egy egész számot, ami nem nagyobb mint 10 (ha 10-nél nagyobb számot ad meg a program leáll és összeadja a bevitt számokat): ");
            if(scanner.hasNextInt()){
                int number = scanner.nextInt();
                    if (number <= 10){
                        sum = sum + number;
                    }else{
                        isValidInput = true;
                    }
            }else{
                System.out.println("Nem számot vagy nem egész számot adott meg!");
                scanner.next();
            }
        } while (!isValidInput);
        System.out.println("A számok összege: " + sum);
    }
}
