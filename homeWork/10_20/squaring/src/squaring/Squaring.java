/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package squaring;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class Squaring {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Kérj be egy pozitív egész számot (addig kérjünk be adatot, amíg pozitív számot nem kapunk).
        //Írd ki 0-tól az adott számig a számok négyzetét. Pl.: bemenet: 4 - kimenet: 0 1 4 9 16
        Scanner scanner = new Scanner(System.in);

        boolean isCorrectNumber = false;

        do {
            System.out.println("Kérem adjon meg egy pozitív egész számot: ");
            if (scanner.hasNextInt()) {
                int number = scanner.nextInt();
                if (number > 0) {
                    for (int i = 0; i <= number; i++) {
                        System.out.print(i * i + "  ");
                        isCorrectNumber = true;
                    }
                }
            } else {
                System.out.println("Nem pozitív vagy egész számot adtál meg!");
                scanner.next();
            }
        } while (!isCorrectNumber);
    }
}
