/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evennumbers;

/**
 *
 * @author s26841
 */
public class EvenNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Írd ki a páros számokat 55 és 88 között. Oldd meg ezt a feladatot while és for felhasználásával is.
        
        // while használatával
        int from = 55;
        int to = 88;

        while (from <= to) {
            if (from % 2 == 0) {
                System.out.println("While használatával: " + from);
            }
            from++;
        }
        
        // for hasztnálatával
        
        from = 55;
        
        for (int i = from; i <= to; i++){   
            if(i % 2 == 0){
                System.out.println("For használatával: " + i);  
            }
        }
    }
}
