/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package divisionwith7;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class DivisionWith7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        // Kérj be egy számot a felhasználótól. Ha 7-tel osztható, írd ki, hogy "kismalac". Ezt a feladatot oldd meg if és switch felhasználásával is.
        
        // If használatával   
        
        Scanner scanner = new Scanner(System.in);

        System.out.println("Kérem adjon meg egy egész számot: ");
        if (scanner.hasNextInt()) {
            int number = scanner.nextInt();
            if (number % 7 == 0) {
                System.out.println("kismalac");
            } else {
                System.out.println(number + " nem oszható 7-el!");
            }
        } else {
            System.out.println("Nem számot vagy egész számot adoot meg!");
            scanner.next();
        }

        // Switch használatával    
        
        System.out.println("Kérem adjon meg egy egész számot: ");
        if (scanner.hasNextInt()) {
            int number = scanner.nextInt();
            if (number % 7 == 0) {
                switch (number) {
                    case 0:
                        System.out.println("kismalac");
                        break;
                    default:
                        System.out.println("A szám nem osztható 7-el!");
                }
            }
        } else {
            System.out.println("Nem számot vagy egész adtál meg!");
            scanner.next();
        }
    }
}
