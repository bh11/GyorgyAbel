/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package method;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Method {

    static int countCharacter(String text, char letter, boolean check) {
        Scanner sc = new Scanner(System.in);

        int counter = 0;
        do {
            System.out.println("Kérlek adj meg egy szöveget és egy karaktert amit megkeressek a szövegben enterrel elválasztva: ");
            if (!sc.hasNextInt() && !sc.hasNextInt()) {
                text = sc.nextLine();
                letter = sc.next().charAt(0);
                check = true;
            } else {
                System.out.println("A bevitt adatok nem megfelelőek!");
                sc.next();
            }
        } while (!check);

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == letter) {
                counter++;
            }
        }

        System.out.println("A \"" + letter + "\" karaktert ennyiszer találtam meg a \"" + text + "\" szóban: " + counter);
        return counter;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        boolean check = false;
        String text = "";
        char letter = ' ';
        int counter = 0;

        counter = countCharacter(text, letter, check);
    }

}
