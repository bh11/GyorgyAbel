/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg50elemntsarray;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        int [] array = new int [50];
        int sum = 0;
        int counter = 0;
        int number = 0;
        boolean isValidNumber = false;
        
        for (int i = 0; i < array.length; i++) {
            array[i] = (int)(Math.random() * 10);
        }
        System.out.print("Tömb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        for (int i = 0; i < array.length; i++) {
            
        }
        System.out.println("\nKérem adjon meg egy 0-9 közötti pozitív egész számot:");
        do{
            if (sc.hasNextInt()){
                number = sc.nextInt();
                if(number >= 0 && number <= 10 ){
                    for (int i = 0; i < array.length; i++) {
                        if(array[i] == number){
                            counter++;
                        }
                    }
                    isValidNumber = true;
                }else{
                    System.out.println("Nem 0-9 közötti számot adott meg!");
                }
            }else{
                System.out.println("Nem pozitív egész számot adott meg!");
                sc.next();
            }
        }while(!isValidNumber);
        System.out.println("\nA tömb elemeinek összege: " + sum);
        System.out.println("Számok számtani közepe: " + ((double)sum / 50));
        System.out.println("A keresett szám a " + number + "\n" + counter + " alkalommal tartalmazza a tömb a " + number + " számot");
    }
    
}
