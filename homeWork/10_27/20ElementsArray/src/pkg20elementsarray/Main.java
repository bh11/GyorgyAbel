/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20elementsarray;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int[] array = new int[20];
        int number = 0;
        int number2 = 0;
        int numberIndex = -1;

        System.out.println("Kérem adjon meg 20 pozitív egész számot: ");
        for (int i = 0; i < 5; i++) {
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                if(number > 0){
                    array[i] = number;
                }else{
                    System.out.println("0-nál nagyobb egész számot kell megadni!");
                    sc.next();
                }    
            } else {
                System.out.println("Nem pozitív egész számot adtál meg!");
                sc.next();
                i--;
            }
        }
        System.out.println("Kérlek adj meg egy pozitív egész számot: ");
        if (sc.hasNextInt()) {
            number2 = sc.nextInt();
            for (int i = 0; i < array.length; i++) {
                if (array[i] == number2) {
                    numberIndex = i;
                }  
            }
        } else {
            System.out.println("Nem pozitív egész számot adtál meg!"); 
        }
        if (numberIndex >= 0) {
            System.out.println("A keresett szám: " + number2 + "\n" + "Indexe: " + numberIndex);
        } else {
            System.out.println("A keresett szám a " + number2 + " nem található a tömbben!");
        }
    }

}
