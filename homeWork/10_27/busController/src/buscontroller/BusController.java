/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscontroller;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class BusController {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String format = "";
        int hour = 0, min = hour;
        Scanner sc = new Scanner(System.in);
        int[] fine = new int[24];
        
        do {
            System.out.println("Adja meg az órát, percet és a bírság rendezésének típusát (ha helyszíni akkor 'h' ha csekkes akkor 'c' karaktert adjon meg):");
                hour = sc.nextInt();
                min = sc.nextInt();
                format = sc.next();
                
                if (hour >= 0 && hour <= 24 && (format.equals("h") || format.equals("c"))) {
                    switch (format) {
                        case "h":
                            fine[hour] += 6000;
                            break;
                        case "c":
                            fine[hour] += 4800;
                            break;
                    }
                }else{
                    System.out.println("Nem megfelelő adatot adtál meg!");
                }          
        } while (hour != 0 || min != 0);
        
        for (int i = 0; i < fine.length; i++) {
            if(fine[i] != 0){
                System.out.printf("%d:00 - %d:59 --> %d Ft \n", i, i, fine[i]);
            }
        }
    }

}
