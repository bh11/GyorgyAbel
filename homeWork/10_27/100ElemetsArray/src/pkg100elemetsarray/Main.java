/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg100elemetsarray;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int[] numbers = new int[100];
        int number = 1;

        System.out.println("Adjon meg pozitív egész számokat enterrel elválasztva: ");
        for (int i = 0; number != 0; i++) {
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                numbers[i] = number;
            } else {
                System.out.println("Nem egész számot adtál meg!");
                sc.next();
                i--;
            }
        }

        System.out.print("A tömb elemei: ");

        for (int i = 0; numbers[i] != 0; i++) {
            System.out.print(numbers[i] + " ");
        }

        System.out.println("");

        int min = numbers[0];
        int minIndex = 0;

        for (int i = 0; numbers[i] != 0; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
                minIndex = i;
            }
        }

        System.out.println("A tömb lekisebb értéke: " + min + "\n" + "Index értéke: " + minIndex);

        int max = 0;
        int maxIndex = 0;

        for (int i = 0; numbers[i] != 0; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
                maxIndex = i;
            }
        }

        System.out.println("A tömb legnagyobb értéke: " + max + "\n" + "Index értéke: " + maxIndex);

        int[] reverse = new int[100];
        int lastIndex = 0;

        for (int i = 0; numbers[i] != 0; i++) {
            lastIndex++;
        }

        System.out.print("A tömb elemei visszafelé: ");

        for (int i = 0; lastIndex != 0; i++) {
            reverse[i] = numbers[lastIndex - 1];
            lastIndex--;
            System.out.print(reverse[i] + " ");
        }
    }
}
