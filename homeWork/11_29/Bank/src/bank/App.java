/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import Person.Person;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Person person1 = new Person("Gyula", 16);
        Person person2 = new Person("Jani", 33);
        Person person3 = new Person("Babi néni", 75);
        Person person4 = new Person("Anna", 22);

        List <Person> persons = new ArrayList <>();
        
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        
    }
    
}
