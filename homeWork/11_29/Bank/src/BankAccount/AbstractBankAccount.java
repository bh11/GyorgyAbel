/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankAccount;

import Person.Person;

/**
 *
 * @author george
 */
public abstract class AbstractBankAccount extends Person{
    
    Person person = new Person();
    
    private final int accountNumber;
    private String owner;
    private int cashWithdrawalCost;
    private final int fromAccountNumber = 0;
    private final int toAccountNumber = 10000000;
    
    static final int studentAccountLimit = 18;
    static final int ACCOUNT_OPENING_COST = 500;
    
    protected double cashWithDrawalNormalCost = cashWithdrawalCost * (double)0.10;
    static final int CASH_WTIHDRAWAL_MAX_COST = 1000;

    public AbstractBankAccount() {
        this.accountNumber = randomAccountNumberGenerator();
        this.owner = person.getName();
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public String getOwner() {
        return owner;
    }
    
    public boolean chaeckAgeAtOpenAccount(){
        return person.getAge() > studentAccountLimit;
    }
    
    private int randomAccountNumberGenerator(){
        return (int) (Math.random() * (toAccountNumber - fromAccountNumber) + fromAccountNumber);
    }
    
}
