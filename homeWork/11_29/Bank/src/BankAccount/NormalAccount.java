/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankAccount;

/**
 *
 * @author george
 */
public class NormalAccount extends AbstractBankAccount{
    
    private final int normalMounthlyCostofBankAccount = 1_500;
    private final int cheaperMounthlyCostofBankAccount = 500;
    private final int freeMounthlyCostSalaryLimit = 250_000;
    private int salary;
    private int transferMoney;
    
    private double transferCost = transferMoney * (double)0.05; 

    public NormalAccount(){
        super();
    }
    
    
    
    public boolean checkMounthlyCost(){
        return salary > freeMounthlyCostSalaryLimit;
    }
}
