/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.mysql.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class HrDao {
    
    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PW = "verbatim";
    
//    public HrDao(){
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    public String findEmployeeWithMaxSalary(){
        String sql = "select first_name, last_name, salary from hr.employees \n" +
                     "where salary = (select max(salary) from hr.employees);";
        
        String result = "";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PW);
            Statement stm = connection.createStatement()){
            
            ResultSet rs = stm.executeQuery(sql);
            
            while(rs.next()){
                result += rs.getString("first_name") + " " + rs.getString("last_name");
            }
        }catch(SQLException ex){
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String mostEmployeeInDepartment(){
        String sql = "SELECT B.department_name, count(*) nr\n" +
                     "FROM hr.employees A INNER JOIN hr.departments B\n" +
                     "ON A.department_id = B.department_id\n" +
                     "GROUP BY B.department_name\n" +
                     "ORDER BY nr DESC\n" +
                     "LIMIT 1;";
        
        String result = "";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PW);
            Statement stm = connection.createStatement()){
            
            ResultSet rs = stm.executeQuery(sql);
            
            while(rs.next()){
                result += (rs.getString("department_name"));
            }
        }catch(SQLException ex){
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String employeeWithMoreThanAvgSalary(){
        String sql = "select first_name, last_name from hr.employees"
                   + " where salary > (select avg(salary) from hr.employees);";
        
        String result = "";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PW);
            Statement stm = connection.createStatement()){
            
            ResultSet rs = stm.executeQuery(sql);
            
            while(rs.next()){
                result += (rs.getString("first_name") + " " + rs.getString("last_name")) + "\n";
            }
        }catch(SQLException ex){
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String employeeHireDateAfter19900101(){
        String sql = "select first_name, last_name, hire_date "
                   + "from hr.employees where hire_date > 19900101;";
        String result = "";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PW);
            Statement stm = connection.createStatement()){
            
            ResultSet rs = stm.executeQuery(sql);
            
            while(rs.next()){
                result += (rs.getString("first_name") 
                + " " + rs.getString("last_name") 
                + " " + rs.getString("hire_date")) + "\n";
            }
        }catch(SQLException ex){
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String jobTitleContainClerkWithOrder(){
        String sql = "select distinct job_title from hr.jobs "
                   + "where job_title like \"%clerk%\" order by job_title asc;";
        
        String result = "";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PW);
            Statement stm = connection.createStatement()){
            
            ResultSet rs = stm.executeQuery(sql);
            
            while(rs.next()){
                result += (rs.getString("job_title")) + "\n";
            }
        }catch(SQLException ex){
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String findEmployeeByIdUnsafe(String id) {
        String sql =    "SELECT * \n" +
                        "FROM employees e \n" +
                        "WHERE e.employee_id = '" + id + "'";
        
        String result = "";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
        
        ResultSet rs = stm.executeQuery(sql);
        
        while (rs.next()) {
            result += (rs.getString("first_name") + " " + rs.getString("last_name")) + "\n";
        }
            
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String findEmployeeByFirstName(String name) {
        String sql = "SELECT * \n"
                    + "FROM employees e \n"
                    + "WHERE e.first_name = ?";
        
        String result = "";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
             PreparedStatement stm = connection.prepareStatement(sql)) {
            
            stm.setString(1, name);
        
        ResultSet rs = stm.executeQuery();
        
        while (rs.next()) {
            result += (rs.getString("first_name") + " " + rs.getString("last_name")) + "\n";
        }
            
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
