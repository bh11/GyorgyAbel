/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author s26841
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Animal[] animals = new Animal[10];
        Animal an1 = new Animal("Pityu", 8, 4);
        Cat an2 = new Cat("Karesz", 3, 3);
        Dog an3 = new Dog("Morzsi", 2, 4, "barna");
        
        Animal an4 = new Bird("Pintyő", 5, 4);
        
        animals [0] = an1;
        animals [1] = an2;
        animals [2] = an3;
        animals [3] = an4;
        
        printAnimals(animals);
    }
    
    public static void printAnimals(Animal [] animals){
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] != null) {
                System.out.println(animals[i]);
            }
        }
    }
}
