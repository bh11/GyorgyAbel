/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author s26841
 */
public class Animal {
    String name;
    int age;
    int countOfLegs;
    

    
    public Animal(String name, int age, int countOfLegs){
        this.name = name;
        this.age = age;
        this.countOfLegs = countOfLegs;
    }
    
    public void makeSound(){}

    @Override
    public String toString(){
        return "Base animal has the following characteristics: " + "name = " + name + ", age = " + age + ", count of legs = " + countOfLegs;
    }
}
