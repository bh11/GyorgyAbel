/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author s26841
 */
public class Bird extends Animal{
    
    private boolean vaccination;

    public Bird(String name, int age, int countOfLegs) {
        super(name, age, countOfLegs);
        vaccination = true;
    }
    
    @Override
    public void makeSound(){
        System.out.println("goat-voice");
    }
    
    @Override
    public String toString(){
        return "Goat has the following characteristics: " + "name = " + name + ", age = " + age + ", count of legs = " + countOfLegs + ", vaccination = " + vaccination;
    }
}
