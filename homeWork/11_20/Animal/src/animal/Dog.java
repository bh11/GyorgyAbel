/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author s26841
 */
public class Dog extends Animal{
    
    private String color;

    public Dog(String name, int age, int countOfLegs, String color) {
        super(name, age, countOfLegs);
        this.color = color;
    }

    @Override
    public void makeSound(){
        System.out.println("dog-voice");
    }
    
    @Override
    public String toString(){
        return "Dog has the following characteristics: " + "name = " + name + ", age = " + age + ", count of legs = " + countOfLegs + ", color = " + color;
    }
}
