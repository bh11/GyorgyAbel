/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author s26841
 */
public class Cat extends Animal{

    private boolean domestic;
    
    public Cat(String name, int age, int countOfLegs) {
        super(name, age, countOfLegs);
        domestic = false;
    }
    
    public boolean isDomestic(){
        return domestic;
    }
    
    @Override
    public void makeSound(){
        System.out.println("cat-voice");
    }
    
    @Override
    public String toString(){
        return "Cat has the following characteristics: " + "name = " + name + ", age = " + age + ", count of legs = " + countOfLegs + ", domestic = " + domestic;
    }
}
