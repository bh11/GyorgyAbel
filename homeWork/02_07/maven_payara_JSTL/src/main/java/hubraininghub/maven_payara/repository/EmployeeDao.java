/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.maven_payara.repository;

import hubraininghub.maven_payara.repository.dto.Employee;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.sql.DataSource;

/**
 *
 * @author george
 */
@Singleton
public class EmployeeDao {

    @Resource(lookup = "resource1")
    private DataSource ds;

    @PostConstruct
    public void init() {
        System.out.println("It has been inicialized!");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("It has been destroyed!");
    }

    public List<Employee> getEmployees() {

        List<Employee> names = new ArrayList<>();

        try (Statement stm = ds.getConnection().createStatement()) {

            String sql = "select first_name, last_name, salary from employees limit 10";

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                names.add(Employee.of(
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getInt("salary")
                ));
            }

        } catch (Exception e) {
        }

        return names;
    }

    public List<Employee> findByName(String str) throws SQLException {

        List<Employee> filterNames = new ArrayList<>();

        String sql = "select first_name, last_name, salary from employees where first_name like ?";

        try (PreparedStatement stm = ds.getConnection().prepareStatement(sql)) {

            stm.setString(1, "%" + str + "%");

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                filterNames.add(Employee.of(
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getInt("salary")
                ));
            }

        } catch (Exception e) {
        }
        return filterNames;
    }
}
