/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.maven_payara.service;

import hubraininghub.maven_payara.repository.EmployeeDao;
import hubraininghub.maven_payara.repository.dto.Employee;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author george
 */

@Stateless
public class EmployeeService {

    @Inject
    private EmployeeDao dao;

    public List<String> getNames() {

        try {

            return dao.getEmployees().stream()
                      .map(Employee::getFirstName)
                      .collect(Collectors.toList());

        } catch (Exception ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    public List<Employee> getEmployees(String str) {

        try {
            
            return dao.findByName(str);
            
        } catch (Exception ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
}
