<%-- 
    Document   : empnames
    Created on : 31-Jan-2020, 20:52:17
    Author     : george
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>First table</p>
        <table border="3">
            <c:forEach var="name" items="${names}">
                <tr><td><c:out value="${name}"/></td></tr>
            </c:forEach>
        </table>
        <br>
        <p>Second table</p>
        <table border="3">
            <c:forEach var="name" items="${names}">
                <tr><td><c:out value="${name}"/>(<c:out value="${name.length()}" />)</td></tr>
            </c:forEach>
        </table>
        <br>
        <form method="post" action="EmployeeServlet">
            <fieldset> Employee By Part Of Name
                <input type="text" name="partOfName">
                <input type="submit" value="Filter By Part Of Name">
            </fieldset>
        </form>
    </body>
</html>
