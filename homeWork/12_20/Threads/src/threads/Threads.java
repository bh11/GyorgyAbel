/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;


/**
 *
 * @author george
 */
public class Threads {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        
        WriteName t1 = new WriteName("t1 Ábel György", "GABEL");
        WriteName t2 = new WriteName("t2 Ábel György", "GABEL");
        WriteName t3 = new WriteName("t3 Ábel György", "GABEL");
        WriteName t4 = new WriteName("t4 Ábel György", "GABEL");
        WriteName t5 = new WriteName("t5 Ábel György", "GABEL");
        
        t3.start();
        t1.join();
        t2.start();
        t2.join(1000);
        t3.start();
        t3.join();
        t4.start();
        t5.start();
    }
}
