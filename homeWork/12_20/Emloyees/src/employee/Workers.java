/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.io.Serializable;

/**
 *
 * @author george
 */
public class Workers extends AbstractEmployee implements Serializable{
    private Leaders leader;

    public Workers() {}
    
    public Workers(String name) {
        super(name);
    }

    public Leaders getLeader() {
        return leader;
    }

    public void setLeader(Leaders leader) {
        this.leader = leader;
    }

    @Override
    public String toString() {
        return "Workers{" + "leader=" + leader + '}';
    }
}
