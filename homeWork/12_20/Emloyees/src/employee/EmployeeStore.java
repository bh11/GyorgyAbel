/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import EmloyeeFactory.EmployeeFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import saver.Saver;

/**
 *
 * @author george
 */
public class EmployeeStore implements Serializable{
    private List<AbstractEmployee> employees = new ArrayList<>();
    private EmployeeFactory emp = new EmployeeFactory();
    
    public List deserializableEmployeeList(){
        return Saver.deSerializeEmployees();
    }

    public List<AbstractEmployee> getEmployees() {
        return employees;
    }
    
    public void createLeader(String leaderName){
        employees = deserializableEmployeeList();
        AbstractEmployee leader = emp.createEmloyee("leader");
        employees.add(leader);
        leader.setName(leaderName);
        serializableEmployeeList();
    }
    
    public void createWorker(String workerName){
        employees = deserializableEmployeeList();
        AbstractEmployee worker = emp.createEmloyee("worker");
        employees.add(worker);
        worker.setName(workerName);
        serializableEmployeeList();
    }
    
    public void serializableEmployeeList(){
        Saver.serializeEmployees(employees);
    }
    
    public String printEmployees(){
        return employees.toString();
    }
}
