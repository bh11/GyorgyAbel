/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author george
 */
public class Leaders extends AbstractEmployee implements Serializable{ 
    private String name;
    private List <Workers> workers = new ArrayList<>();
    private String subordinates = "";

    public Leaders() {}
    
    public Leaders(String name) {
        super(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Workers> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Workers> workers) {
        this.workers = workers;
    }

    @Override
    public String toString() {
        workers.stream()
               .forEach(p -> {
                   subordinates += p.getName() + " ,";
               });
        return "Leaders{" + "name=" + name + ", workers=" + subordinates + '}';
    }
}
