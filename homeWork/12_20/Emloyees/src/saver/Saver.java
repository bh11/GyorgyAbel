/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author george
 */
public class Saver implements Serializable{
    
        private static final String FILENAME_FOR_LIST_OF_EMPLOYEES = "employees.ser";

        public static void serializeEmployees(List emp){
        try (FileOutputStream fs = new FileOutputStream(FILENAME_FOR_LIST_OF_EMPLOYEES);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(emp);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static List deSerializeEmployees() {
        List emp = null;
        try (FileInputStream fs = new FileInputStream(FILENAME_FOR_LIST_OF_EMPLOYEES);
                ObjectInputStream oi = new ObjectInputStream(fs)) {
            emp = (ArrayList) oi.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emp;
    }
}
