/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.util.Scanner;

/**
 *
 * @author george
 */
public class ConsoleView implements View {

    private static final String STOP = "exit";

    private Controller controller;

    private void startConsoleReader() {
        try (Scanner scanner = new Scanner(System.in)) {
            String inputTypeEmployee;

            do {
                System.out.println("Input type of Emloyees (L -> Leader | W -> Worker | LI -> List Employees | S -> save | exit -> stop program): ");
                inputTypeEmployee = scanner.nextLine();

                if (STOP.equalsIgnoreCase(inputTypeEmployee)) {
                    break;
                }

                if ("L".equalsIgnoreCase(inputTypeEmployee)) {
                    System.out.println("Leader name is: ");
                    String inputLeaderName;
                    inputLeaderName = scanner.nextLine();
                    controller.setLeaderName(inputLeaderName);
                }
                
                if ("W".equalsIgnoreCase(inputTypeEmployee)) {
                    System.out.println("Worker name is: ");
                    String inputWorkerName;
                    inputWorkerName = scanner.nextLine();
                    controller.setWorkerName(inputWorkerName);
                }
                
                if ("LI".equalsIgnoreCase(inputTypeEmployee)) {
                    controller.notifyView();
                }
                
                if ("S".equalsIgnoreCase(inputTypeEmployee)) {
                    controller.notifySerializableRequest();
                }

            } while (true);
        }
    }

    @Override
    public void setController(Controller c) {
        controller = c;
    }
    
    @Override
    public void setText(String s) {
        
    }
    
    @Override
    public void start() {
        startConsoleReader();
    }
}
