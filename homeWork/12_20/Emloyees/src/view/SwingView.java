/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author george
 */
public class SwingView extends JFrame implements View{
    
    private Controller controller;

    private final JTextField employee = new JTextField(30);
    private final JTextField showEmployee = new JTextField(50);
    private final JButton leader = new JButton("L");
    private final JButton worker = new JButton("W");
    private final JButton serializable = new JButton("S");
    private final JButton list = new JButton("LI");
    
    public void buildWindow(){
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
        add(buildSouth(), BorderLayout.SOUTH);
        
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private JPanel buildNorth(){
        employee.setEditable(true);
        JPanel jp = new JPanel();
        
        jp.add(employee);
        jp.add(leader);
        jp.add(worker);
        jp.add(serializable);

        leader.addActionListener(p -> {
            controller.setLeaderName(employee.getText());
        });
        worker.addActionListener(p -> {
            controller.setWorkerName(employee.getText());
        });
        serializable.addActionListener(p -> {
            controller.notifySerializableRequest();
        });
        
        return jp;
    }
    
    private JPanel buildCenter(){
        JPanel jp = new JPanel();

        list.addActionListener(p -> {
            controller.notifyView();
        });
        
        jp.add(list);
        return jp;
    }
    
    private JPanel buildSouth(){
        showEmployee.setEditable(false);
        JPanel jp = new JPanel();
        
        jp.add(showEmployee);
        return jp;
    }
    
    @Override
    public void setController(Controller c) {
        controller = c;
    }

    @Override
    public void setText(String s) {
        showEmployee.setText(s);
    }    
    
    @Override
    public void start() {
        buildWindow();
    }
}
