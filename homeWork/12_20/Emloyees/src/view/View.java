/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;

/**
 *
 * @author george
 */
public interface View {
    
    void setController(Controller c);
    
    public void setText(String s);
    
    void start();
}
