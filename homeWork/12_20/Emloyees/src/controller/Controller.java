/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Model;
import view.View;

/**
 *
 * @author george
 */
public class Controller {
    
    private View v;
    private Model m;

    public Controller(View v, Model m) {
        this.v = v;
        this.m = m;

        v.setController(this);
        m.setController(this);
    }
    
    public void setLeaderName(String leaderName){
        if (leaderName.length() > 0) {
            m.setLeaderName(leaderName);
        }
    }
    
    public void setWorkerName(String workerName){
        if (workerName.length() > 0) {
            m.setWorkerName(workerName);
        }
    }
    
    public void notifySerializableRequest(){
        m.serializable();
    }

    public void notifyView(){
        v.setText (m.getEmployees());
    }
}
