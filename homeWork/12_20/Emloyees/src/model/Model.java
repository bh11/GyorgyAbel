/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import employee.EmployeeStore;

/**
 *
 * @author george
 */
public class Model {
    
    private Controller controller;
    private EmployeeStore emp = new EmployeeStore();
    
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void setLeaderName(String leaderName) {
        emp.createLeader(leaderName);
    }
    
    public void setWorkerName(String workerName){
        emp.createWorker(workerName);
    }
    
    public void serializable() {
        emp.serializableEmployeeList();
    }
    
    public String getEmployees(){
        return emp.printEmployees();
    }
    
    
}
