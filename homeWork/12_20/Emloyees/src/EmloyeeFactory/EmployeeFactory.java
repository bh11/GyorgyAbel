/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmloyeeFactory;

import employee.AbstractEmployee;
import employee.Leaders;
import employee.Workers;

/**
 *
 * @author george
 */
public class EmployeeFactory{
    
    public AbstractEmployee createEmloyee(String type){
        if ("worker".equalsIgnoreCase(type)) {
            return new Workers();
        }
        if ("leader".equalsIgnoreCase(type)) {
            return new Leaders();
        }
        return null;
    }
}
