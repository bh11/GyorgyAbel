/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controller.Controller;
import controller.Controller;
import model.Model;
import view.ConsoleView;
import view.SwingView;
import view.View;

/**
 *
 * @author george
 */
public class App {

//    1. Írj egy MVC programot, amelyben egy cég dolgozóit tudjuk nyilvántartani.
//       Típust tekintve 2 dolgozó létezik: munkás és főnök. 
//       Minden munkásnak van egy főnöke és egy főnöknek lehet több munkása. 
//       A főnöknek nincs főnöke. Az alkalmazásnak két View-ja legyen: 
//       egy konzolos és egy grafikus (Swing).  
//       A controllertől és a modelltől legyen független, 
//       hogy melyik view-val indítják el a programot. 
//       Az alkalmazásban lehetséges hozzáadni és listázni a dolgozókat mindkettő View esetén. 
//       A program perzisztensen legyen képes az aktuális 
//       állapotát tárolni szerializáció megvalósításával. 
//       A megvalósításhoz használj legalább egy tervezési mintát. 
//       (15 pont)
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Model m = new Model();
        View v = new SwingView();
        Controller c = new Controller(v, m);
        
        v.start();
    }
    
}
