/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mostrepeatingnumber;

/**
 *
 * @author s26841
 */
public class MostRepeatingNumber {

    //leggyakoribb szám: 0-100 közötti véletlen számokkal töltsünk fel egy 300 elemű tömböt, majd keressük meg h melyik szám került bele a leggyakrabban
    
    static int[] NUMBERS = new int[300];
    static int[] COUNTER = new int[1050];

    static int[] randomNumbers(int a, int b) {
        System.out.print("Elements of array: ");
        for (int i = 0; i < NUMBERS.length; i++) {
            int randomNumbers = (int) (Math.random() * (b - a) + a);
            NUMBERS[i] = randomNumbers;
            System.out.print(NUMBERS[i] + " ");
        }
        System.out.println();
        return NUMBERS;
    }

    static int[] mostRepeatingNumber() {
        for (int i = 0; i < NUMBERS.length; i++) {
            NUMBERS[i] = COUNTER[NUMBERS[i]]++;
        }
        return COUNTER;
    }

    static int printMaxValue() {
        mostRepeatingNumber();
        int maxValue = COUNTER[0];
        for (int i = 1; i < COUNTER.length; i++) {
            if (COUNTER[i] > maxValue) {
                maxValue = i;
            }
        }
        return maxValue;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int a = 1000;
        int b = 1050;

        randomNumbers(a, b);
        
        System.out.println("Most repeating number is: " + printMaxValue());
    }
}
