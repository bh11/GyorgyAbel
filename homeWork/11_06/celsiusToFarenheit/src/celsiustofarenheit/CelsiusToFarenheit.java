/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celsiustofarenheit;

/**
 *
 * @author s26841
 */
public class CelsiusToFarenheit {
    //Celsius -> Fahrenheit átváltó függvény
    static float celsiusToFarenheit(float num){
        float far = 0;
        float dif = 1.8f;
        far = (num * dif) + 32;
        return far;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        float cel = 11;
        System.out.println(celsiusToFarenheit(cel));
    }
    
}
