/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twodimensionsarrayminvalue;

/**
 *
 * @author s26841
 */
public class TwoDimensionsArrayMinValue {

    static int[][] array = new int[10][15];
    static int[] min = new int[3];
    static int number1 = 100;
    static int number2 = 200;

    static int[][] randomNumbers(int a, int b, int array[][]) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                int randomNumbers = (int) (Math.random() * (b - a) + a);
                array[i][j] = randomNumbers;
            }
        }
        return array;
    }

    static int[] minNumber() {
        randomNumbers(number1, number2, array);
        int minNumber = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length - 1; j++) {
                if (array[i][j] < minNumber) {
                    minNumber = array[i][j];
                    min[0] = i;
                    min[1] = j;
                }
            }
        }
        min[2] = minNumber;
        return min;
    }

    static void printNumber() {
        minNumber();
        System.out.printf("The number line position: %d\n", min[0]);
        System.out.printf("The number column position: %d\n", min[1]);
        System.out.printf("The number is: %d\n", min[2]);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        printNumber();
    }

}
