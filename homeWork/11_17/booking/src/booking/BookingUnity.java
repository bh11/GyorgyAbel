/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking;

/**
 *
 * @author s26841
 */
public class BookingUnity {
    private String id;
    private String country;
    private Bill [] bill;
    private int maxNumberOfBill = 10;

    public BookingUnity(String id, String country){
        bill = new Bill [maxNumberOfBill];
        setId(id);
        setCountry(country);
    }
    
    public void addBill(Bill invoice){
        bill[bill.length - 1] = invoice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Bill[] getBill() {
        return bill;
    }

    public void setBill(Bill[] bill) {
        this.bill = bill;
    }

    public int getMaxNumberOfBill() {
        return maxNumberOfBill;
    }

    public void setMaxNumberOfBill(int maxNumberOfBill) {
        this.maxNumberOfBill = maxNumberOfBill;
    }
    
    public void printBill(){
        for (Bill inv : bill) {
            System.out.println(inv.getId() + " " + inv.getAmount());
        }
    }
}
