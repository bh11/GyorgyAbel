/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;


/**
 *
 * @author tamas
 */
enum InfinityStone {
    SOUL("orange", 2), TIME("green", 3), SPACE("blue", 4), MIND("yellow", 5),
    REALITY("red", 6), POWER("purple", 7);
    
    private final String color;
    private final int power;
    
    private InfinityStone(String color, int power) {
        this.color = color;
        this.power = power;
        
    }
    
    public String getColor() {
        return color;
    }
    
    public int getPower() {
        return power;
    }
    
    public static InfinityStone getByPower(int power) {
        for (InfinityStone s : values()) {
            if (s.getPower() == power) {
                return s;
            }
        }
        
        return null;
    }
    
}
