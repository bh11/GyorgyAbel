/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import utilities.AvengersUtilities;

/**
 *
 * @author tamas
 */
public class HeroFactory {

    enum Origin {
        EARTH,
        ALIEN
    }

    /**
     *
     * @param parameters 0: name, 1: power, 2: istone, 3: origin
     * @return
     */
    public static Hero create(String[] parameters) {
        switch (Origin.valueOf(parameters[3].toUpperCase())) {
            case EARTH:
                return new EarthHero(parameters[0],
                        Integer.valueOf(parameters[1]),
                        Integer.valueOf(parameters[2]),
                        new PassPort(AvengersUtilities.generateSerial()));
            case ALIEN:
                return new AlienHero(parameters[0],
                        Integer.valueOf(parameters[1]),
                        Integer.valueOf(parameters[2]));
            default:
                return null;
        }
    }

}
