/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

/**
 *
 * @author tamas
 */
public class EarthHero extends Hero implements Flying {

    private PassPort passPort;

    public EarthHero(String name, int power, int infinityStone, PassPort passPort) {
        super(name, power, infinityStone);
        this.passPort = passPort;
    }

    @Override
    public void fly() {
        System.out.println("Flying...");
    }

    public PassPort getPassPort() {
        return passPort;
    }

    public void setPassPort(PassPort passPort) {
        this.passPort = passPort;
    }

}
