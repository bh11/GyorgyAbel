/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

/**
 *
 * @author tamas
 */


public abstract class Hero {
    
    public static final int MIN_VALUE = 2;
    public static final int MAX_VALUE = 10;
    
    private final String name;
    private final int power;
    private InfinityStone infinityStone;
    
    public String getName() {
        return name;
    }
    
    public int getPower() {
        return power;
    }
    
    public int getInfinityStone() {
        return infinityStone.getPower();
    }
    
    public void setInfinityStone(int infinityStone) {
        this.infinityStone = InfinityStone.getByPower(infinityStone);
    }
    
    public Hero(String name, int power, int infinityStone) {
        this.name = name;
        if (power >= MIN_VALUE || power <= MAX_VALUE) {
            this.power = power;
        } else {
            this.power = MIN_VALUE;
        }
        this.infinityStone = InfinityStone.getByPower(infinityStone);
    }

    @Override
    public String toString() {
        return "Hero{" + "name=" + name + ", power=" + power + ", infinityStone=" + infinityStone + '}';
    }
    
    
    
}
