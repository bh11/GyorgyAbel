/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

/**
 *
 * @author tamas
 */
public class AlienHero extends Hero implements Swimming, FireUsing {

    public AlienHero(String name, int power, int infinityStone) {
        super(name, power, infinityStone);
    }

    @Override
    public void swim() {
        System.out.println("Swimming...");
    }

    @Override
    public void useFire() {
        System.out.println("Using fire...");
    }
}
