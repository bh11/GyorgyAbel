/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import hero.HeroFactory;
import store.Fleet;

/**
 *
 * @author Kenwulf
 */
public class HeroParser {

    private final String[][] data;

    public HeroParser(String input) {
        String[] inputLines = input.split(";");
        data = new String[inputLines.length][];

        for (int i = 0; i < inputLines.length; i++) {
            data[i] = inputLines[i].split(" ");
        }
    }

    public Fleet parseToFleet() {
        Fleet fleet = new Fleet();
        for (String[] heroParam : data) {
            fleet.add(HeroFactory.create(heroParam));
        }

        return fleet;
    }

    public String[][] getData() {
        return data;
    }

}
