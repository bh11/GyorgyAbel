/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author tamas
 */
public class ConsoleReader {

    public static String read() throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String input;

        while ((input = br.readLine()) != null && "end".compareTo(input) != 0) {
            if (input.matches("([a-zA-Z]+)\\s[0-9]\\s[0-9]\\s(EARTH|ALIEN|earth|alien)")) {
                sb.append(input).append(";");
            } else {
                System.out.println("Invalid Entry");
            }

        }

        return sb.toString();

    }

    public static String read(File file) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(file));

        String input;

        while ((input = br.readLine()) != null) {
            if (input.matches("([a-zA-Z]+)\\s[0-9]\\s[0-9]\\s(EARTH|ALIEN|earth|alien)")) {
                sb.append(input).append(";");
            }
        }
        return sb.toString();
    }

}
