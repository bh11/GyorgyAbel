/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import hero.EarthHero;
import store.Fleet;

/**
 *
 * @author tamas
 */
public class Printer {

    public void print(Fleet fleet) {
        fleet.getShips().forEach((s) -> {
            s.getHeroes().forEach((h) -> {
                System.out.println(h.toString());
            });
        });
    }

    public void numOfEarthHerosInFleet(Fleet f) {
        System.out.println("Number of Earth Heroes: "
                + f.getShips()
                        .stream()
                        .flatMap(s -> s.getHeroes().stream())
                        .filter(h -> h instanceof EarthHero)
                        .count());
    }

    public void findMaxPowered(Fleet f) {
        System.out.println("Hero with max power: "
                + f.getShips()
                        .stream()
                        .flatMap(s -> s.getHeroes().stream())
                        .max((h1, h2) -> h1.getPower() - h2.getPower())
                        .get()
                        .getName()
        );
    }

    public void listPassports(Fleet f) {
        f.getShips()
                .stream()
                .flatMap(s->s.getHeroes().stream())
                .filter(h->h instanceof EarthHero)
                .forEach(h -> System.out.println(((EarthHero)h).getPassPort().getSerial()));
    }

}
