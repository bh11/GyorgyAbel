/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author Kenwulf
 */
public class AvengersUtilities {

    public static String generateSerial() {
        StringBuilder sb = new StringBuilder();
        sb.append(generateString(2, "ABC")).append("-").append(generateString(4, "123"));
        sb.append("-").append(generateString(4, "123"));

        return sb.toString();
    }

    private static String generateString(int length, String mode) {
        StringBuilder sb = new StringBuilder();
        int from;
        int to;

        if ("ABC".compareTo(mode) == 0) {
            from = 'A';
            to = 'Z';
        } else if ("123".compareTo(mode) == 0) {
            from = '0';
            to = '9';
        } else {
            from = 0;
            to = 126;
        }

        for (int i = 0; i < length; i++) {
            sb.append((char) generateRandomInt(from, to));
        }

        return sb.toString();
    }

    private static int generateRandomInt(int from, int to) {
        return (int) (Math.random() * (to + 1 - from) + from);
    }

}
