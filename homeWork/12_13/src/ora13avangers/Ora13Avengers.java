/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ora13avangers;

import java.io.File;
import java.io.IOException;
import store.Fleet;
import utilities.*;

/**
 *
 * @author tamas
 */
public class Ora13Avengers {

    public static void main(String[] args) {
        Fleet f = null;
        File file = new File("input.txt");
        try {
            f = new HeroParser(ConsoleReader.read(file)).parseToFleet();
        } catch (IOException e) {
            System.err.println("Could not read");
        }

        Printer printer = new Printer();
        printer.print(f);
        printer.numOfEarthHerosInFleet(f);
        printer.findMaxPowered(f);
        printer.listPassports(f);

    }
}
