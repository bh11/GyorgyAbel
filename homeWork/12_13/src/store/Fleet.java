/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.Hero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author tamas
 */
public class Fleet {
    
    private final List<Ship> ships = new ArrayList<>();
    
    public void add(Hero h) {
        findShip().add(h);
    }
    
    private Ship findShip() {
        if (ships.isEmpty()) {
            ships.add(new Ship());
            return ships.get(0);
        }
        
        if (ships.get(ships.size() - 1).getHeroes().size() > Ship.MAX_SIZE) {
            ships.add(new Ship());
        }
        
        return ships.get(ships.size() - 1);
        
    }
    
    public List<Ship> getShips() {
        return Collections.unmodifiableList(ships);
    }
    
}
