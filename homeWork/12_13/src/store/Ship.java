/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.Hero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author tamas
 */
public class Ship {

    public static final int MAX_SIZE = 4;

    private final List<Hero> heroes = new ArrayList<>();

    public void add(Hero h) {
        if (heroes.size() <= MAX_SIZE) {
            heroes.add(h);
            orderBySecondCharacter();
        }
    }

    public List<Hero> getHeroes() {
        return Collections.unmodifiableList(heroes);
    }

    private void orderBySecondCharacter() {
        Collections.sort(heroes, (h1, h2) -> {
            return h1.getName().charAt(1) - h2.getName().charAt(1);
        });
    }

}
