/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hubraininghub.junitexample;

import hubraninghub.junitexample.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author george
 */
public class CalculatorTest {

    @Test
    void testAdd(){
        //Given
        //Mindig azokat az adatokat tartmazza, ami a teszthez kell
        int operndus1 = 10;
        int operandus2 = 15;
        Calculator underTest = new Calculator(operndus1, operandus2);
        
        //When
        //Metódus hivás, itt történik valami
        int result = underTest.add();
        
        //Then
        //Azt kapjuk-e amit akartunk
        Assertions.assertEquals(operndus1 + operandus2, result, () -> "Wrong addition");
    }
    
    @Test
    void testsubstract(){
        int operndus1 = 10;
        int operandus2 = 15;
        Calculator underTest = new Calculator(operndus1, operandus2);
        
        int result = underTest.substract();
        
        Assertions.assertEquals(operndus1 - operandus2, result, () -> "Wrong substract");
    }
    
    @Test
    void testmultiply(){
        int operndus1 = 10;
        int operandus2 = 15;
        Calculator underTest = new Calculator(operndus1, operandus2);
        
        int result = underTest.multiply();
        
        Assertions.assertEquals(operndus1 * operandus2, result, () -> "Wrong multiply");
    }
}
