/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Game {

    static Scanner SC = new Scanner(System.in);

    static final int MIN_NUMBER = 10;
    static final int MAX_NUMBER = 15;

    static final int MIN_BOMB_NUMBER = 8;
    static final int MAX_BOMB_NUMBER = 20;

    static final char BAD_BOMB = 'D';
    static final char GOOD_BOMB = '*';
    static final char PLAYER = '☺';
    static final char EMPTY = '\0';

    static final char LEFT = 'l';
    static final char RIGHT = 'r';
    static final char UP = 'u';
    static final char DOWN = 'd';

    static int PLAYER_X_POSITON = 0;
    static int PLAYER_Y_POSITON = 0;

    static int LIFE_POINTS = 5;
    static int COUNT = 0;

    static int getNumber(int from, int to) {
        do {
            System.out.printf("Give me a number between %d and %d ", from, to);
            if (SC.hasNextInt()) {
                int number = SC.nextInt();
                if (number >= from && number <= to) {
                    return number;
                }
            } else {
                nextLine();
            }
        } while (true);
    }

    static void nextLine() {
        SC.next();
    }

    static void printMap(char[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == EMPTY) {
                    System.out.print("\\");
                } else {
                    System.out.print(map[i][j]);
                }
            }
            System.out.println();
        }
    }

    static void play(int row, int col) {
        char[][] map = new char[row][col];
        putPlayer(map);
        printMap(map);
        putBombs(map);
        step(map);
    }

    static void putPlayer(char[][] map) {
        map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = PLAYER;
    }

    static void putBombs(char[][] map) {
        int numnerOfBombs = getNumber(MIN_BOMB_NUMBER, MAX_BOMB_NUMBER);
        int deadlyBombsNumber = deadlyBombNumber(numnerOfBombs);
        int goodBombsNumber = goodBombsNumber(numnerOfBombs);
        setBombs(map, deadlyBombsNumber, BAD_BOMB);
        setBombs(map, goodBombsNumber, GOOD_BOMB);
        printMap(map);
    }

    static int generateRandomNumber(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static void setBombs(char[][] map, int num, char character) {
        for (int i = 0; i < num; i++) {
            int x = generateRandomNumber(0, map.length);
            int y = generateRandomNumber(0, map[i].length);
            if (map[x][y] == EMPTY) {
                map[x][y] = character;
            } else {
                i--;
            }
        }
    }

    static int deadlyBombNumber(int numberOfBombs) {
        return numberOfBombs / 2;
    }

    static int goodBombsNumber(int numberOfBombs) {
        return numberOfBombs / 2;
    }

    static void handleBombs(char[][] map, int x, int y) {
        switch (map[x][y]) {
            case BAD_BOMB:
                LIFE_POINTS = 0;
                break;
            case GOOD_BOMB:
                LIFE_POINTS--;
            default:
                break;
        }
    }

    static void step(char[][] map) {
        while (LIFE_POINTS > 0) {
            char c = SC.next().charAt(0);
            switch (c) {
                case 'r':
                    rightStep(map);
                    break;
                case 'l':
                    leftStep(map);
                    break;
                case 'u':
                    upStep(map);
                    break;
                case 'd':
                    downStep(map);
                    break;
            }
            printMap(map);
            System.out.println("Life remaning: " + LIFE_POINTS);
            System.out.println("You have stepped: " + COUNT);
            if (LIFE_POINTS == 0) {
                System.out.println("You are dead ☠");
            }
        }
    }

    static void rightStep(char[][] map) {
        if(!playerIsLastColumn(map)){
            handleBombs(map, PLAYER_X_POSITON, PLAYER_Y_POSITON + 1);
            if(LIFE_POINTS > 0){
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = EMPTY;
                PLAYER_Y_POSITON++;
                count(COUNT);
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = PLAYER;
             }
        }
    }

    static void leftStep(char[][] map) {
        if (!playerIsFirstColumn(map)) {
            handleBombs(map, PLAYER_X_POSITON, PLAYER_Y_POSITON - 1);
            if (LIFE_POINTS > 0) {
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = EMPTY;
                PLAYER_Y_POSITON--;
                count(COUNT);
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = PLAYER;
            }
        }
    }

    static void upStep(char[][] map) {
        if (!playerIsFirstRow(map)) {
            handleBombs(map, PLAYER_X_POSITON - 1, PLAYER_Y_POSITON);
            if (LIFE_POINTS > 0) {
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = EMPTY;
                PLAYER_X_POSITON--;
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = PLAYER;
            }
        }
    }

    static void downStep(char[][] map) {
        if (!playerIsLastRow(map)) {
            handleBombs(map, PLAYER_X_POSITON + 1, PLAYER_Y_POSITON);
            if (LIFE_POINTS > 0) {
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = EMPTY;
                PLAYER_X_POSITON++;
                map[PLAYER_X_POSITON][PLAYER_Y_POSITON] = PLAYER;
            }
        }
    }

    static int count(int number) {
        return COUNT++;
    }

    static boolean playerIsLastColumn(char[][] map) {
        return PLAYER_Y_POSITON == map[0].length - 1;
    }

    static boolean playerIsLastRow(char[][] map) {
        return PLAYER_X_POSITON == map.length - 1;
    }

    static boolean playerIsFirstColumn(char[][] map) {
        return PLAYER_Y_POSITON <= map[0][0];
    }

    static boolean playerIsFirstRow(char[][] map) {
        return PLAYER_X_POSITON <= map[0][0];
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int row = getNumber(MIN_NUMBER, MAX_NUMBER);
        int col = getNumber(MIN_NUMBER, MAX_NUMBER);

        play(row, col);
    }
}
