/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author s26841
 */
public class Arrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int [] array = new int [] {1, 5, 3, 2, 9};
        int [] array2 = {5, 6, 4, 23};
        double [] array3 = {5.5d, 6.6d, 7.7d, 12.562348d};
        double [] array4 = new double[9];
        float [] array5 = new float[] {1f, 5f, 3f, 2f, 9f};
        
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        for (int i = 0; i < array2.length; i++) {
            System.out.println(array2[i]);
        }
        for (int i = 0; i < array3.length; i++) {
            System.out.println(array3[i]);
        }
        for (int i = 0; i < array4.length; i++) {
            System.out.println(array4[i]);
        }
        for (int i = 0; i < array5.length; i++) {
            System.out.println(array5[i]);
        }
    }
}
