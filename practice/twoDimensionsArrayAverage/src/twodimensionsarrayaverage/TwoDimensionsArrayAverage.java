/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twodimensionsarrayaverage;

/**
 *
 * @author george
 */
public class TwoDimensionsArrayAverage {

    static int getRandomNumber(int from, int to) {
        return (int) (Math.random() * (++to - from) + from);
    }

    static void fillByRandomNumbers(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = getRandomNumber(0, 255);
            }
        }
    }

    static double[][] copyArray(int[][] array) {
        double[][] result = new double[array.length][array[0].length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                result[i][j] = array[i][j];
            }
        }
        return result;
    }

    static double avg(int[][] arr, int row, int col) {
        int sum = 0;
        for (int i = row; i < row + 3; i++) {
            for (int j = col; j < col + 3; j++) {
                sum += arr[i][j];
            }
        }
        return sum / 9D;
    }

    static void doAveraging(int[][] from, double[][] to) {
        for (int i = 1; i < to.length - 1; i++) {
            for (int j = 1; j < to[i].length - 1; j++) {
                to[i][j] = avg(from, i - 1, j - 1);
            }
        }
    }

    static void print(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void print(double[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.printf("%.2f ", array[i][j]);
            }
            System.out.println();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int size = getRandomNumber(10, 15);
        int[][] array = new int[size][size];
        fillByRandomNumbers(array);
        double[][] copied = copyArray(array);
        doAveraging(array, copied);
        print(array);
        System.out.println();
        print(copied);
    }
}
