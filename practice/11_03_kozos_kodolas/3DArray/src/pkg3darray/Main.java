/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3darray;

import java.util.Scanner;

/**
 *
 * @author czirjak_zoltan
 */
public class Main {

    static final int MAX_NUMBER_OF_SHOOTS = 20;
    static final int SHIP = 7;
    static final int MIN_SIZE = 5;
    static final int PENALTY = 2;
    static final float REWARD = 0.5f;

    static final Scanner SCANNER = new Scanner(System.in);

    static double currentLifePoints = MAX_NUMBER_OF_SHOOTS;
    static int numberOfHitShips = 0;
    static int[][][] world;

    static int getSize() {
        do {
            if (SCANNER.hasNextInt()) {
                int size = SCANNER.nextInt();
                if (size >= MIN_SIZE) {
                    return size;
                }

            } else {
                SCANNER.next();
            }

        } while (true);

    }

    static int getNumber() {
        do {
            if (SCANNER.hasNextInt()) {
                return SCANNER.nextInt();

            } else {
                SCANNER.next();
            }

        } while (true);

    }

    static void initWorld() {
        int a = getSize();
        int b = getSize();
        int c = getSize();

        world = new int[a][b][c];
    }

    static int calculateNumberOfShips() {
        return world.length * world[0].length * world[0][0].length / 2;
    }

    static int generatePosition(int to) {
        return (int) (Math.random() * to);
    }

    static void placeShips() {
        int numberOfShips = calculateNumberOfShips();

        for (int i = 0; i < numberOfShips; i++) {
            int x = generatePosition(world.length);
            int y = generatePosition(world[0].length);
            int z = generatePosition(world[0][0].length);

            if (world[x][y][z] != SHIP) {
                world[x][y][z] = SHIP;

            } else {
                i--;

            }
        }
    }

    static void play() {
        do {
            System.out.println("Give me an (x,y,z) coordinate: ");
            int x = getNumber();
            int y = getNumber();
            int z = getNumber();

            handleCoordinates(x, y, z);

        } while (currentLifePoints > 0);

        finishGame();
    }

    static void finishGame() {
        System.out.printf("Game has been finished, hit ships: %d\n", numberOfHitShips);
    }

    static void handleCoordinates(int x, int y, int z) {
        if (!onMap(x, y, z)) {
            currentLifePoints -= PENALTY;
            System.out.printf("Out of space, life points: %d\n", currentLifePoints);

        } else if (isTargetHit(x, y, z)) {
            currentLifePoints += REWARD;
            numberOfHitShips++;

            System.out.printf("A ship has been targeted, life points: %d\n", currentLifePoints);

        } else {
            currentLifePoints--;
            System.out.printf("Life points: %d\n", currentLifePoints);

        }
    }

    static boolean onMap(int x, int y, int z) {
        return x >= 0 && x <= world.length - 1
                && y >= 0 && y <= world[0].length - 1
                && z >= 0 && z <= world[0][0].length - 1;
    }

    static boolean isTargetHit(int x, int y, int z) {
        return world[x][y][z] == SHIP;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        initWorld();
        placeShips();
        play();

        SCANNER.close();
    }

}
