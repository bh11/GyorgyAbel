/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package palindrome;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class Palindrome {

    static Scanner sc = new Scanner(System.in);

    static char [] getText() {
        do {
            System.out.println("Please enter a text: ");
            if (sc.hasNext()) {
                String text = sc.nextLine();
                char [] array = text.toCharArray();
                return array;
            }
        } while (true);

    }

    static char [] cleaningSpace(char [] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == ' ') {
                count++;
            }
        }
        char [] resultArray = new char [(array.length) - count];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if(array[i] != ' '){
                resultArray[j] = array[i];
                j++;
            }
        }
        return resultArray;
    }

    static boolean palindrome(char [] array) {
        array = cleaningSpace(array);
        boolean isPalindrome = true;
        int arrayZeroElement = 0;
        int arrayLength = array.length - 1;
        while (arrayLength > arrayZeroElement) {
            if (array[arrayZeroElement] != array[arrayLength]) {
                return !isPalindrome;
            }
            ++arrayZeroElement;
            --arrayLength;
        }
        return isPalindrome;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        char [] text = getText();
        if (palindrome(text)) {
            System.out.println("This text is palindrome");
        } else {
            System.out.println("This text is not palindrome");
        }
        sc.close();
    }
}
