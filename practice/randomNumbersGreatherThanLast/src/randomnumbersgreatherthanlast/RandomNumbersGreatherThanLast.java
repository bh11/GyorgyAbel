/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package randomnumbersgreatherthanlast;

/**
 *
 * @author s26841
 */
public class RandomNumbersGreatherThanLast {

    static int[] randomNumbers() {
        int[] array = new int[10000];
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                array[i] = randomGenerator(array[i - 1] + 1, array[i - 1] + 100);
            }

        }
        return array;
    }

    static int[] checkSequence() {
        int[] array = randomNumbers();
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                array[i + 1] = randomGenerator(array[i] + 1, array[i] + 1000);
                i--;
            }
        }
        return array;
    }

    static int randomGenerator(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[] array = checkSequence();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

}
