/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

    
/**
 *
 * @author george
 */
public class huszasFeladat {
    
    static boolean isLowerCase(char c) {
        return c >= 'a' && c <= 'z';
    }
    static int countSmallLetters(char[] arr) {
        int size = 0;
        for (int i = 0; i < arr.length; i++) {
            if (isLowerCase(arr[i])) {
                size++;
            }
        }
        return size;
    }
    static char[] pickSmallLetters(char[] arr) {
        char[] result = new char[countSmallLetters(arr)];
        int count = 0;
        
        for (int i = 0; i < arr.length; i++) {
            if (isLowerCase(arr[i])) {
                result[count++] = arr[i];
            }
        }
        
        return result;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        char[] test = pickSmallLetters(new char[]{'a', 'A', 'a', 'b', 'B'});
        
        for (int i = 0; i < test.length; i++) {
            System.out.println(test[i]);
        }
    }
}
