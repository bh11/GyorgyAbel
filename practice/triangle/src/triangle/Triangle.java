/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triangle;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Triangle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Kérjünk be a felhasználótól 3 számot és mondjuk meg, hogy lehet-e egyenlő szárú háromszög.
        
        Scanner scanner = new Scanner(System.in);

        System.out.println("Kérem adjon meg egy számot: ");
        if (scanner.hasNextInt()) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int c = scanner.nextInt();

            boolean abGtc = a + b > c;
            boolean acGtb = a + c > b;
            boolean cbGta = c + b > a;
            boolean isCorrectTriangle = a == b || a == c || b == c;

            if (abGtc && acGtb && cbGta && isCorrectTriangle) {
                System.out.println("Lehet egyenlő szárú háromszög!");
            } else {
                System.out.println("Nem lehet egyenlő szárú háromszög");
            }
        } else {
            System.out.println("Nem számot adtál meg!!!");
        }
    }

}
