/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comperable;

/**
 *
 * @author s26841
 */
public class Person implements Comparable{
    
    private int age;
    private String name;

    public Person() {}
    
    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }
    
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" + "age=" + age + ", name=" + name + '}';
    }

    @Override
    public int compareTo(Object a) {
        int compareage = ((Person) a).getAge();
        return this.age - compareage;
    }
}
