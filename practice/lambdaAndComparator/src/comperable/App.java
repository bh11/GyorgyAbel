/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comperable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author s26841
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        List<Person> persons = new ArrayList<>();
        
        persons.add(new Person(28, "Gyula"));
        persons.add(new Person(21, "Kati"));
        persons.add(new Person(53, "Béla"));
        persons.add(new Person(40, "Norbi"));
        
        Collections.sort(persons);
        
        for (Person person : persons) {
            System.out.println(person);
        }
        System.out.println();
        persons.forEach(person -> System.out.println(person));
        
    }
    
}
