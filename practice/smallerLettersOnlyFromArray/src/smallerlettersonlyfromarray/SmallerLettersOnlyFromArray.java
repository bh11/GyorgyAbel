/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smallerlettersonlyfromarray;

/**
 *
 * @author s26841
 */
public class SmallerLettersOnlyFromArray {

    static boolean isLowerCase(char c){
        return c >= 'a' && c <= 'z';
    }
    
    static int countSmallerCase(char [] array){
        int num = 0;
        for (int i = 0; i < array.length; i++) {
            if(isLowerCase(array[i])){
                num++;
            }
        }
        return num;
    }
    
    static char [] lowerCases(char [] array){
        char [] result = new char [countSmallerCase(array)];
        int num = 0;
        for (int i = 0; i < array.length; i++) {
            if(isLowerCase(array[i])){
                result[num++] = array[i];
            }
        }
        return result;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        char array[] = lowerCases(new char [] {'a', 'b', 'c', 'M', 'm', 'U', 'p', 'l'});
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
