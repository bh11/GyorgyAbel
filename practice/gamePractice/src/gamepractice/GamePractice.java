/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamepractice;

import java.util.Scanner;

/**
 *
 * @author george
 */
public class GamePractice {

    static Scanner SC = new Scanner(System.in);

    static final int SIZE = 10;
    static final int NUMBER_OF_STAR = 5;
    static final int NUMBER_OF_QUESTIONMARK = 3;
    static int PLAYER_X_POSITION = 0;
    static int PLAYER_Y_POSITION = 0;
    static int COUNT = 0;
    static char[][] FIELD = new char[SIZE][SIZE];

    static final char STAR = '*';
    static final char QUESTIONMARK = '?';

    static final char PLAYER = '☺';
    static final char RIGHT = 'r';
    static final char LEFT = 'l';
    static final char UP = 'u';
    static final char DOWN = 'd';
    static final char EMPTY = '\0';

    static void printField() {
        for (int i = 0; i < FIELD.length; i++) {
            for (int j = 0; j < FIELD[i].length; j++) {
                if (FIELD[i][j] == EMPTY) {
                    System.out.print("\\");
                } else {
                    System.out.print(FIELD[i][j]);
                }
            }
            System.out.println();
        }
    }

    static void putCharacters() {
        putPlayer();
        setStars();
        setQuestionMarks();
    }

    static void play() {
        putCharacters();
        printField();
        steps();
    }

    static void steps() {
        while (isCharacterOnField(QUESTIONMARK)) {
            char c = SC.next().charAt(0);
            switch (c) {
                case 'r':
                    rightStep();
                    break;
                case 'l':
                    leftStep();
                    break;
                case 'u':
                    upStep();
                    break;
                case 'd':
                    downStep();
                    break;
            }
            printField();
            System.out.println("You have stepped so far: " + COUNT);
            if (!isCharacterOnField(QUESTIONMARK)) {
                System.out.println("You picked up the all characters!");
            }
        }
    }

    static void count() {
        COUNT++;
    }

    static boolean isCharacterOnField(char character) {
        boolean isStars = false;
        for (int i = 0; i < FIELD.length; i++) {
            for (int j = 0; j < FIELD[i].length; j++) {
                if (FIELD[i][j] == character) {
                    isStars = true;
                    return isStars;
                }
            }
        }
        return isStars;
    }

    static void putPlayer() {
        FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = PLAYER;
    }

    static void setStars() {
        for (int i = 0; i < NUMBER_OF_STAR; i++) {
            int x = randomGenerator(0, FIELD.length);
            int y = randomGenerator(0, FIELD[i].length);
            if (FIELD[x][y] == EMPTY || FIELD[x][y] == STAR) {
                FIELD[x][y] = STAR;
            }
        }
    }

    static void setQuestionMarks() {
        for (int i = 0; i < NUMBER_OF_QUESTIONMARK; i++) {
            int x = randomGenerator(0, FIELD.length);
            int y = randomGenerator(0, FIELD[i].length);
            if (FIELD[x][y] == EMPTY) {
                FIELD[x][y] = QUESTIONMARK;
            } else {
                i--;
            }
        }
    }

    static boolean handleQuestionMarks(int a, int b) {
        boolean questionmarkWithStar = false;
        if (FIELD[a][b] == QUESTIONMARK && isCharacterOnField(STAR)) {
            questionmarkWithStar = true;
            return questionmarkWithStar;
        } else if (FIELD[a][b] == QUESTIONMARK && !isCharacterOnField(STAR)) {
            return questionmarkWithStar;
        }
        return questionmarkWithStar;
    }

    static void rightStep() {
        if (!playerIsLastColumn()) {
            if (handleQuestionMarks(PLAYER_X_POSITION, PLAYER_Y_POSITION + 1)) {
                System.out.println("You are not able to pick up question mark until star is on the map!");
            } else {
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = EMPTY;
                PLAYER_Y_POSITION++;
                count();
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = PLAYER;
            }
        }
    }

    static void leftStep() {
        if (!playerIsFirstColumn()) {
            if (handleQuestionMarks(PLAYER_X_POSITION, PLAYER_Y_POSITION - 1)) {
                System.out.println("You are not able to pick up question mark until star is on the map!");
            } else {
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = EMPTY;
                PLAYER_Y_POSITION--;
                count();
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = PLAYER;
            }
        }
    }

    static void upStep() {
        if (!playerIsFirstRow()) {
            if (handleQuestionMarks(PLAYER_X_POSITION - 1, PLAYER_Y_POSITION)) {
                System.out.println("You are not able to pick up question mark until star is on the map!");
            } else {
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = EMPTY;
                PLAYER_X_POSITION--;
                count();
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = PLAYER;
            }
        }
    }

    static void downStep() {
        if (!playerIsLastRow()) {
            if (handleQuestionMarks(PLAYER_X_POSITION + 1, PLAYER_Y_POSITION)) {
                System.out.println("You are not able to pick up question mark until star is on the map!");
            } else {
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = EMPTY;
                PLAYER_X_POSITION++;
                count();
                FIELD[PLAYER_X_POSITION][PLAYER_Y_POSITION] = PLAYER;
            }
        }
    }

    static boolean playerIsLastColumn() {
        return PLAYER_Y_POSITION == FIELD[0].length - 1;
    }

    static boolean playerIsLastRow() {
        return PLAYER_X_POSITION == FIELD.length - 1;
    }

    static boolean playerIsFirstColumn() {
        return PLAYER_Y_POSITION <= FIELD[0][0];
    }

    static boolean playerIsFirstRow() {
        return PLAYER_X_POSITION <= FIELD[0][0];
    }

    static int randomGenerator(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        play();

        SC.close();
    }

}
