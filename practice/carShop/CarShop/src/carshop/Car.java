/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop;

import java.util.Objects;

/**
 *
 * @author s26841
 */
public abstract class Car {
    
    protected static int Id = 10000;
    protected double distance;
    protected double consumption;
    protected int power;
    protected String plate;
    protected String color;
    protected int price;

    public Car(double distance, double consumption, int power, String plate, String color, int price) {
        this.distance = distance;
        this.consumption = consumption;
        this.power = power;
        this.plate = plate;
        this.color = color;
        this.price = price;
        this.Id++;
    }
    
    public abstract void test();
    
    public abstract int getId();

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getConsumption() {
        return consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.distance) ^ (Double.doubleToLongBits(this.distance) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.consumption) ^ (Double.doubleToLongBits(this.consumption) >>> 32));
        hash = 89 * hash + this.power;
        hash = 89 * hash + Objects.hashCode(this.plate);
        hash = 89 * hash + Objects.hashCode(this.color);
        hash = 89 * hash + this.price;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Car other = (Car) obj;
        if (Double.doubleToLongBits(this.distance) != Double.doubleToLongBits(other.distance)) {
            return false;
        }
        if (Double.doubleToLongBits(this.consumption) != Double.doubleToLongBits(other.consumption)) {
            return false;
        }
        if (this.power != other.power) {
            return false;
        }
        if (this.price != other.price) {
            return false;
        }
        if (!Objects.equals(this.plate, other.plate)) {
            return false;
        }
        if (!Objects.equals(this.color, other.color)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Car{" + "distance=" + distance + ", consumption=" + consumption + ", power=" + power + ", plate=" + plate + ", color=" + color + ", price=" + price + '}';
    }
    
    
    
}
