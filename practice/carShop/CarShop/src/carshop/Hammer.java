/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop;

/**
 *
 * @author s26841
 */
public class Hammer extends Car{

    private final int Id;

    public Hammer(int Id, double distance, double consumption, int power, String plate, String color, int price) {
        super(distance, consumption, power, plate, color, price);
        this.Id = Car.Id;
    }

    @Override
    public void test() {
        System.out.println("Hammer test");
        System.out.println("I do nothing");
    }

    @Override
    public int getId() {
        return this.Id;
    }

    @Override
    public String toString() {
        return "Hammer{" + "Id=" + Id + "Palte= " + this.plate + '}';
    }
    
}
