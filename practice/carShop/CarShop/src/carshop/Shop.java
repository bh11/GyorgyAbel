/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop;

import Exception.CarLimitException;
import Exception.EmptyCarShopException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author s26841
 */
public class Shop {
    
    protected List <Car> cars = new ArrayList<>();   
    
    private int sumPrice = 0;
    
    public Shop(){  
    }
    
    public void addCar(Car car) throws CarLimitException{
        if (cars.size() == 5) {
            throw new CarLimitException("Car limit is 5");
        }
        cars.add(car);
        sumPrice += car.getPrice();
    }
    
    public void testCar() throws EmptyCarShopException{
        if (cars.isEmpty()) {
            throw new EmptyCarShopException("The Carshop is empty");
        }
        for (Car car : cars) {
            if (car.getId() % 2 == 0) {
                car.test();
                System.out.println(car);
            }
        }
    }

    public List<Car> getCars() {
        return cars;
    }
    
    public int numberOfCarInShop(){
        return cars.size();
    }
    
    public int getSumPriceOfCar(){
        return sumPrice;
    }
}
