/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop;

/**
 *
 * @author s26841
 */
public class Mercedes extends Car{

    private final int Id;

    public Mercedes(int Id, double distance, double consumption, int power, String plate, String color, int price) {
        super(distance, consumption, power, plate, color, price);
        this.Id = Car.Id;
    }
   
    @Override
    public void test() {
        System.out.println("Mercedes test");
        this.consumption += 0.04;
    }

    @Override
    public int getId() {
        return this.Id;
    }

    @Override
    public String toString() {
        return "Mercedes{" + "Id= " + Id + " Plate= " + this.plate + " Consumption= " + consumption + '}';
    }
    
}
