/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop;

import Exception.CarLimitException;
import Exception.EmptyCarShopException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author s26841
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Shop carShop = new Shop();
        
        Car mercedes = new Mercedes(0, 2500, 7.6, 160, "AMD-255", "fekete", 5_500_000);
        Car mercedes2 = new Mercedes(0, 16000, 7.1, 360, "AHD-702", "fehér", 7_500_000);
        Car hammer = new Hammer(0, 200, 4.6, 80, "FDD-235", "kék", 10_500_000);
        Car hammer2 = new Hammer(0, 26500, 5.6, 130, "GFD-452", "pink", 15_500_000);
        Car bmw = new Bmw(0, 34333, 5.6, 201, "WRE-324", "barna", 2_500_000);
        Car bmw2 = new Bmw(0, 2240, 6.6, 245, "GDF-243", "lila", 6_500_000);
               
        System.out.println("Before the tests!");

        try {
            
            carShop.addCar(mercedes);
            carShop.addCar(mercedes2);
            carShop.addCar(hammer);
            carShop.addCar(hammer2);
            //carShop.addCar(bmw);
            carShop.addCar(bmw2);
        } catch (CarLimitException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        carShop.getCars().forEach(System.out::println);
        

        System.out.println("\nAfter the tests!");
        
        try {
            carShop.testCar();
        } catch (EmptyCarShopException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        carShop.getCars().forEach(System.out::println);

        System.out.println("\nNumber of cars in shop: " + carShop.numberOfCarInShop());
        System.out.println("\nSum price of cars in shop: " + carShop.getSumPriceOfCar());
        
    }
    
}
