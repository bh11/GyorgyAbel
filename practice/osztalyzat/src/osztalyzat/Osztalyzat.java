/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package osztalyzat;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Osztalyzat {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Adjuk meg 1-5 közötti jegyet és döntsük el, hogy megfelel-e.
        // 1-3 között nem felelt meg, 4-5 között megfelelt.
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Kérlek add meg az osztályzatot: ");
        if (scanner.hasNextInt()) {
            int a = scanner.nextInt();
            switch (a) {
                case 5:
                case 4:
                    System.out.println("Megfelelt!");
                    break;
                case 3:
                case 2:
                case 1:
                    System.out.println("Nem felelt meg!");
                    break;
                default:
                    System.out.println("Nem 1 és 5 közötti számot adtál meg!");
            }
        } else {
            System.out.println("Nem számot adtál meg!");
        }
    }

}
