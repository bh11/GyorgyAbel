/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fivenumbersin;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class FiveNumbersIn {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);
        long sum = 0;
        int i = 0;
        do {
            System.out.println("Kérem adjon meg egy darab számot (5 számot fogok bekérni összesen): ");
            if(scanner.hasNextLong()){
                long number = scanner.nextLong();
                sum = sum + number;
                i++;
            }else{
                System.out.println("Nem számot adtál meg!");
                scanner.next();
            }
        } while (i < 5);
        System.out.println("A számok összege: " + sum);
    }

}
