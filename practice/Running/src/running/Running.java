/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package running;

import java.util.Scanner;

/**
 *
 * @author s26841
 */

public class Running {

    static Scanner SC = new Scanner (System.in);
            
    static int [] maxRunner = new int [scanNumber()];
    
    static int singleCount = 0;
    static int duoCount = 0;
    static int trioCount = 0;
    static int freePlacesCount = 0;
    
    static int duo = 0;
    static int trio = 0;
    
    static int scanNumber(){
        do {
            System.out.println("Kérlek add meg a maximális futók számát: ");
            if (SC.hasNextInt()) {
                int num = SC.nextInt();
                return num;
            }else{
                SC.next();
            }            
        } while (true);
    }
    
    static char typeOfRunning(){
        do {
            System.out.println("Kérlek add meg milyen számra neveznél, ha egyéni 's', ha duóban 'd', ha trióban 't' majd üss entert: ");
            if (SC.hasNext()) {
                char c = SC.next().charAt(0);
                return c;
            }else{
                SC.next();
            }            
        } while (true);
    }
    
    static boolean checkFreePlaces(){
        boolean freePlace = false;
        for (int i = 0; i < maxRunner.length; i++) {
            if (maxRunner[i] == 0){
                freePlace = true;
                return freePlace;
            }   
        }
        return freePlace;
    }
    
    static void uploadSingleRunner(){
        for (int i = 0; i < maxRunner.length; i++) {
            if (maxRunner[i] == 0) {
                maxRunner[i] = 1;
                System.out.println("Sikeres egyéni nevezés!");
                break; 
            }   
        }
    }
    
    static void printRunners(){
        for (int i = 0; i < maxRunner.length; i++) {
            if (maxRunner[i] == 1) {
                singleCount++;
            }else if(maxRunner[i] == 2){
                duoCount++;
            }else if (maxRunner[i] == 3) {
                trioCount++;
            }
        }
    }
    
    static void waitingDuoList(){
        if (duo == 0) {
            duo++;
            System.out.println("Felvettelek a várólistára, mivel most még nincs olyan futó, aki tudna duóban indulni veled!");
        }else{
            duo = 0;
            for (int i = 0; i < maxRunner.length; i++) {
                if(maxRunner[i] == 0){
                    maxRunner[i] = 2;
                    System.out.println("Találtam neked egy csapattársat a futáshoz! A nevezés sikeres volt. Gratulálok");
                    break;
                }
            }
        }
    }
    
    static void waitingTrioList(){
        if (trio == 0 || trio == 1) {
            trio++;
            System.out.println("Felvettelek a várólistára, mivel most még nincsenek olyan futók, aki tudnának trióban indulni veled!");
        }else{
            trio = 0;
            for (int i = 0; i < maxRunner.length; i++) {
                if(maxRunner[i] == 0){
                    maxRunner[i] = 3;
                    System.out.println("Találtam neked csapattársakat a futáshoz! A nevezés sikeres volt. Gratulálok");
                    break;
                }
            }
        }
    }
    
    static void askTypeOfRunning(){
        while(checkFreePlaces()) {
            switch(typeOfRunning()){
                case 's': uploadSingleRunner(); break;
                case 'd': waitingDuoList(); break;
                case 't': waitingTrioList(); break;
                default: break;
            }
        }
        if(!checkFreePlaces()){
            System.out.println("Sajnálom. A nevezést lezártuk, mivel nincs több szabad hely!");
            printRunners();
            System.out.println("Egyéniben nevezett versenyzők száma: " + singleCount);
            System.out.println("Duóban nevezett versenyzők száma: " + duoCount);
            System.out.println("Trióban nevezett versenyzők száma: " + trioCount);
            System.out.println("Párok hiányában a kimaradt duó versenyzők száma: " + duo);
            System.out.println("Párok hiányában a kimaradt trió versenyzők száma: " + trio);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        askTypeOfRunning();
        SC.close();
    }
}
