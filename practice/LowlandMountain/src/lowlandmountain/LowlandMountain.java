/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lowlandmountain;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class LowlandMountain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        // Kérjünk be a felhasználótól egy számot és mondjuk meg, hogy milyen domborzatnak felel meg.
        // 0-200 között Alföld, 201-500 között Dombság, 501-1500 között Középhegység, 1501 fölött Hegység
        
        Scanner scanner = new Scanner(System.in);

        System.out.println("Kérem adjon meg egy számot: ");
        if (scanner.hasNextInt()) {
            int number = scanner.nextInt();
            if (number <= 200) {
                System.out.println("Alföld");
            } else if (number <= 500) {
                System.out.println("Dombság");
            } else if (number <= 1500) {
                System.out.println("Középhegység");
            } else {
                System.out.println("Hegység");
            }
        }        
    }
}
