/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package letters;

import java.util.Scanner;

/**
 *
 * @author s26841
 */
public class Letters {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Kérlek az meg egy nagybetűt vagy kisbetűt vagy számot: ");
        char a = scanner.next().charAt(0);
        if (a >= 'A' && a <= 'Z') {
            System.out.println("Nagy betűt írtál be!");
        } else if (a > 'a' && a < 'z') {
            System.out.println("Kis betűt írtál be!");
        } else if (a <= 0 && a <= 9) {
            System.out.println("Nem betűt, vagy számot adtál meg!");
        } else {
            System.out.println("Számot írtál be!");
        }
    }
}
