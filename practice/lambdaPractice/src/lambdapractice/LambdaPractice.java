/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdapractice;

/**
 *
 * @author s26841
 */
public class LambdaPractice {
    static final int VALUE = 10;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Program programs = new Program();
        
        Fly flying = () -> {
                System.out.println("Repülök");
                System.out.println("Ez is repül");
            };
        
        Fly flying2 = () -> {
            int i = 15;
            System.out.println(i + " Itt is repül " + VALUE);
        };
        
        Fly flying3 = () -> System.out.println("reprep");
        
        Fly flying4 = () -> System.out.println("repüla repüla");
        
        programs.doSomething(flying);
        programs.doSomething(flying2);
        programs.doSomething(flying3);
        programs.doSomething(flying4);
        
        Thread t1 = new Thread(new Runnable(){
            @Override
            public void run() {
                System.out.println("Futok!");
            }
        });
        
        Thread t2 = new Thread(() -> System.out.println("futifuti"));
        
        t1.start();
        t2.start();
        
    }
    
}
