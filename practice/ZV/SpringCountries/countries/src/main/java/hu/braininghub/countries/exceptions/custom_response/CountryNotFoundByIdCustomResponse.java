/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.countries.exceptions.custom_response;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;

/**
 *
 * @author george
 */

public class CountryNotFoundByIdCustomResponse {
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestape;
    private Integer status;
    private String errorMessage;

    public CountryNotFoundByIdCustomResponse() {}
    
    public CountryNotFoundByIdCustomResponse(LocalDateTime timestape, Integer status, String errorMessage) {
        this.timestape = timestape;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public LocalDateTime getTimestape() {
        return timestape;
    }

    public void setTimestape(LocalDateTime timestape) {
        this.timestape = timestape;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
