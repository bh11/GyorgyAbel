package hu.braininghub.countries.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.braininghub.countries.repository.entity.Continent;

@Repository
public interface ContinentDao extends JpaRepository<Continent, Long> {

    @Override
    List<Continent> findAll();

    @Override
    Optional<Continent> findById(Long id);

    List<Continent> findByNameContainingIgnoreCase(String name);
}
