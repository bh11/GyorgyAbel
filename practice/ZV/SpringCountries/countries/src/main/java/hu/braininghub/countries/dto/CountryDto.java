package hu.braininghub.countries.dto;

import hu.braininghub.countries.repository.entity.Continent;

public class CountryDto {

    private Long id;
    private String name;
    private Continent continent;

    public CountryDto() {
    }

    public CountryDto(Long id, String name, Continent continent) {
        super();
        this.id = id;
        this.name = name;
        this.continent = continent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((continent == null) ? 0 : continent.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CountryDto other = (CountryDto) obj;
        if (continent == null) {
            if (other.continent != null) {
                return false;
            }
        } else if (!continent.equals(other.continent)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CountryDto [id=" + id + ", countryName=" + name + ", continent=" + continent + "]";
    }
}
