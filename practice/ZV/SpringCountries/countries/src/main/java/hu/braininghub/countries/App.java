package hu.braininghub.countries;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"hu.braininghub.countries.repository"})
@EntityScan(basePackages = {"hu.braininghub.countries.repository.entity"})
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
