package hu.braininghub.countries.mapper;

import org.springframework.stereotype.Component;

import hu.braininghub.countries.dto.ContinentDto;
import hu.braininghub.countries.repository.entity.Continent;

@Component
public class ContinentMapper implements Mapper<Continent, ContinentDto>{

	@Override
	public Continent toEntity(ContinentDto dto) {
		Continent entity = new Continent();
		entity.setId(dto.getId());
		entity.setContinentName(dto.getName());
		return entity;
	}

	@Override
	public ContinentDto toDto(Continent entity) {
		ContinentDto dto = new ContinentDto();
		dto.setId(entity.getId());
		dto.setName(entity.getContinentName());
		return dto;
	}

}
