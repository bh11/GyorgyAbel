package hu.braininghub.countries.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.braininghub.countries.repository.entity.Country;

@Repository
public interface CountryDao extends JpaRepository<Country, Long> {

    @Override
    List<Country> findAll();

    @Override
    Optional<Country> findById(Long id);

    List<Country> findByNameContainingIgnoreCase(String name);
}
