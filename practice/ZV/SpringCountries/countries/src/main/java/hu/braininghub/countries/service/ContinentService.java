package hu.braininghub.countries.service;

import hu.braininghub.countries.dto.ContinentDto;
import hu.braininghub.countries.mapper.ContinentMapper;
import hu.braininghub.countries.repository.ContinentDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContinentService {

    private ContinentDao continentDao;
    private ContinentMapper continentMapper;

    @Autowired
    public void setContinentDao(ContinentDao continentDao) {
        this.continentDao = continentDao;
    }

    @Autowired
    public void setContinentMapper(ContinentMapper continentMapper) {
        this.continentMapper = continentMapper;
    }

    public List<ContinentDto> getContinentByPartOfName(String name) {

        List<ContinentDto> result = new ArrayList<>();

        continentDao.findByNameContainingIgnoreCase(name).forEach(continent -> {
            result.add(continentMapper.toDto(continent));
        });

        return result;
    }
}
