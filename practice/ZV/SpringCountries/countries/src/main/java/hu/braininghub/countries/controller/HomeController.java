package hu.braininghub.countries.controller;

import hu.braininghub.countries.dto.ContinentDto;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.braininghub.countries.dto.CountryDto;
import hu.braininghub.countries.service.ContinentService;
import hu.braininghub.countries.service.CountryService;

@RestController
public class HomeController {

    private CountryService countryService;
    private ContinentService continentService;

    @Autowired
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    @Autowired
    public void setContinentService(ContinentService continentService) {
        this.continentService = continentService;
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {

        return "Hello Gyuri fut a spring appod";
    }

    @RequestMapping(value = "/countryById", method = RequestMethod.GET)
    public ResponseEntity<CountryDto> getCountryById(@RequestBody Long id){
        
            CountryDto result = countryService.getCountryById(id);
            return ResponseEntity.ok(result);
            
    }

    @RequestMapping(value = "/countryByPartOfName", method = RequestMethod.GET)
    public ResponseEntity<List<CountryDto>> getCountryByName(@RequestBody String name) {

        List<CountryDto> result = countryService.getCountryByPartOfName(name);
        return ResponseEntity.ok(result);
    }
    
    @RequestMapping(value = "/continentByPartOfName", method = RequestMethod.GET)
    public ResponseEntity<List<ContinentDto>> getContinentByName(@RequestBody String name) {

        List<ContinentDto> result = continentService.getContinentByPartOfName(name);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public ResponseEntity<List<CountryDto>> getAllCountries() {

        List<CountryDto> result = countryService.getAllCountries();
        return ResponseEntity.ok(result);
    }
}
