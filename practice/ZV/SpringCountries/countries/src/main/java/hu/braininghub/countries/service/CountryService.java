package hu.braininghub.countries.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.braininghub.countries.dto.CountryDto;
import hu.braininghub.countries.exceptions.CountryNotFoundByIdException;
import hu.braininghub.countries.mapper.ContinentMapper;
import hu.braininghub.countries.mapper.CountryMapper;
import hu.braininghub.countries.repository.ContinentDao;
import hu.braininghub.countries.repository.CountryDao;
import hu.braininghub.countries.repository.entity.Country;

@Service
public class CountryService {

    private CountryDao countryDao;
    private ContinentDao continentDao;
    private CountryMapper countryMapper;
    private ContinentMapper continentMapper;

    @Autowired
    public void setCountryDao(CountryDao countryDao) {
        this.countryDao = countryDao;
    }

    @Autowired
    public void setContinentDao(ContinentDao continentDao) {
        this.continentDao = continentDao;
    }

    @Autowired
    public void setCountryMapper(CountryMapper countryMapper) {
        this.countryMapper = countryMapper;
    }

    @Autowired
    public void setContinentMapper(ContinentMapper continentMapper) {
        this.continentMapper = continentMapper;
    }

    public CountryDto getCountryById(Long id) throws CountryNotFoundByIdException{
        Optional<Country> countryById = countryDao.findById(id);

        if (countryById.isPresent()) {
            return countryMapper.toDto(countryById.get());
        }else{
            throw new CountryNotFoundByIdException(id);
        }
    }

    public List<CountryDto> getCountryByPartOfName(String name) {
        List<CountryDto> result = new ArrayList();

        countryDao.findByNameContainingIgnoreCase(name).forEach(country -> {
            CountryDto temp = countryMapper.toDto(country);
            result.add(temp);
        });

        return result;
    }

    public List<CountryDto> getAllCountries() {

        List<CountryDto> result = new ArrayList<>();

        countryDao.findAll().forEach(country -> {
            result.add(countryMapper.toDto(country));
        });

        return result;
    }
}
