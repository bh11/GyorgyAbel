package hu.braininghub.countries.dto;

import java.util.List;

import hu.braininghub.countries.repository.entity.Country;

public class ContinentDto {

    private Long id;
    private String name;
    private List<Country> countries;

    public ContinentDto() {
    }

    public ContinentDto(Long id, String name, List<Country> countries) {
        super();
        this.id = id;
        this.name = name;
        this.countries = countries;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String continentName) {
        this.name = continentName;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((countries == null) ? 0 : countries.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ContinentDto other = (ContinentDto) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (countries == null) {
            if (other.countries != null) {
                return false;
            }
        } else if (!countries.equals(other.countries)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ContinentDto [id=" + id + ", continentName=" + name + ", countries=" + countries + "]";
    }
}
