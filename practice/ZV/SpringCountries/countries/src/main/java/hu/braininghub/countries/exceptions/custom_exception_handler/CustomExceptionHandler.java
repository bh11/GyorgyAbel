/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.countries.exceptions.custom_exception_handler;

import hu.braininghub.countries.exceptions.CountryNotFoundByIdException;
import hu.braininghub.countries.exceptions.custom_response.CountryNotFoundByIdCustomResponse;
import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author george
 */

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{
    
    @ExceptionHandler(CountryNotFoundByIdException.class)
    public ResponseEntity<CountryNotFoundByIdCustomResponse> customHandleNotFound(Exception ex, WebRequest request){
        
        CountryNotFoundByIdCustomResponse response = new CountryNotFoundByIdCustomResponse();
            response.setTimestape(LocalDateTime.now());
            response.setErrorMessage(ex.getMessage());
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.ok(response);
    }
}
