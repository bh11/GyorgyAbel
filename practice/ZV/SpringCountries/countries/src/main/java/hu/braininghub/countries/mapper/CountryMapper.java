package hu.braininghub.countries.mapper;

import org.springframework.stereotype.Component;

import hu.braininghub.countries.dto.CountryDto;
import hu.braininghub.countries.repository.entity.Country;

@Component
public class CountryMapper implements Mapper<Country, CountryDto>{

	@Override
	public Country toEntity(CountryDto dto) {
		Country entity = new Country();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		return entity;
	}

	@Override
	public CountryDto toDto(Country entity) {
		CountryDto dto = new CountryDto();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		return dto;
	}

}
