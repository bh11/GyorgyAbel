insert into Continent (continent_name) values ('Africa'),('USA'),('Ausztrália');
insert into Country (country_name, continent_id) values
('Egyiptom', (select continent_id from Continent where continent_name = 'Africa')),
('Niger', (select continent_id from Continent where continent_name = 'Africa')),
('Libia', (select continent_id from Continent where continent_name = 'Africa'));
insert into Country (country_name, continent_id) values
('Nebraska', (select continent_id from Continent where continent_name = 'USA')),
('Kalifornia', (select continent_id from Continent where continent_name = 'USA')),
('Kentucky', (select continent_id from Continent where continent_name = 'USA'));
insert into Country (country_name, continent_id) values
('Tonga', (select continent_id from Continent where continent_name = 'Ausztrália')),
('Tuvalu', (select continent_id from Continent where continent_name = 'Ausztrália')),
('Palau', (select continent_id from Continent where continent_name = 'Ausztrália'));