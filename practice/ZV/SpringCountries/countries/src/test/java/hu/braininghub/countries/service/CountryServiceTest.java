/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.countries.service;

import hu.braininghub.countries.dto.CountryDto;
import hu.braininghub.countries.exceptions.CountryNotFoundByIdException;
import hu.braininghub.countries.mapper.CountryMapper;
import hu.braininghub.countries.repository.CountryDao;
import hu.braininghub.countries.repository.entity.Country;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author george
 */
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class CountryServiceTest {

    private CountryService undertest = new CountryService();

    @Mock
    private CountryMapper countryMapper;

    @Mock
    private CountryDao countryDao;

    @BeforeEach
    void init() {
        undertest.setCountryDao(countryDao);
        undertest.setCountryMapper(countryMapper);
    }

    @Test
    public void testGetAllCountries() {

        Country entity = mock(Country.class);
        CountryDto dto = mock(CountryDto.class);

        when(countryDao.findAll()).thenReturn(Arrays.asList(entity));
        when(countryMapper.toDto(entity)).thenReturn(dto);

        List<CountryDto> result = undertest.getAllCountries();
        assertEquals(Arrays.asList(dto), result);
    }

    @Test
    public void testGetCountryByIdWithCorrectIdInput() {

        Long countryId = 1L;

        Country entity = mock(Country.class);
        CountryDto dto = mock(CountryDto.class);

        when(countryDao.findById(countryId)).thenReturn(Optional.of(entity));
        when(countryMapper.toDto(entity)).thenReturn(dto);

        CountryDto result = undertest.getCountryById(countryId);
        assertEquals(dto, result);
    }

    @Test
    public void testGetCountryByIdWithIncorrectIdInput() {

        Long countryId = -5L;

        when(countryDao.findById(countryId)).thenReturn(Optional.empty());

        CountryNotFoundByIdException result = assertThrows(
                CountryNotFoundByIdException.class,
                () -> undertest.getCountryById(countryId),
                "A következő azonosítóval nem található ország: " + countryId
        );

        assertTrue(result.getMessage().equals("A következő azonosítóval nem található ország: " + countryId));
    }
}
