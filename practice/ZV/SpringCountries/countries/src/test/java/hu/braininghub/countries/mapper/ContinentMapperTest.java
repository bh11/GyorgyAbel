/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.countries.mapper;

import hu.braininghub.countries.dto.ContinentDto;
import hu.braininghub.countries.repository.entity.Continent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author george
 */

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ContinentMapperTest {
    
    private Continent continent;
    private ContinentDto continentDto;
    
    private ContinentMapper underTest = new ContinentMapper();
    
    @BeforeEach
    void init(){
        continent = new Continent();
        continent.setId(1L);
        continent.setContinentName("test");
        
        continentDto = new ContinentDto();
        continentDto.setId(1L);
        continentDto.setName("test");
    }
    
    @Test
    public void testGetContinentWithContinentDtoInput(){
        
        Continent result = underTest.toEntity(continentDto);
        assertEquals(continent, result);
    }
    
    @Test
    public void testGetContinentDtoWithContinentInput(){
        
        ContinentDto result = underTest.toDto(continent);
        assertEquals(continentDto, result);
    }
}
