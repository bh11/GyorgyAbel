/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longestchaininarray;

/**
 *
 * @author s26841
 */
public class LongestChainInArray {

    static void longestChain(int array[]) {
        int index = -1;
        int length = 0;
        
        for (int i = 0; i < array.length; i++) {
            int count = 1;
            boolean stop = false;
            for (int j = i + 1; !stop && j < array.length; j++) {
                if(array[j - 1] < array[j]){
                    count++;
                }else{
                    stop = true;
                }
            }
            if(length < count){
                length = count;
                index = i;
            }
        }
        for (int i = index; i < index + length; i++) {
            System.out.println(array[i]);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int array[] = new int[]{1, 2, 3, 1, 2, 5, 6, 8, 9, 7, 8, 9, 3, 4, 5, 6, 1, 2, 3};
        longestChain(array);
    }

}
